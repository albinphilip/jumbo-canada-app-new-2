@extends('front.layouts.app')
@section('title','Discount Registration | Jumbo Canada')
@section('description','Register for the best discount for products from Jumbo. This is an exclusive discount for students and immigrants. AVAIL NOW!')
@section('keywords','preethi products,vidiem products,kent product,branded products buy online,online shop,,kitchen appliances near me,kitchen accessories online shopping,best home appliances,home appliances store,kitchen appliances store')
@section('canonical','https://jumbokitchenappliances.com/exclusive-discount-for-students-and-new-immigrants')
@section('content')
    <?php
    $province = '';
    $customer = Auth::guard('customer')->user();
    if (Auth::guard('customer')->user())
        if ($customer->getAddress)
            $province = $customer->getAddress->province;
    ?>
    <div class="discount-for-students page">
        <div class="banner">
            <img src="{{asset('assets/img/product-list-banner.jpg')}}" alt="" class="img-fluid">
            <div class="details">
                <h1>Exclusive Discount for Students<br>
                    and New Immigrants</h1>
            </div>
        </div>
        <div class="container">
            <h6>Jumbo Canada Appliances Inc,  would like to welcome new immigrants and students to their new home country, Canada. 
                Welcome to a new life, full of adventure, discovery, and friendships. You will find every country of the world reflected in this 
                beautiful multicultural land. Jumbo Canada  would like to make you feel at home and help you set up your new home with exclusive discounts
                 on Mixer Grinders and Cookwares.</h6>
            <h3>Fill the form below to avail exciting discounts</h3>
            <div class="discount-inner-block">
                <form action="{{route('discount.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-sm-6">
                            <input type="text" class="form-control" placeholder="Your Name" name="name" required
                                   value="@if(Auth::guard('customer')->check()){{$customer->firstname.' '.$customer->lastname}}@endif">
                        </div>
                        <div class="col-sm-6">
                            <input type="email" class="form-control  {{ $errors->has('email') ? 'has-error' : ''}}"
                                   placeholder="Email" name="email" required value="@if(Auth::guard('customer')->check()){{$customer->email}}@endif">
                            {!! $errors->first('email', '<p class="help-block" style="color:red;">:message</p>') !!}

                        </div>
                        <div class="col-sm-6">
                            <input type="tel" class="form-control" placeholder="Phone Number"  data-inputmask='"mask": "(999) 999-9999"' data-mask
                                   name="phone" required value="@if(Auth::guard('customer')->check()){{$customer->telephone}}@endif">
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" placeholder="City" name="city"
                                   value="@if(Auth::guard('customer')->check() && $customer->addresses->count() > 0){{$customer->getAddress->city}} @else{{ old('city') }}@endif">
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" placeholder="Address Line 1" name="address_1"
                                   value="@if(Auth::guard('customer')->check() && $customer->addresses->count() > 0){{$customer->getAddress->address_1}} @else{{ old('address_1') }}@endif">
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" placeholder="Address Line 2" name="address_2"
                                   value="@if(Auth::guard('customer')->check() && $customer->addresses->count() > 0){{$customer->getAddress->address_2}} @else{{ old('address_2') }}@endif">
                        </div>
                        <div class="col-sm-6">
                            <select name="province" id="province" class="form-control" required>
                                <option value="">Select Province</option>
                                <option value="Nunavut"
                                        @if($province=='Nunavut') selected
                                        @elseif(old('provenance') == 'Nunavut') selected @endif>
                                    Nunavut
                                </option>
                                <option value="Quebec"
                                        @if($province=='Quebec') selected
                                        @elseif(old('provenance') == 'Quebec') selected @endif>
                                    Quebec
                                </option>
                                <option value="Northwest Territories"
                                        @if($province=='Northwest Territories') selected
                                        @elseif(old('provenance') == 'Northwest Territories') selected @endif>
                                    Northwest
                                    Territories
                                </option>
                                <option value="Ontario"
                                        @if($province=='Ontario') selected
                                        @elseif(old('provenance') == 'Ontario') selected @endif>
                                    Ontario
                                </option>
                                <option value="British Columbia"
                                        @if($province=='British Columbia') selected
                                        @elseif(old('British Columbia') == 'British Columbia') selected @endif>
                                    British Columbia
                                </option>
                                <option value="Alberta"
                                        @if($province=='Alberta') selected
                                        @elseif(old('provenance') == 'Alberta') selected @endif>
                                    Alberta
                                </option>
                                <option value="Saskatchewan"
                                        @if($province=='Saskatchewan') selected
                                        @elseif(old('provenance') == 'Saskatchewan') selected @endif>
                                    Saskatchewan
                                </option>
                                <option value="Manitoba"
                                        @if($province=='Manitoba') selected
                                        @elseif(old('provenance') == 'Manitoba') selected @endif>
                                    Manitoba
                                </option>
                                <option value="Yukon" @if($province=='Yukon') selected
                                        @elseif(old('provenance') == 'Yukon') selected @endif>Yukon
                                </option>
                                <option value="Newfoundland and Labrador"
                                        @if($province == 'Newfoundland and Labrador') selected
                                        @elseif(old('provenance') == 'Newfoundland and Labrador') selected @endif>
                                    Newfoundland
                                    and Labrador
                                </option>
                                <option value="New Brunswick"
                                        @if($province=='New Brunswick') selected
                                        @elseif(old('provenance') == 'New Brunswick') selected @endif>
                                    New
                                    Brunswick
                                </option>
                                <option value="Nova Scotia"
                                        @if($province == 'Nova Scotia') selected
                                        @elseif(old('provenance') == 'Nova Scotia') selected @endif>
                                    Nova
                                    Scotia
                                </option>
                                <option value="Prince Edward Island"
                                        @if($province=='Prince Edward Island') selected
                                        @elseif(old('provenance') == 'Prince Edward Island') selected @endif>
                                    Prince Edward
                                    Island
                                </option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" placeholder="ZipCode" name="postcode"
                                   value="@if(Auth::guard('customer')->check() && $customer->addresses->count() > 0){{$customer->getAddress->postcode}} @else{{ old('postcode') }}@endif">
                        </div>
                        <div class="col-12">
                            <div class="radio-buttons">

                                <span>Are you</span>
                                <div class="pretty p-default p-curve">
                                    <input type="radio" name="current_status" value="Student" required>
                                    <div class="state p-primary-o">
                                        <label>A Student</label>
                                    </div>
                                </div>
                                <div class="pretty p-default p-curve">
                                    <input type="radio" name="current_status" value="Immigrant"/>
                                    <div class="state p-primary-o">
                                        <label>A New Immigrant (landed in last 6 months)</label>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-3">
                            <div class="proof">
                                <h5>Proof of Document <small style="color:red;">*</small></h5>
                                <div class="upload">
                                    <input class="form-control" name="proof" id="upload-file-img-three" type="file"
                                           required
                                           accept="application/pdf , image/png, image/jpeg" >

                                    <a id="fileupload-img-three"><span>Attach Proof</span><img
                                            src="{{asset('assets/img/attach.svg')}}"
                                            alt="upload"></a>

                                </div>
                            <!-- <div class="upload">
                                    <input name="logo" class="form-control" id="upload-file-img-four" type="file"
                                           accept="image/png, image/jpeg">

                                    <a id="fileupload-img-four"><img
                                            src="{{asset('assets/img/attach.svg')}}"
                                            alt="upload"></a>

                                </div>-->
                            </div>
                        </div>
                        <div class="col-sm-6" style="margin-top: 20px">
                            <input type="checkbox" style="height: 20px; width: 10%" required="" id="agree"> <label
                                for="agree">I Agree
                                <a target="_blank"
                                   href="{{route('policy','terms-and-conditions-of-warranty-and-safe-appliances-usage')}}">
                                    Terms and Conditions</a></label>
                        </div>
                        <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
                        <div class="col-sm-6">
                            <input type="submit" value="submit">
                        </div>

                    </div>
                </form>
                <p>College ID Card , A letter from your college student affairs or admissions office</p>
                <p>Proof of Document (New immigrant)</p>
                <ul>
                    <li>Permanent Resident Card</li>
                    <li>must be valid or expired no more than five years</li>
                    <li>Confirmation of Permanent Residence (IMM 5292, 5688)</li>
                    <li>Canadian Immigration Identification Card</li>
                    <li>Record of Landing (IMM 1000)</li>
                </ul>
                <p><b>Note :</b> On submission of the form admin team will review the proof of document and eligible
                    entry will
                    receive
                    the discount code via email.</p>
            </div>
        </div>
    </div>

@endsection
@section('additionalScripts')
    <script>
        $('input[type="file"]').change(function (e) {
            var fileName = e.target.files[0].name;
            $("#fileupload-img-three").html('<img src="{{asset('assets/img/attach.svg')}}"alt="upload"> ' + fileName.substr(1,6))
        });
    </script>
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute("{{env('GOOGLE_RECAPTCHA_KEY')}}", {action: "productRegistration"}).then(function (token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                recaptchaResponse.value = token;
            });
        });
    </script>
@endsection
