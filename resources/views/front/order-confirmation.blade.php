@extends('front.layouts.app')
@section('title','Jumbo Canada | Order Confirmation')
@section('description','')
@section('keywords','')
@section('content')
    <div class="cart-page page">
        <div class="cart-inner">

            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="common-block">
                            <h2>Thank you for your order</h2>
                            @foreach($orders as $ordr)
                                @php($order = $ordr)
                            @endforeach
                            <ul class="order-details">
                                <li><span>Order number</span><span>#{{$order->order_id}}</span></li>
                                <li><span>order date</span><span>{{date_format($order->created_at,'D M-d-Y')}}</span></li>
                                <li><span>customer</span><span>{{$order->customer->firstname.' '.$order->customer->lastname}}</span></li>
                            </ul>
                                <a href="{{route('order-print',base64_encode($order->order_id))}}" target="_blank" class="print"><i class="fa fa-print" aria-hidden="true"></i>print</a>


                            <p>

                               </p>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="address-block">
                                        <p class="active">Shipping Address</p>
                                        <p>{{$order->deliveryAddress->firstname.' '.$order->deliveryAddress->lastname}}</p>
                                        <p>{{$order->deliveryAddress->address_1}}</p>
                                        <p>{{$order->deliveryAddress->address_2}}</p>
                                        <p>{{$order->deliveryAddress->city}} - {{$order->deliveryAddress->postcode}}</p>
                                        <p>{{$order->deliveryAddress->province}}</p>
                                        <p>Canada</p>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="address-block">
                                        <p class="active">Billing Address</p>
                                        <p>{{$order->billingAddress->firstname.' '.$order->billingAddress->lastname}}</p>
                                        <p>{{$order->billingAddress->address_1}}</p>
                                        <p>{{$order->billingAddress->address_2}}</p>
                                        <p>{{$order->billingAddress->city}} - {{$order->billingAddress->postcode}}</p>
                                        <p>{{$order->billingAddress->province}}</p>

                                        <p>Canada</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                        @if(!Auth::guard('customer')->check())
                        <div class="common-block register">
                            <h2>Register</h2>
                            <h6>FASTER CHECKOUT EVERY TIME</h6>
                            <p>1. <a href="{{route('register-user')}}">Create account</a> or <a href="{{route('customerlogin')}}">Sign in</a></p>
                            <p>2. Save your info</p>
                            <p>3. Checkout faster and enjoy special offers!</p>

                        </div>
                        @endif
                    </div>
                    <div class="col-sm-4">
                        <div class="price-details common-block">
                            <h4>Price Details</h4>
                            <table>
                                <tbody>
                                <tr>
                                    <td>Total price</td>
                                    <td>C${{number_format($order->orderFeesplitUp->sub_total,'2','.',',')}}</td>
                                </tr>
                                <tr>
                                    <td>Reduction</td>
                                    <td>C${{number_format($order->orderFeesplitUp->reduction,'2','.',',')}}</td>
                                </tr>
                                <tr>
                                    <td>Tax({{$order->orderFeesplitUp->tax.'%'}})</td>
                                    <td>C${{$order->orderFeesplitUp->tax_value}}</td>
                                </tr>
                                <tr>
                                    <td> Shipping Charge</td>
                                    <td>C${{number_format($order->orderFeesplitUp->shipping_charge,'2','.',',')}}</td>
                                </tr>


                                <tr class="total">
                                    <td>GRAND TOTAL</td>
                                    <td>C${{number_format($order->order_total,'2','.',',')}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="common-block" id="track_button">
                            <h2>items ordered</h2>
                            @foreach($orders as $order)

                                @if($order->product->getVariant->quantity > 0)
                                    <h6 style="padding: 5px;">Arrives in 1-5 business days</h6>
                                @else
                                    <h6 style="padding: 5px;">Arrives in 25-30 business days</h6>
                                @endif
                            <div class="product">
                                <div class="image">
                                    <a href="{{route('productdetail',[strtolower($order->product->brand->name),$order->product->slug])}}">
                                        <img src="{{asset($order->product->getImage->thumbnail_path)}}" alt="" class="img-fluid">
                                    </a>
                                </div>
                                <div class="details">
                                    <h6>{{$order->product->name}}</h6>
                                    <div class="col-12">
                                        @for($n = 0; $n < $order->quantity; $n++ )
                                            @if($order->shipping_partner == 'Canada Post')
                                                @php($track_array = explode(',',$order->tracking_pin))
                                                <span>
                                                   <a href=""  class="btn btn-sm" style="background-color:#0C2F6B;color: white;" id="track{{$order->id}}"
                                                      onclick="return track({{$track_array[$n]}});" type="button">Track ({{$n+1}})</a>
                                                </span>
                                            @endif
                                        @endfor
                                    </div>
                                </div>
                            </div>



                            @endforeach

                        </div>

                    </div>
                </div>
                <div class="accessories">
                    <h5>Accessories</h5>
                    <div class="owl-carousel owl-theme products-slider">
                        @foreach($products as $product)
                        <div class="item">
                            <div class="product-block">
                                <div class="pdt-image">
                                    <a href="{{route('productdetail',[strtolower($product->brand->name),$product->slug])}}">
                                    @if($product->getImage!=null)
                                        <img data-src="{{asset($product->getImage->thumbnail_path)}}" alt="" class="img-fluid w-100 lazy">
                                    @endif
                                    </a>
                                </div>
                                <div class="pdt-details">
                                    <p class="brand">{{$product->brand->name}}</p>
                                    <p class="name"><a href="{{route('productdetail',[strtolower($product->brand->name),$product->slug])}}">{{$product->name}}</a></p>
                                    <p class="price">
                                        @php($price = 0)
                                        @if($product->offer != null && $product->offer_expire >= today())
                                            <span class="offer">-{{$product->offer}}%</span><span
                                                class="old-price">C${{$product->getVariant->price}}</span>
                                            @php($price = $product->getVariant->price - ($product->getVariant->price*$product->offer/100))
                                        @else
                                            @php($price = $product->getVariant->price)
                                        @endif
                                        <span id="price">C${{$price}}</span>
                                    </p>
                                </div>
                                <div class="actions">
                                    <a href="{{route('productdetail',[strtolower($product->brand->name),$product->slug])}}" class="shop">shop now</a>
                                    <a href="{{route('productdetail',[strtolower($product->brand->name),$product->slug])}}" class="add">add to cart</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- tracking modal -->
    <div class="modal " id="tracking-modal" tabindex="-1" role="dialog"
         aria-labelledby="tracking-modal"
         style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <!--                <h5 class="modal-title">Add Addresses</h5>-->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="body-inner">
                        <h4>shipped with Canada Post</h4>
                        <h5>Tracking ID: <span id="track_id"></span></h5>
                        <div class="day">


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script defer>
    function track(pin) {
        //id = $(e.relatedTarget).data('pin');
        $("#track_id").text(pin);
        url = '/front/order/tracking/'+pin;
        //alert('hh');
        $.ajax({
            type: "GET",
            url: url,
            cache: false,
            data: {_token: '{{csrf_token()}}',_method:"get",id:pin},
            success: function (result) {
                if(result.msg == 'success')
                {
                    //console.log(result.datas);
                    if(!result.datas[0])
                        $('.day').html('<p> Tracking History Not Available</p>');
                    else {
                        $('.day').html('');
                        for(i = 0 ;i< result.datas.length;i++){
                            c_date =  result.datas[i]["date"][0];
                            d_date = c_date.split('-');
                            new_date = new Date(d_date[0],d_date[1]-1,d_date[2]);
                            $('.day').append('<h6>' +new_date.toDateString() + '</h6>' +
                                '<ul class="time-location">'+
                                '<li>'+
                                '<div class="time">'+result.datas[i]["time"][0]+'</div>'+
                                '<div class="location">'+
                                '<h6>'+result.datas[i]["description"][0]+'</h6>'+
                                '<span>'+result.datas[i]["site"][0]+'</span>'+
                                '</div>'+
                                '</li>'+
                                '</ul>');
                        }
                    }
                    $('#tracking-modal').modal('show');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Failed to get tracking details");
            }
        });
        return false;
    }
</script>



