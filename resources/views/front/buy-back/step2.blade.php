@extends('front.layouts.app')
@section('title','Buy Back Registration | Buy Back Offer | Jumbo Canada')
@section('description','Buy Back Offer - Apply now for buyback offer through our Jumbo buyback service. Get extra discounts on your next purchase.')
@section('keywords','')
@section('content')
    @php($customer = Auth::guard('customer')->user())
    <div class="buy-back-page page">
        <div class="banner">
            <img src="{{asset('assets/img/product-list-banner.jpg')}}" alt="" class="img-fluid">
            <!--            <div class="container-fluid">-->
            <!--                <div class="row">-->
            <!--                    <div class="col-sm-6">-->
            <!--                        <h1>Go Green - Buy Back Offer</h1>-->
            <!--                        <p>With a core aim to conserve nature and recycle waste materials we introduce the Go Green Buy-->
            <!--                            Back-->
            <!--                            Offer where we provide customers exclusive discounts on recycling their old Preethi-->
            <!--                            Products. Lets-->
            <!--                            work together to make this a sustainable eco system. Lets go Green !</p>-->
            <!--                    </div>-->
            <!--                    <div class="col-sm-6"></div>-->
            <!--                </div>-->
            <!---->
            <!--            </div>-->
        </div>

        <div class="buy-back-inner">
            <h2>Buy Back Offer</h2>
            <div class="container">
                <ul class="head-block list-inline">
                    <li class="list-inline-item">
                        <div class="block">
                            <p>Step01</p>
                            <h5>Find your nearest recycling location</h5>
                        </div>
                    </li>
                    <li class="list-inline-item active">
                        <div class="block">
                            <p>Step02</p>
                            <h5>Enter details of product to be recycled /
                                recycled product</h5>
                        </div>
                    </li>
                    <li class="list-inline-item">
                        <div class="block">
                            <p>Step03</p>
                            <h5>Attach your Receipt and Avail discount</h5>
                        </div>
                    </li>
                </ul>
                <div class="form-block">
                    <form action="{{route('buy-back.step2.store')}}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-sm-6">
                                <select name="reason" id="reason" class="form-control" required autofocus>
                                    <option value="">Select Product Condition</option>
                                    <option value="Working Condition" {{old('reason') == '>Working Condition' ? 'selected' : '' }}>Working Condition</option>
                                    <option value="Damaged" {{old('reason') == 'Damaged' ? 'selected' : '' }}>Damaged</option>
                                    <option value="Dead" {{old('reason') == 'Dead' ? 'selected' : '' }}>Dead</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <div class="fill">
                                    <input type="text" class="form-control" placeholder="Serial Number" name="serial_number" value="{{old('serial_number')}}">

                                </div>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" required placeholder="Name*" name="name"
                                       value="@if(Auth::guard('customer')->check()){{$customer->firstname.' '.$customer->lastname}} @else{{ old('name') }}@endif">
                            </div>
                            <div class="col-sm-6">
                                <input type="email" class="form-control" required placeholder="Email*" name="email"
                                       value="@if(Auth::guard('customer')->check()){{$customer->email}} @else{{ old('email') }}@endif">
                            </div>
                            <div class="col-sm-6">
                                <input type="tel" class="form-control" required  data-inputmask='"mask": "(999) 999-9999"' data-mask placeholder="Phone* " name="phone"
                                       value="@if(Auth::guard('customer')->check()){{$customer->telephone}}@else{{ old('phone') }}@endif">
                            </div>
                            <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
                            <div class="col-12">
                                <input type="submit" value="continue">
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('additionalScripts')
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute("{{env('GOOGLE_RECAPTCHA_KEY')}}", { action: "BuyBackStep2" }).then(function (token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                recaptchaResponse.value = token;
            });
        });
    </script>
    <!-- InputMask -->
    <script src="{{asset('/plugins/inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>
    <script>
        $('[data-mask]').inputmask()
    </script>
@endsection

