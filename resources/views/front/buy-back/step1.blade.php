@extends('front.layouts.app')
@section('title','Buy Back Registration | Jumbo Canada')
@section('description','Buy Back - Apply for buyback through our Jumbo buyback service. Ensure to have the necessary details regarding the product.')
@section('keywords','buy back registration,back registration,kitchen shop online shopping,kitchen supplies online canada,kitchen appliances near me,kitchen accessories online shopping,best home appliances,home appliances store,kitchen appliances store')
@section('canonical','https://jumbokitchenappliances.com/buy-back/step1')
@section('content')
    <div class="buy-back-page page">
        <div class="banner">
            <img src="{{asset('assets/img/product-list-banner.jpg')}}" alt="" class="img-fluid">
            <!--            <div class="container-fluid">-->
            <!--                <div class="row">-->
            <!--                    <div class="col-sm-6">-->
            <!--                        <h1>Go Green - Buy Back Offer</h1>-->
            <!--                        <p>With a core aim to conserve nature and recycle waste materials we introduce the Go Green Buy-->
            <!--                            Back-->
            <!--                            Offer where we provide customers exclusive discounts on recycling their old Preethi-->
            <!--                            Products. Lets-->
            <!--                            work together to make this a sustainable eco system. Lets go Green !</p>-->
            <!--                    </div>-->
            <!--                    <div class="col-sm-6"></div>-->
            <!--                </div>-->
            <!---->
            <!--            </div>-->
        </div>

        <div class="buy-back-inner">
            <h2>Buy Back Offer</h2>
            <div class="container">
                <ul class="head-block list-inline">
                    <li class="list-inline-item active">
                        <div class="block">
                            <p>Step01</p>
                            <h5>Enter details of product to be recycled
                                / recycled product</h5>
                        </div>
                    </li>
                    <li class="list-inline-">
                        <div class="block">
                            <p>Step02</p>
                            <h5>Find your nearest recycling
                                location</h5>
                        </div>
                    </li>
                    <li class="list-inline-item">
                        <div class="block">
                            <p>Step03</p>
                            <h5>Attach your Receipt and
                                Avail discount</h5>
                        </div>
                    </li>
                </ul>
                <div class="form-block">
                    <h3>How to Recycle</h3>
                    <p>Find the nearest drop-off location <a href="https://www.recyclemyelectronics.ca/" target="_blank">www.recyclemyelectronics.ca</a></p>
                    <p>Drop your product and get the receipt</p>
                    <ul class="list-inline links">
                        <!--                        <li class="list-inline-item"><a href="">Save</a></li>-->
                        <li class="list-inline-item"><a href="{{route('buy-back.step2')}}">continue</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


@endsection
