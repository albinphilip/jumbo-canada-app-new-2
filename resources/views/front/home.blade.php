@extends('front.layouts.app')
@section('title', 'Jumbo Canada | Official dealer of Preethi & Vidiem in Canada')
@section('description','Looking for the best kitchen supply store. You came to the right place we are Canada�s best kitchen supply store with the best customer experience.')
@section('keywords', 'canada kitchen store,canada kitchen supply,canada online kitchen store,cookware online shopping,kitchen accessories online shopping,kitchen accessories store,kitchen shop online shopping,kitchen supplies online canada')
@section('canonical', 'https://jumbokitchenappliances.com/')
@section('content')
    @if ($popup != null && $popup->expiry_date >= today())
        <input type="hidden" id="popup" value="set">
    @endif
    <div class="modal fade" id="image-popup" tabindex="-1" role="dialog" aria-labelledby="consultation-modal"
        style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" @if ($popup != null && $popup->url != '') onclick="window.location.href='{{ url($popup->url) }}'" @endif>
                    <img src="{{ asset('assets/img/close.png') }}" alt="best kitchen appliances - Ultra link kitchen">

                </button>
                @if ($popup != null && $popup->title != '')
                    <div class="modal-header">
                        <a href="{{ $popup->url ? url($popup->url) : '' }}" style="color: #00060c;">
                            <h5 class="modal-title">{{ $popup->title }}</h5>
                        </a>
                    </div>
                @endif
                <div class="modal-body">
                    <div class="body-inner">
                        @if ($popup != null && $popup->body != '')
                            <a href="{{ $popup->url ? url($popup->url) : '' }}" style="color: #00060c;">
                                <p>{{ $popup->body }} </p>
                            </a>
                        @endif
                        @if ($popup != null && $popup->image != '')
                            <a href="{{ $popup->url ? url($popup->url) : '' }}" class="product"><img
                                    src="{{ asset($popup->image) }}" alt="best kitchen utensils -Ultra link kitchen"
                                    class="img-fluid"></a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="index">
        <div class="banner">
            <div class="owl-carousel owl-theme banner-slider">
                <div class="item">
                    <img src="{{ asset('assets/img/banner1.png') }}" alt="" class="img-fluid w-100">
                </div>
                <div class="item">
                    <img src="{{ asset('assets/img/banner2.png') }}" alt="" class="img-fluid w-100">
                </div>
                <div class="item">
                    <img src="{{ asset('assets/img/banner3.png') }}" alt="" class="img-fluid w-100">
                </div>
                <div class="item">
                    <img src="{{ asset('assets/img/banner04-2.png') }}" alt="" class="img-fluid w-100">
                </div>
            </div>
            <!--            <a href="" class="discount-registration">discount registration</a>-->
        </div>
        <div class="container">
            <div class="index-products">
                <h3 class="common-head">Appliances</h3>
                <a href="{{ route('listproduct', 'mixer') }}" class="view-all">view all</a>
                <div class="owl-carousel owl-theme products-slider">
                    @php($i = 0)
                    @foreach ($products->sortByDesc('id') as $grinder)
                        @if(Illuminate\Support\Str::contains($grinder->category->name, 'Mixer'))
                            <div class="item">
                                <div class="product-block">
                                    <div class="pdt-image">
                                        <a
                                            href="{{ route('productdetail', [strtolower($grinder->brand->name), $grinder->slug]) }}">
                                            <img src="{{ asset($grinder->getImage->thumbnail_path) }}" alt=""
                                                class="img-fluid w-100 " id="image{{ $grinder->id }}">
                                        </a>
                                    </div>
                                    <div class="pdt-details">
                                        <p class="brand" id="brand[{{ $grinder->id }}]">{{ $grinder->brand->name }}</p>
                                        <p class="name" id="name[{{ $grinder->id }}]">{{ $grinder->name }}</p>
                                        <p class="price">
                                            @if ($grinder->offer != null && $grinder->offer_expire >= today())
                                                <span class="offer">-{{ $grinder->offer }}%</span><span
                                                    class="old-price">C${{ $grinder->getVariant->price }}</span>
                                                @php($price = $grinder->getVariant->price - ($grinder->getVariant->price *
                                                    $grinder->offer) / 100)
                                            @else
                                                @php($price = $grinder->getVariant->price)
                                            @endif
                                            <span id="price[{{ $grinder->id }}]">C${{ $price }}</span>
                                        </p>
                                        <p hidden id="variant[{{ $grinder->id }}]">{{ $grinder->getVariant->value }}</p>
                                        <p hidden id="slug[{{ $grinder->id }}]">{{ $grinder->slug }}</p>
                                        @if ($grinder->reviews->count() > 0)
                                            @php($rating = 0)
                                                @foreach ($grinder->reviews as $reviews)
                                                    @php($rating = $rating + $reviews->rating / Count($grinder->reviews))
                                                @endforeach
                                            <p hidden id="rating[{{ $grinder->id }}]">{{ $rating }}</p>
                                        @else
                                            <p hidden id="rating[{{ $grinder->id }}]">{{ 0 }}</p>
                                        @endif

                                    </div>
                                    <div class="actions">
                                        <a href="#" onclick="addToCart({{ $grinder->id }},'shop')" class="shop">shop now</a>
                                        <a href="#" onclick="addToCart({{ $grinder->id }},'cart')" class="add">add to cart</a>
                                    </div>
                                </div>
                            </div>
                            @php($i++)
                        @endif
                        @if($i == 6) @break @endif
                    @endforeach
                </div>

                <h3 class="common-head">Kitchenware</h3>
                <a href="{{ route('listproduct', 'kitchenware') }}" class="view-all">view all</a>

                <div class="owl-carousel owl-theme products-slider">
                    @php($i = 0)
                    @foreach ($products->sortByDesc('id') as $cookware)
                        @if(Illuminate\Support\Str::contains($cookware->category->name, 'Kitchenware') || Illuminate\Support\Str::contains($cookware->category->name, 'Water'))
                            <div class="item">
                                <div class="product-block">
                                    <div class="pdt-image">
                                        <a
                                            href="{{ route('productdetail', [strtolower($cookware->brand->name), $cookware->slug]) }}">
                                            <img data-src="{{ asset($cookware->getImage->thumbnail_path) }}" alt=""
                                                class="img-fluid w-100 lazy" id="image{{ $cookware->id }}">
                                        </a>
                                    </div>
                                    <div class="pdt-details">
                                        <p class="brand" id="brand[{{ $cookware->id }}]">{{ $cookware->brand->name }}</p>
                                        <p class="name" id="name[{{ $cookware->id }}]">{{ $cookware->name }}</p>
                                        <p class="price">
                                            @if ($cookware->offer != null && $cookware->offer_expire >= today())
                                                <span class="offer">-{{ $cookware->offer }}%</span><span
                                                    class="old-price">C${{ $cookware->getVariant->price }}</span>
                                                @php($price = $cookware->getVariant->price - ($cookware->getVariant->price *
                                                    $cookware->offer) / 100)
                                            @else
                                                @php($price = $cookware->getVariant->price)
                                            @endif
                                            <span id="price[{{ $cookware->id }}]">C${{ $price }}</span>
                                        </p>

                                        <p hidden id="variant[{{ $cookware->id }}]">{{ $cookware->getVariant->value }}</p>
                                        <p hidden id="slug[{{ $cookware->id }}]">{{ $cookware->slug }}</p>
                                        @if ($cookware->reviews->count() > 0)
                                            @php($rating = 0)
                                                @foreach ($cookware->reviews as $reviews)
                                                    @php($rating = $rating + $reviews->rating / Count($cookware->reviews))
                                            @endforeach
                                            <p hidden id="rating[{{ $cookware->id }}]">{{ $rating }}</p>
                                        @else
                                            <p hidden id="rating[{{ $cookware->id }}]">{{ 0 }}</p>
                                        @endif

                                    </div>
                                    <div class="actions">
                                        <a href="#" onclick="addToCart({{ $cookware->id }},'shop')" class="shop">shop now</a>
                                        <a href="#" onclick="addToCart({{ $cookware->id }},'cart')" class="add">add to cart</a>

                                    </div>
                                </div>
                            </div>
                            @php($i++)
                        @endif
                        @if($i == 6) @break @endif
                    @endforeach
                </div>
             </div>
            <div class="more-products">
            <div class="owl-carousel owl-theme more-products-slider">
                @foreach ($banners->sortByDesc('id') as $banner)
                    <div class="item">
                        <div class="slider-item">
                            <img data-src="{{ asset($banner->banner_image) }}" alt="" class="img-fluid w-100 lazy">
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="genuine">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h3>100% genuine products</h3>
                    <p> This is the official gateway to purchase Indian cookware and Kitchen Appliances from the 
                        authentic source at the best price. We ensure 100 % genuine warranty for products and customer satisfaction.
                    </p>
                    <ul class="list-inline links">
                        <li class="list-inline-item"><a href="{{ route('warranty-registration') }}">Register your
                                product</a></li>
                        <li class="list-inline-item"><a href="{{ route('store') }}">Find Dealers near you</a></li>
                    </ul>
                </div>
                <div class="col-sm-6"></div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="products-slide-block">
        <h3 class="common-head">Parts and Accessories</h3>
        <a href="{{ route('listproduct', 'parts') }}" class="view-all">view all</a>
        <div class="owl-carousel owl-theme products-slider">
            @php($i = 0)
            @foreach ($products->sortByDesc('id') as $part)
                @if(Illuminate\Support\Str::contains($part->category->name, 'Parts'))
                    <div class="item">
                        <div class="product-block">
                            <div class="pdt-image">
                                <a href="{{ route('productdetail', [strtolower($part->brand->name), $part->slug]) }}">
                                    <img data-src="{{ asset($part->getImage->thumbnail_path) }}" alt=""
                                        class="img-fluid w-100 lazy" id="image{{ $part->id }}">
                                </a>
                            </div>
                            <div class="pdt-details">
                                <p class="brand" id="brand[{{ $part->id }}]">{{ $part->brand->name }}</p>
                                <p class="name" id="name[{{ $part->id }}]">{{ $part->name }}</p>
                                <p class="price">
                                    @if ($part->offer != null && $part->offer_expire >= today())
                                        <span class="offer">-{{ $part->offer }}%</span><span
                                            class="old-price">C${{ $part->getVariant->price }}</span>
                                        @php($price = $part->getVariant->price - ($part->getVariant->price *
                                            $part->offer) / 100)
                                    @else
                                        @php($price = $part->getVariant->price)
                                    @endif
                                        <span id="price[{{ $part->id }}]">C${{ $price }}</span>
                                </p>
                                <p hidden id="variant[{{ $part->id }}]">{{ $part->getVariant->value }}</p>
                                <p hidden id="slug[{{ $part->id }}]">{{ $part->slug }}</p>
                                @if ($part->reviews->count() > 0)
                                    @php($rating = 0)
                                        @foreach ($part->reviews as $reviews)
                                            @php($rating = $rating + $reviews->rating / Count($part->reviews))
                                        @endforeach
                                    <p hidden id="rating[{{ $part->id }}]">{{ $rating }}</p>
                                @else
                                    <p hidden id="rating[{{ $part->id }}]">{{ 0 }}</p>
                                @endif

                            </div>
                            <div class="actions">
                                <a href="#" onclick="addToCart({{ $part->id }},'shop')" class="shop">shop now</a>
                                <a href="#" onclick="addToCart({{ $part->id }},'cart')" class="add">add to cart</a>
                            </div>
                        </div>
                    </div>
                    @php($i++)
                @endif
                @if($i == 7) @break @endif
            @endforeach
        </div>
    </div>
    <div class="buy-back">
        <a href="{{ route('buy-back.step1') }}">
            <img data-src="{{ asset('assets/img/buy-back-exchange.png') }}" alt="" class="img-fluid w-100 lazy">
        </a>
    </div>

    </div>

    </div>

@endsection
@section('additionalScripts')
    <script defer>
        $(window).on('load', function() {
            popup = $('#popup').val();
            console.log(popup);
            if (readCookie('popup') == null && popup == 'set') {
                $("#image-popup").modal('show');
                createCookie('popup', 'yes', 1);
            }
        });

        function createCookie(name, value, days) {
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                var expires = "; expires=" + date.toGMTString();
            } else var expires = "";
            document.cookie = name + "=" + value + expires + "; path=/";
        }

        function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        }

    </script>
    <script defer>
        function addToCart(id, tag) {
            $('#msg' + id).html('<i class="fa fa-cog fa-spin"></i> Adding');
            console.log(id);
            variant = null;
            rating = null;
            url = "/add-to-cart";
            var name = document.getElementById('name[' + id + ']').textContent;
            console.log(name);

            var quantity = 1;
            var price = document.getElementById('price[' + id + ']').textContent;
            price = price.slice(2);
            var variant = document.getElementById('variant[' + id + ']').textContent;
            var image = $('#image' + id).attr('src');
            var rating = document.getElementById('rating[' + id + ']').textContent;
            var slug = document.getElementById('slug[' + id + ']').textContent;
            var brand = document.getElementById('brand[' + id + ']').textContent;
            //console.log(rating);
            console.log(slug);
            console.log(image);
            //console.log(variant);

            $.ajax({
                url: url,
                method: 'post',
                data: {
                    product_id: id,
                    name: name,
                    quantity: quantity,
                    price: price,
                    variant: variant,
                    image: image,
                    rating: rating,
                    slug: slug,
                    brand: brand,
                    "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    console.log(response)
                    if (response.success == 'success') {
                        $('#msg' + id).hide();
                        fireTost('Item Added To Cart');
                        console.log(response.cart)
                        $('#c_count span').text(response.cart);
                        if (tag == 'shop')
                            location.href = '/cart';
                    }
                },

            });
        }

        function fireTost(message) {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
            Toast.fire({
                icon: 'success',
                title: message,
            })
        }

    </script>
@endsection
