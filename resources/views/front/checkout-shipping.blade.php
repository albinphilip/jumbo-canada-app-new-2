@extends('front.layouts.app')
@section('title','Jumbo Canada | Checkout')
@section('description','')
@section('keywords','')
@section('content')
    @php($customer=Auth::guard('customer')->user())

    <div class="cart page">
        <div class="container-fluid">
            <ul class="list-inline text-center breadcrumb">
                <li class="list-inline-item"><a href="{{route('cart.index')}}">CART</a></li>
                <li class="list-inline-item"><a href="#" style="color: darkblue">CHECKOUT</a></li>
                <li class="list-inline-item"><a href="#">PAYMENT</a></li>
            </ul>
            @if(!Auth::guard('customer')->check())
            <ul class="list-inline text-left">
                <li class="list-inline-item"><a href="{{route('customerlogin')}}" class="btn btn-primary">LOGIN</a></li>
                <li class="list-inline-item">OR</li>
                <li class="list-inline-item"><a href="{{route('register-user')}}" class="btn btn-primary">SIGN UP</a></li>
            </ul>
            @endif
            <div class="cart-inner">
                <div class="row">
                    <div class="col-sm-8">
                        <h3>Shipping Addresses</h3>
                        @if(!Auth::guard('customer')->check())

                            <form action="{{route('guest.storeaddress')}}" name= "shipping" method="post" id="addressForm" >
                                <input type="hidden" name="recaptcha_response" id="recaptchaResponse1">
                                @csrf
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="text" placeholder="First Name" name="firstname" required class="form-control"
                                               value="@if(session()->has('shippingAddress')){{$shipping->firstname}}@endif" id="firstname">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" placeholder="Last Name" name="lastname" required class="form-control"
                                               value="@if(session()->has('shippingAddress')){{$shipping->lastname}}@endif" id="lastname">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="email" placeholder="Email" name="email"  class="form-control"
                                               value="@if(session()->has('shippingAddress')){{$shipping->customer->email}}@endif" required id="email">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" placeholder="Phone Number" name="telephone"  class="form-control" data-inputmask='"mask": "(999) 999-9999"' data-mask
                                               value="@if(session()->has('shippingAddress')){{$shipping->customer->telephone}}@endif" required id="telephone">
                                    </div>

                                    <div class="col-sm-6">
                                        <input type="text" placeholder="Address Line 1" name="address_1"  class="form-control"
                                               value="@if(session()->has('shippingAddress')){{$shipping->address_1}}@endif" required id="address_1">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" placeholder="Address Line 2" name="address_2" class="form-control"
                                               value="@if(session()->has('shippingAddress')){{$shipping->address_2}}@endif" id="address_2">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" placeholder="City" name="city" class="form-control"
                                               value="@if(session()->has('shippingAddress')){{$shipping->city}}@endif" required id="city">
                                    </div>
                                    <div class="col-sm-6">
                                        <select name="province" class="form-control" id="province" required>
                                            <option value="">Select Province</option>
                                            <option value="Nunavut" @if(session()->has('shippingAddress')){{$shipping->province == 'Nunavut' ? 'selected' :''}}@endif >Nunavut</option>
                                            <option value="Quebec" @if(session()->has('shippingAddress')){{$shipping->province == 'Quebec' ? 'selected' :''}}@endif>Quebec</option>
                                            <option value="Northwest Territories" @if(session()->has('shippingAddress')){{$shipping->province == 'Northwest Territories' ? 'selected' :''}}@endif>Northwest Territories</option>
                                            <option value="Ontario" @if(session()->has('shippingAddress')){{$shipping->province == 'Ontario' ? 'selected' :''}}@endif>Ontario</option>
                                            <option value="British Columbia" @if(session()->has('shippingAddress')){{$shipping->province == 'British Columbia' ? 'selected' :''}}@endif>British Columbia</option>
                                            <option value="Alberta" @if(session()->has('shippingAddress')){{$shipping->province == 'Alberta' ? 'selected' :''}}@endif>Alberta</option>
                                            <option value="Saskatchewan" @if(session()->has('shippingAddress')){{$shipping->province == 'Saskatchewan' ? 'selected' :''}}@endif>Saskatchewan</option>
                                            <option value="Manitoba" @if(session()->has('shippingAddress')){{$shipping->province == 'Manitoba' ? 'selected' :''}}@endif>Manitoba</option>
                                            <option value="Yukon"  @if(session()->has('shippingAddress')){{$shipping->province == 'Yukon' ? 'selected' :''}}@endif>Yukon</option>
                                            <option value="Newfoundland and Labrador" @if(session()->has('shippingAddress')){{$shipping->province == 'Newfoundland and Labrador' ? 'selected' :''}}@endif>Newfoundland and Labrador</option>
                                            <option value="New Brunswick" @if(session()->has('shippingAddress')){{$shipping->province == 'New Brunswick' ? 'selected' :''}}@endif>New Brunswick</option>
                                            <option value="Nova Scotia" @if(session()->has('shippingAddress')){{$shipping->province == 'Nova Scotia' ? 'selected' :''}}@endif>Nova Scotia</option>
                                            <option value="Prince Edward Island" @if(session()->has('shippingAddress')){{$shipping->province == 'Prince Edward Island' ? 'selected' :''}}@endif>Prince Edward Island</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" placeholder="ZIP Code" name="postcode" class="form-control"
                                               value="@if(session()->has('shippingAddress')){{$shipping->postcode}}@endif" required id="postcode">
                                    </div>
                                </div>
                                <h3>Billing Addresses</h3>
                                <div class="col-12">
                                    <label><input type="checkbox" name="addressCheck" id="addressCheck" onclick="checkAddress();" value="yes">
                                    Same as Shipping address</label>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="text" placeholder="First Name" name="bill_firstname" required class="form-control"
                                               value="@if(session()->has('billingAddress')){{$billing->firstname}}@endif" id="bill_firstname">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" placeholder="Last Name" name="bill_lastname" required class="form-control"
                                               value="@if(session()->has('billingAddress')){{$billing->lastname}}@endif" id="bill_lastname">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="email" placeholder="Email" name="bill_email"  class="form-control"
                                               value="@if(session()->has('billingAddress')){{$billing->customer->email}}@endif" required id="bill_email">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" placeholder="Phone Number" name="bill_telephone"  data-inputmask='"mask": "(999) 999-9999"' data-mask class="form-control"
                                               value="@if(session()->has('billingAddress')){{$billing->customer->telephone}}@endif" required id="bill_telephone">
                                    </div>

                                    <div class="col-sm-6">
                                        <input type="text" placeholder="Address Line 1" name="bill_address_1"  class="form-control"
                                               value="@if(session()->has('billingAddress')){{$billing->address_1}}@endif" required id="bill_address_1">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" placeholder="Address Line 2" name="bill_address_2" class="form-control"
                                               value="@if(session()->has('billingAddress')){{$billing->address_2}}@endif" id="bill_address_2">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" placeholder="City" name="bill_city" class="form-control"
                                               value="@if(session()->has('billingAddress')){{$billing->city}}@endif" required id="bill_city">
                                    </div>
                                    <div class="col-sm-6">
                                        <select name="bill_province" class="form-control" id="bill_province" required>
                                            <option value="">Select Province</option>
                                            <option value="Nunavut" @if(session()->has('billingAddress')){{$billing->province == 'Nunavut' ? 'selected' :''}}@endif >Nunavut</option>
                                            <option value="Quebec" @if(session()->has('billingAddress')){{$billing->province == 'Quebec' ? 'selected' :''}}@endif>Quebec</option>
                                            <option value="Northwest Territories" @if(session()->has('billingAddress')){{$billing->province == 'Northwest Territories' ? 'selected' :''}}@endif>Northwest Territories</option>
                                            <option value="Ontario" @if(session()->has('billingAddress')){{$billing->province == 'Ontario' ? 'selected' :''}}@endif>Ontario</option>
                                            <option value="British Columbia" @if(session()->has('billingAddress')){{$billing->province == 'British Columbia' ? 'selected' :''}}@endif>British Columbia</option>
                                            <option value="Alberta" @if(session()->has('billingAddress')){{$billing->province == 'Alberta' ? 'selected' :''}}@endif>Alberta</option>
                                            <option value="Saskatchewan" @if(session()->has('billingAddress')){{$billing->province == 'Saskatchewan' ? 'selected' :''}}@endif>Saskatchewan</option>
                                            <option value="Manitoba" @if(session()->has('billingAddress')){{$billing->province == 'Manitoba' ? 'selected' :''}}@endif>Manitoba</option>
                                            <option value="Yukon"  @if(session()->has('billingAddress')){{$billing->province == 'Yukon' ? 'selected' :''}}@endif>Yukon</option>
                                            <option value="Newfoundland and Labrador" @if(session()->has('billingAddress')){{$billing->province == 'Newfoundland and Labrador' ? 'selected' :''}}@endif>Newfoundland and Labrador</option>
                                            <option value="New Brunswick" @if(session()->has('billingAddress')){{$billing->province == 'New Brunswick' ? 'selected' :''}}@endif>New Brunswick</option>
                                            <option value="Nova Scotia" @if(session()->has('billingAddress')){{$billing->province == 'Nova Scotia' ? 'selected' :''}}@endif>Nova Scotia</option>
                                            <option value="Prince Edward Island" @if(session()->has('billingAddress')){{$billing->province == 'Prince Edward Island' ? 'selected' :''}}@endif>Prince Edward Island</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" placeholder="ZIP Code" name="bill_postcode" class="form-control"
                                               value="@if(session()->has('billingAddress')){{$billing->postcode}}@endif" required id="bill_postcode">
                                    </div>
                                </div>
                                <h3>Delivery Instruction</h3>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <textarea class="form-control" placeholder="" name="instruction" >@if(session()->has('instruction')){{session()->get('instruction')}}@endif</textarea>
                                    </div>
                                </div>
                            </form>
                        @else
                        <!-- registered user -->
                            <a href="" class="add-new" data-toggle="modal" data-target="#add-address">add new</a>
                            <div class="modal fade" id="add-address" tabindex="-1" role="dialog"
                                 aria-labelledby="consultation-modal"
                                 style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Add Addresses</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="body-inner">
                                                <form action="{{route('user.storeaddress')}}" method="post">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <input type="text" placeholder="First Name" name="firstname" required class="form-control"
                                                                   value="">
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input type="text" placeholder="Last Name" name="lastname" required class="form-control"
                                                                   value="">
                                                        </div>
                                                    </div>

                                                    <input type="text" placeholder="Address Line 1" name="address_1"  class="form-control"
                                                           value="" required id="new_addr_1">
                                                    <input type="text" placeholder="Address Line 2" name="address_2" class="form-control"
                                                           value=""  id="new_addr_2">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <input type="text" placeholder="City" name="city" class="form-control"
                                                                   value="" required id="new_city" >
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <select name="province" class="form-control" id="new_province" required>
                                                                <option value="">Select Province</option>
                                                                <option value="Nunavut" >Nunavut</option>
                                                                <option value="Quebec" >Quebec</option>
                                                                <option value="Northwest Territories">Northwest Territories</option>
                                                                <option value="Ontario">Ontario</option>
                                                                <option value="British Columbia" >British Columbia</option>
                                                                <option value="Alberta">Alberta</option>
                                                                <option value="Saskatchewan">Saskatchewan</option>
                                                                <option value="Manitoba">Manitoba</option>
                                                                <option value="Yukon" >Yukon</option>
                                                                <option value="Newfoundland and Labrador" >Newfoundland and Labrador</option>
                                                                <option value="New Brunswick" >New Brunswick</option>
                                                                <option value="Nova Scotia" >Nova Scotia</option>
                                                                <option value="Prince Edward Island">Prince Edward Island</option>
                                                            </select>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <input type="text" placeholder="ZIP Code" name="postcode" class="form-control"
                                                                   value="" required id="new_postcode">
                                                        </div>
                                                    </div>
                                                    <div class="pretty p-default p-curve">
                                                        <input type="checkbox" name="custom_field" value="yes" checked>
                                                        <div class="state">
                                                            <label>Make this my default address</label>
                                                        </div>
                                                    </div>
                                                    <div class="actions">
                                                        <ul class="list-inline">
                                                            <li class="list-inline-item">
                                                                <button type="button" data-dismiss="modal"
                                                                        aria-label="Close" class="form-control">cancel
                                                                </button>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <button type="submit" class="form-control">create</button>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <form action="{{route('user.billingaddress')}}" name= "shipping" method="post" id="addressForm" >
                                @csrf
                                @php($flag = 0)
                            @if($customer->addresses()->count() > 0)
                                <div class="row">
                                    @foreach($customer->addresses->sortByDesc('id') as $address)
                                        @if($address->type !='billing' && $address->type !='hidden')
                                            @php($flag = 1)
                                            <div class="address-block">
                                                <label class="radio-inline">
                                                    <input type="radio" class="form-check-inline" value="{{$address->id}}"
                                                           name="radio1" id="radio1" onclick="radioclick()" required
                                                            @if(session()->has('shippingAddress')){{$shipping->id  == $address->id ? 'checked' : ''}}@endif>
                                                    <p id="name{{$address->id}}">{{$address->firstname.' '.$address->lastname}}</p>
                                                    <p hidden id="fname{{$address->id}}">{{$address->firstname}}</p>
                                                    <p hidden id="lname{{$address->id}}">{{$address->lastname}}</p>
                                                    <p id="addr1{{$address->id}}">{{$address->address_1}}</p>
                                                    <p id="addr2{{$address->id}}">{{$address->address_2}}</p>
                                                    <p id="province{{$address->id}}">{{$address->province}}</p>
                                                    <p id="city{{$address->id}}">{{$address->city .' - '}}{{$address->postcode}}</p>
                                                </label>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            @endif
                            <h3>Billing Addresses</h3>
                                <div class="col-12">
                                    <label><input type="checkbox" name="addressCheck" id="addressCheck2" onclick="checkUserAddress();" value="yes">
                                    Same as Shipping address</label>
                                </div>
                                <div class="row" id="bill_form">
                                    <div class="col-sm-6">
                                        <input type="text" placeholder="First Name" name="bill_firstname" required class="form-control"
                                               value="@if(session()->has('billingAddress')){{$billing->firstname}}@endif" id="bill_firstname">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" placeholder="Last Name" name="bill_lastname" required class="form-control"
                                               value="@if(session()->has('billingAddress')){{$billing->lastname}}@endif" id="bill_lastname">
                                    </div>

                                    <div class="col-sm-6">
                                        <input type="text" placeholder="Address Line 1" name="bill_address_1"  class="form-control"
                                               value="@if(session()->has('billingAddress')){{$billing->address_1}}@endif" required id="bill_address_1">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" placeholder="Address Line 2" name="bill_address_2" class="form-control"
                                               value="@if(session()->has('billingAddress')){{$billing->address_2}}@endif" id="bill_address_2">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" placeholder="City" name="bill_city" class="form-control"
                                               value="@if(session()->has('billingAddress')){{$billing->city}}@endif" required id="bill_city">
                                    </div>
                                    <div class="col-sm-6">
                                        <select name="bill_province" class="form-control" id="bill_province" required>
                                            <option value="">Select Province</option>
                                            <option value="Nunavut" @if(session()->has('billingAddress')){{$billing->province == 'Nunavut' ? 'selected' :''}}@endif >Nunavut</option>
                                            <option value="Quebec" @if(session()->has('billingAddress')){{$billing->province == 'Quebec' ? 'selected' :''}}@endif>Quebec</option>
                                            <option value="Northwest Territories" @if(session()->has('billingAddress')){{$billing->province == 'Northwest Territories' ? 'selected' :''}}@endif>Northwest Territories</option>
                                            <option value="Ontario" @if(session()->has('billingAddress')){{$billing->province == 'Ontario' ? 'selected' :''}}@endif>Ontario</option>
                                            <option value="British Columbia" @if(session()->has('billingAddress')){{$billing->province == 'British Columbia' ? 'selected' :''}}@endif>British Columbia</option>
                                            <option value="Alberta" @if(session()->has('billingAddress')){{$billing->province == 'Alberta' ? 'selected' :''}}@endif>Alberta</option>
                                            <option value="Saskatchewan" @if(session()->has('billingAddress')){{$billing->province == 'Saskatchewan' ? 'selected' :''}}@endif>Saskatchewan</option>
                                            <option value="Manitoba" @if(session()->has('billingAddress')){{$billing->province == 'Manitoba' ? 'selected' :''}}@endif>Manitoba</option>
                                            <option value="Yukon"  @if(session()->has('billingAddress')){{$billing->province == 'Yukon' ? 'selected' :''}}@endif>Yukon</option>
                                            <option value="Newfoundland and Labrador" @if(session()->has('billingAddress')){{$billing->province == 'Newfoundland and Labrador' ? 'selected' :''}}@endif>Newfoundland and Labrador</option>
                                            <option value="New Brunswick" @if(session()->has('billingAddress')){{$billing->province == 'New Brunswick' ? 'selected' :''}}@endif>New Brunswick</option>
                                            <option value="Nova Scotia" @if(session()->has('billingAddress')){{$billing->province == 'Nova Scotia' ? 'selected' :''}}@endif>Nova Scotia</option>
                                            <option value="Prince Edward Island" @if(session()->has('billingAddress')){{$billing->province == 'Prince Edward Island' ? 'selected' :''}}@endif>Prince Edward Island</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" placeholder="ZIP Code" name="bill_postcode" class="form-control"
                                               value="@if(session()->has('billingAddress')){{$billing->postcode}}@endif" required id="bill_postcode">
                                    </div>
                                </div>
                                <h3>Delivery Instruction</h3>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <textarea class="form-control" placeholder="" name="instruction">@if(session()->has('instruction')){{session()->get('instruction')}}@endif</textarea>
                                    </div>
                                </div>
                            </form>
                        @endif
                            <div class="secure-payment">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <img src="{{asset('assets/img/secure.png')}}" alt="" class="img-fluid secure">
                                        <h4>100% <br><span>secure payment</span></h4>
                                    </div>
                                    <div class="col-sm-8">
                                        <img src="{{asset('assets/img/pay.png')}}" alt="" class="img-fluid pay">
                                    </div>
                                </div>
                            </div>
                        </div>

                    <div class="col-sm-4">
                        <div class="cart-actions">
                            <h3>CART ITEMS</h3>
                            <div class="cart-items-pay">
                                <table>
                                    <tbody>
                                    @foreach($cart as $item)
                                        <tr>
                                            <td>{{$item->name}} <span style="color: darkblue"> ({{$item->qty}}) </span>

                                            </td>
                                            <td>C${{$item->price*$item->qty}}</td>
                                        </tr>
                                        @endforeach
                                    <tr class="total">
                                        <td>TOTAL</td>
                                        <td>C${{Cart::total()}}</td>
                                        @php(session()->put('total', \Cart::total()))

                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            @if($customer && $flag == 0 )
                                <button type="button" class="proceed" form="addressForm" onclick="msg();">Make Payment</button>
                                <p id="msg" style="color:red;"></p>
                            @else
                                <button type="submit" class="proceed" form="addressForm">Continue <i class="fa fa-angle-double-right"></i> </button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('additionalScripts')
    <!-- InputMask -->
    <script src="{{asset('/plugins/inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>
    <script>
        $('[data-mask]').inputmask()
    </script>
    <script>
        function checkAddress()
        {
            if ($('#addressCheck').is(":checked")) {
                console.log('checked');
                fname = $('#firstname').val();
                lname = $('#lastname').val();
                email = $('#email').val();
                telephone = $('#telephone').val();
                province = $('#province').val();
                address_1 = $('#address_1').val();
                address_2 = $('#address_2').val();
                city = $('#city').val();
                postcode = $('#postcode').val();

                $('#bill_firstname').val(fname);
                $('#bill_lastname').val(lname);
                $('#bill_email').val(email);
                $('#bill_telephone').val(telephone);
                $('#bill_province').val(province);
                $('#bill_address_1').val(address_1);
                $('#bill_address_2').val(address_2);
                $('#bill_city').val(city);
                $('#bill_postcode').val(postcode);
            } else {
                $('#bill_firstname').val(null);
                $('#bill_lastname').val(null);
                $('#bill_email').val(null);
                $('#bill_telephone').val(null);
                $('#bill_province').val(null);
                $('#bill_address_1').val(null);
                $('#bill_address_2').val(null);
                $('#bill_city').val(null);
                $('#bill_postcode').val(null);
            }
        }

        function radioclick() {
            address =  $('input[name=radio1]:checked').val();
            console.log(address);
            url="/store-delivery-address/"+address;
            $.ajax({
                url: url,
                method: 'get',
                success: function (response) {
                    console.log(response)
                    if ($('#addressCheck2').is(":checked"))
                    {
                         checkUserAddress();
                    }
                },

            })
        }

    </script>
    <script>
        function checkUserAddress()
        {
            if ($('#addressCheck2').is(":checked")) {
                address =  $('input[name=radio1]:checked').val();
                if(!address) alert('Select Shipping Address');
                else {
                    fname = document.getElementById('fname'+address).textContent;
                    f=lname = document.getElementById('lname'+address).textContent;
                    province = document.getElementById('province'+address).textContent;
                    address_1 = document.getElementById('addr1'+address).textContent;
                    address_2 = document.getElementById('addr2'+address).textContent;
                    city = document.getElementById('city'+address).textContent;
                    city = city.split('-');

                    $('#bill_firstname').val(fname);
                    $('#bill_lastname').val(lname);
                    $('#bill_address_1').val(address_1);
                    $('#bill_address_2').val(address_2);
                    $('#bill_province').val(province);
                    $('#bill_city').val(city[0]);
                    $('#bill_postcode').val(city[1]);
                }
            }
            else {
                $('#bill_firstname').val(null);
                $('#bill_lastname').val(null);
                $('#bill_email').val(null);
                $('#bill_telephone').val(null);
                $('#bill_province').val(null);
                $('#bill_address_1').val(null);
                $('#bill_address_2').val(null);
                $('#bill_city').val(null);
                $('#bill_postcode').val(null);
            }
        }
         function msg(){
            $('#msg').text('Add Shipping and Billing Address');
         }
    </script>
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute("{{env('GOOGLE_RECAPTCHA_KEY')}}", { action: "CheckoutAddress" }).then(function (token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse1');
                recaptchaResponse.value = token;
            });
        });
    </script>
    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_API_KEY')}}&sensor=false&libraries=places"></script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var options = {
                componentRestrictions: {country: ['CA']},
            };
            console.log('here');

            var places = new google.maps.places.Autocomplete(document.getElementById('address_1'), options);
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var add=address.split(",");
                console.log(add[0],add[1],add[2]);
                 var province=add[2].substr(0,3);
                console.log(province);
                province_array = {'NL':'Newfoundland and Labrador','PE':'Prince Edward Island',
                    'NS':'Nova Scotia','NB':'New Brunswick','QC':'Quebec','ON':'Ontario','MB':'Manitoba',
                    'SK':'Saskatchewan','AB':'Alberta','YT':'Yukon','NT':'Northwest Territories','NU':'Nunavut','BC':'British Columbia'};

                $("#address_1").val(add[0]);
                $('#city').val(add[1]);
                console.log(province_array[province.trim()]);
                $('select[name^="province"] option:selected').attr("selected",null);
               // $("#province").find('option[value="'+province_array[province.trim()]+'"]').attr('selected','selected');
                $('#province').val(province_array[province.trim()]);
                $('#postcode').val(add[2].substr(3,8));
                console.log($('#province').val());
            });
//billing
            var places1 = new google.maps.places.Autocomplete(document.getElementById('bill_address_1'), options);
            google.maps.event.addListener(places1, 'place_changed', function () {
                var place1 = places1.getPlace();
                var address1 = place1.formatted_address;
                var add1=address1.split(",");
                console.log(add1[0],add1[1],add1[2]);
                var province1=add1[2].substr(0,3);
                console.log(province1);
                province_array = {'NL':'Newfoundland and Labrador','PE':'Prince Edward Island',
                    'NS':'Nova Scotia','NB':'New Brunswick','QC':'Quebec','ON':'Ontario','MB':'Manitoba',
                    'SK':'Saskatchewan','AB':'Alberta','YT':'Yukon','NT':'Northwest Territories','NU':'Nunavut','BC':'British Columbia'};

                $("#bill_address_1").val(add1[0]);
                $('#bill_city').val(add1[1]);
                console.log(province_array[province1.trim()]);
                $('select[name^="bill_province"] option:selected').attr("selected",null);
                //$("#bill_province").find('option[value="'+province_array[province1.trim()]+'"]').attr('selected','selected');
                $('#bill_province').val(province_array[province1.trim()]);
                $('#bill_postcode').val(add1[2].substr(3,8));
                console.log($('#bill_province').val());
            });
        });
    </script>

@endsection

