@extends('front.layouts.app')
@section('title','Contact Us | Best Home Appliances Online Store in Canada')
@section('description','Complete our online booking form and we will contact you shortly. With the unique offers your Jumbo home appliance is as good as it gets.')
@section('keywords','best online store for appliances,canada kitchen store,canada kitchen supply,canada online kitchen store,cookware online shopping,kitchen accessories online shopping,kitchen accessories store,kitchen shop online shopping,kitchen supplies online canada')
@section('canonical','https://jumbokitchenappliances.com/contact-us')
@section('content')
    @php($customer=Auth::guard('customer')->user())
    <div class="contact page">
        <div class="container-fluid">
            <h1>Contact Us</h1>
            <div class="row">
                <div class="col-sm-3">
                    <ul class="contact-info">
                        <li class="phone">
                            <h4>Free support line !</h4>
                            <a href="tel:18557733844">+1 (855) 773 3844</a>
                        </li>
                        <li class="email">
                            <h4>Email</h4>
                            <a href="mailto:support@jumbocanada.com" target="_blank">support@jumbocanada.com</a>
                        </li>
                    </ul>

                </div>
                <div class="col-sm-9">
                    <form action="{{route('enquiry')}}" class="contact-form php-email-form" method="post" id="enquiry-form">
                        @csrf
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="text" placeholder="Your Name" class="form-control" name="name" required
                                       value="@if(Auth::guard('customer')->check()){{$customer->firstname.' '.$customer->lastname}}@endif">
                            </div>
                            <div class="col-sm-6">
                                <input type="email" placeholder="E-Mail" pattern= "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" class="form-control" name="email" required
                                       value="@if(Auth::guard('customer')->check()){{$customer->email}}@endif">

                            </div>
                            <div class="col-sm-6">
                                <input type="text" placeholder="Phone Number"  class="form-control" name="phone" required
                                       data-inputmask='"mask": "(999) 999-9999"' data-mask
                                       value="@if(Auth::guard('customer')->check()){{$customer->telephone}}@endif">

                            </div>
                            <div class="col-sm-12">
                                <textarea class="form-control" placeholder="Enquiry" name="message" required></textarea>
                            </div>
                            <div class="col-sm-4">
                                <input type="submit">
                            </div>
                            <div class="col-6">
                                <div class="thanks" id="thanks" style="display: none; color:red;">
                                    Thank you for sharing your details. We will get back to you soon
                                </div>
                                <div class="response"  id="pleaseWait" style="display: none;color:red;" >
                                    Please wait <i class="fa fa-cog fa-spin"></i>
                                </div>
                                <div class="error-message mt-3"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="stay-in-touch">
                <h3>Stay In Touch</h3>
                <h6>You can unsuscribe at any time - We promise we not to spam.</h6>
                <form method="post" id="subscribe1">
                    @csrf
                    <input type="email" placeholder="Enter your email" class="form-control" name="email" required id="email_sub1">
                    <button type="submit" class="form-control" id="save1">subscribe<img src="{{asset('assets/img/arrow-grey.png')}}" alt="" class="img-fluid">
                    </button>
                </form>
            </div>

        </div>
    </div>
@endsection
@section('additionalScripts')
    <!-- InputMask -->
    <script  src="{{asset('/plugins/inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>
    <script>
        $('[data-mask]').inputmask()
    </script>
    <script defer>
        $("#subscribe1").on('submit', function (event) {
            $("#save1").val('Please wait ..');
            $("#save1").html('Please wait ..');
            email =$('#email_sub1').val();
            event.preventDefault();
            //grecaptcha.ready(function () {
              //  grecaptcha.execute("{{env('GOOGLE_RECAPTCHA_KEY')}}", {action: "contact"}).then(function (token) {
                    $.ajax({
                        type: 'post',
                        url: '/subscribe',
                        data:{ email,
                        "_token": "{{ csrf_token() }}",
                    },
                        success: function (response) {

                            if (response == 'OK'){


                                firTost('Thank you for subscribing');

                            }
                            else if(response == 'YES'){
                                firTost('Already Subscribed');

                            }
                            else{
                                alert(response);
                                firTosterror(response);

                            }
                            $("#save").html('Subscribe');
                            $("#subscribe")[0].reset();
                                },
                        error: function () {
                            alert('Some error occurred');
                        }
                    });
              //  })
            //  })
        });
        function firTost(message) {
            const Toast22 = Swal.mixin({
                toast: false,
                //position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
            Toast22.fire({
                icon: 'success',
                title: message,
            })
        }
        function firTosterror(message) {
            const Toast1 = Swal.mixin({
                toast: false,
               // position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
            Toast1.fire({
                icon: 'error',
                title: message,
            })
        }


    </script>
    <script src="https://www.google.com/recaptcha/api.js?render={{env('GOOGLE_RECAPTCHA_KEY')}}"></script>
    <script>var recptchaKey = "{{env('GOOGLE_RECAPTCHA_KEY')}}"</script>
    <style>
        .grecaptcha-badge {
            visibility: hidden;
        }
    </style>
    <!-- RECAPTCHA -->
@endsection
