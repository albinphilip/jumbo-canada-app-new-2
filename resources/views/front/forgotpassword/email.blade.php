@extends('front.layouts.app')
@section('title','Preethi Ultra | Reset Password')
@section('description','')
@section('keywords','')
@section('content')
    <div class="profile page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3">
                    <ul class="profile-links">
                        <li><a href="{{route('profile')}}">My Profile</a></li>
                        <li><a href="{{route('address')}}">My Address Book</a></li>
                        <li><a href="{{route('edit-profile')}}">Edit Account</a></li>
                        <li><a href="{{route('password')}}">Password</a></li>
                        <li><a href="">My Favourites Lists</a></li>
                        <li><a href="">Order History</a></li>
                    </ul>
                </div>
                <div class="col-sm-9">
                    <h1>Forgot Password</h1>
                    <div class="profile-inner">
                        <form class="form-horizontal" method="POST" action="{{ route('password.sendemail') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-5">
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <input type="hidden" name="recaptcha_response" id="recaptchaResponse">

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <input type="submit" value="Send Reset Link">
                                </div>
                            </div>
                            @if(Session::has('message'))
                                <p style="color:red;">{{Session::get('message')}}</p>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('additionalScripts')
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute("{{env('GOOGLE_RECAPTCHA_KEY')}}", { action: "ForgetPasswordEmail" }).then(function (token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                recaptchaResponse.value = token;
            });
        });
    </script>
@endsection
