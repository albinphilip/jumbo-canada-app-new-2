@extends('front.layouts.app')
@section('title','Preethi Ultra | Reset Password')
@section('description','')
@section('keywords','')
@section('content')
    <div class="profile page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3">
                    <ul class="profile-links">
                        <li><a href="{{route('profile')}}">My Profile</a></li>
                        <li><a href="{{route('address')}}">My Address Book</a></li>
                        <li><a href="{{route('edit-profile')}}">Edit Account</a></li>
                        <li><a href="{{route('password')}}">Password</a></li>
                        <li><a href="">My Favourites Lists</a></li>
                        <li><a href="">Order History</a></li>
                    </ul>
                </div>
                <div class="col-sm-9">
                    <h1>Reset Password</h1>
                    <div class="profile-inner">
                        <form class="form-horizontal" method="POST" action="{{ route('password') }}">
                            {{ csrf_field() }}

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="row">
                                        <input type="hidden" value="{{$email}}" name="email">
                                        <div class="col-sm-12"><input type="password" placeholder="New Password"
                                                                      class="form-control {{ $errors->has('password') ? 'has-error' : ''}}" name="password" required>
                                            {!! $errors->first('password', '<p class="help-block" style="color:red;">:message</p>') !!}
                                        </div>
                                        <div class="col-sm-12">
                                            <p class="password">Password must be of minimum length 8-16 and must has at least 1 digit
                                                and 1 character</p>
                                        </div>
                                        <div class="col-sm-12"><input type="password" placeholder="Confirm New Password"
                                                                      class="form-control {{ $errors->has('confirm_password') ? 'has-error' : ''}}" name="confirm_password" required>
                                            {!! $errors->first('confirm_password', '<p class="help-block" style="color:red;">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
                            <input type="submit" value="update">
                            @if(Session::has('message'))
                                <p style="color:red;">{{Session::get('message')}}</p>
                                <span><a href="{{route('customerlogin')}}">Click Here To Login</a></span>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('additionalScripts')
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute("{{env('GOOGLE_RECAPTCHA_KEY')}}", { action: "ResetPassword" }).then(function (token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                recaptchaResponse.value = token;
            });
        });
    </script>
@endsection
