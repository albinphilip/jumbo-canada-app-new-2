@extends('front.layouts.app')
@section('title','Jumbo Canada | Write a review')
@section('description','')
@section('keywords','')
@section('content')
    <div class="register page">
        <div class="register-inner">
            <h1>Review {{$product->name}}</h1>
            <form action="{{route('review.store')}}" method="post">
                @csrf
                <div class="row">
                        <input type="hidden"  value="{{$product->id}}" name="product_id">

                    <div class="col-sm-6">
                        <input type="number" step=".01" max="5" min="0" class="form-control" required placeholder="Rating (out of 5)" name="rating">

                    </div>

                    <div class="col-sm-6"></div>
                    <div class="col-sm-6">
                        <textarea class="form-control" name="review" placeholder="Review" required></textarea>
                    </div>
                    <div class="col-12">
                        <input type="submit" value="submit">
                    </div>
                </div>
            </form>
        </div>
    </div>


@endsection


