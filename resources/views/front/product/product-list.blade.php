@extends('front.layouts.app')
@if($tab == 'countertop')
    @section('title','Mixer Grinders | Home Appliances Online | Preethi, Vidiem')
    @section('description',"Best quality Mixers and grinders in Canada. Indian Mixers and Grinders. Authorized dealer. Preethi Mixer. Vidiem Grinder.")
    @section('keywords','kitchen tools online store,online shop kitchen accessories,kitchen items shopping online,canada kitchen store,canada kitchen supply,canada online kitchen store,cookware online shopping,Mixer Grinders online')
    @section('canonical','https://jumbokitchenappliances.com/products/countertop')
@elseif($tab == 'parts')
    @section('title','Preethi parts and Accessories | Preethi Appliances | Preethi Product')
    @section('description',"Kitchen Appliances Parts and Accessories in Canada. Preethi Parts online. Mixer Parts and Accessories.")
    @section('keywords','preethi products parts,preethi mixer grinder price,preethi mixer grinder parts,preethi parts and accessories,preethi appliances,preethi products online store,preethi mixer grinder,preethi wet grinder,preethi products,preethi steele mixer grinder,preethi mixer grinder jar,blue leaf mixer grinder')
    @section('canonical','https://jumbokitchenappliances.com/products/parts')
@elseif($tab == 'kitchenware')
    @section('title','Butterfly Cookware |Idly Cooker online | Kitchen ware')
    @section('description',"Buy top quality cookware and kitchen accessories in Canada. Butterfly Cookware Canada. Indian Kitchenware in Canada.")
    @section('keywords','kitchen ware,kitchen accessories,online store for kitchen accessories,kitchen accessories online shopping,kitchen accessories store,online shopping kitchen items,kitchen items shop near me,kitchen accessories shop,kitchen ware shop')
    @section('canonical','https://jumbokitchenappliances.com/products/kitchenware')
@elseif($tab == 'water')
    @section('title','Kent Alkaline Pitcher | Kent Gravity Pitcher')
    @section('description',"Kent Gravity Pitcher and Kent Alkaline pitcher online. Water filter pitchers in Canada. Best quality water filters in Canada.")
    @section('keywords','kent products,shop kitchenware online,kitchen appliances store,the kitchen store canada,the kitchen shop online,online shop for appliances,canada kitchen store online,online home appliances store,kent online products,kitchen tools online store,online shop kitchen accessories,kitchen items shopping online,Kent alkaline pitcher products')
    @section('canonical','https://jumbokitchenappliances.com/products/water')
@endif
@section('content')
    <div class="product-list-page page">
        <div class="banner-common">
           @if($banner != null) <img src="{{asset($banner->image)}}" alt="" class="img-fluid w-100">@endif
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="accordion" id="accordion-four">
                        @foreach($brands as $c_brand)
                            @if($brand == '') @php($name = '')
                            @elseif($brand != null && is_string($brand) ) @php($name = $brand)
                            @else @php($name = $brand->name) @endif
                            @if($name == $c_brand->name) @php($active = 'show') @php($collapsed = '')
                            @else @php($active = '') @php($collapsed = 'collapsed') @endif
                            <div class="card">
                                <div  id="heading{{$c_brand->id}}" class="card-header">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link {{$collapsed}} " data-toggle="collapse"
                                                data-target="#collapse{{$c_brand->id}}" aria-expanded="true"
                                                aria-controls="collapse{{$c_brand->id}}">
                                            {{$c_brand->name}}
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapse{{$c_brand->id}}" class="collapse {{$active}}" aria-labelledby="heading{{$c_brand->id}}"
                                     data-parent="#accordion-four">
                                    <div class="card-body">
                                        <ul>

                                            @foreach($categories as $category)
                                                @if(\App\Product::getProduct($category->id,$c_brand->id) > 0)
                                                <li><a href="{{route('brand_listproduct',[strtolower($c_brand->name),strtolower(explode(' ',$category->name)[0])])}}"
                                                       @if($tab==strtolower(explode(' ',$category->name)[0]) && $name == $c_brand->name) class="active" @endif>{{$category->name}}</a></li>
                                                @endif
                                            @endforeach
                                        </ul>

                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <!-- compare link -->


                @if($cat != 'all' )
                    @if(session()->has('compare-'.strtolower(explode(' ',$cat->name)[0])))

                    <a href="{{route('product.comparison',strtolower(explode(' ',$cat->name)[0]))}}" class="compare-link" style="background-color: #353172 !important">compare
                        {{strtolower(explode(' ',$cat->name)[0])}}</a>
                     @endif
                @else
                    @if(session()->has('compare-'.$tab))

                        <a href="{{route('product.comparison',$tab)}}"  style="background-color: #353172 !important">compare
                            {{$tab}}</a>
                    @endif
                @endif
                <div class="col-sm-8">
                    <div class="products">
                        <div class="row">
                            @if($cat != 'all')
                                @foreach($cat->products->sortByDesc('getVariant.price') as $product)
                                    @if($product->status !='Hidden')
                                    @if($brand !='' && $product->brand->name != $brand->name) @continue; @endif
                                    <div class="col-6 col-xl-4 col-lg-6 col-sm-4 col-md-6">
                                        <div class="product-block">
                                            <div class="pdt-image">
                                                <a href="{{route('productdetail',[strtolower($product->brand->name),$product->slug])}}">
                                                    <img @if($product->getImage != null)
                                                         src="{{asset($product->getImage->thumbnail_path)}}" @endif alt="" class="img-fluid w-100" id="image{{$product->id}}">
                                                </a>
                                            </div>
                                            <div class="pdt-details">
                                                <p class="brand" id="brand[{{$product->id}}]">{{$product->brand->name}}</p>
                                                <p class="name" id="name[{{$product->id}}]">{{$product->name}}</p>
                                                <p class="price">
                                                    @if($product->offer != null && $product->offer_expire >= today())
                                                        <span class="offer">-{{$product->offer}}%</span><span
                                                        class="old-price">C${{$product->getVariant->price}}</span>
                                                        @php($price = $product->getVariant->price - ($product->getVariant->price*$product->offer/100))
                                                    @else
                                                        @php($price = $product->getVariant->price)
                                                    @endif
                                                    <span id="price[{{$product->id}}]">C${{$price}}</span>
                                                </p>
                                                <p hidden id="variant[{{$product->id}}]">{{$product->getVariant->value}}</p>
                                                <p hidden id="slug[{{$product->id}}]">{{$product->slug}}</p>

                                                @if($product->reviews->count()>0)
                                                    @php($rating = 0)
                                                    @foreach($product->reviews as $reviews)
                                                        @php($rating = $rating+$reviews->rating / Count($product->reviews))
                                                    @endforeach
                                                    <p hidden id="rating[{{$product->id}}]">{{$rating}}</p>
                                                @else
                                                    <p hidden id="rating[{{$product->id}}]">{{0}}</p>
                                                @endif

                                            </div>
                                            <div class="actions">
                                                <a href="#" onclick="addToCart({{$product->id}},'shop')" class="shop">shop now</a>
                                                <a href="{{route('compare.store',$product->id)}}" class="add">add to compare</a>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                @endforeach
                            @else
                                @foreach($brand->products->sortByDesc('getVariant.price') as $product)
                                    @if($product->status !='Hidden')
                                        <div class="col-6 col-xl-4 col-lg-6 col-sm-4 col-md-6">
                                            <div class="product-block">
                                                <div class="pdt-image">
                                                    <a href="{{route('productdetail',[strtolower($product->brand->name),$product->slug])}}">
                                                        <img @if($product->getImage != null)
                                                             src="{{asset($product->getImage->thumbnail_path)}}" @endif alt="" class="img-fluid w-100" id="image{{$product->id}}">
                                                    </a>
                                                </div>
                                                <div class="pdt-details">
                                                    <p class="brand" id="brand[{{$product->id}}]">{{$product->brand->name}}</p>
                                                    <p class="name" name="name[{{$product->id}}]" id="name[{{$product->id}}]">{{$product->name}}</p>
                                                    <p class="price">
                                                        @if($product->offer != null && $product->offer_expire >= today())
                                                            <span class="offer">-{{$product->offer}}%</span><span
                                                                class="old-price">C${{$product->getVariant->price}}</span>
                                                            @php($price = $product->getVariant->price - ($product->getVariant->price*$product->offer/100))
                                                        @else
                                                            @php($price = $product->getVariant->price)
                                                        @endif
                                                        <span id="price[{{$product->id}}]">C${{$price}}</span>
                                                    </p>
                                                    <p hidden id="variant[{{$product->id}}]">{{$product->getVariant->value}}</p>
                                                    <p hidden id="slug[{{$product->id}}]">{{$product->slug}}</p>

                                                    @if($product->reviews->count()>0)
                                                        @php($rating = 0)
                                                        @foreach($product->reviews as $reviews)
                                                            @php($rating = $rating+$reviews->rating / Count($product->reviews))
                                                        @endforeach
                                                        <p hidden id="rating[{{$product->id}}]">{{$rating}}</p>
                                                    @else
                                                        <p hidden id="rating[{{$product->id}}]">{{0}}</p>
                                                    @endif
                                                </div>
                                                <div class="actions">
                                                    <a href="#" onclick="addToCart({{$product->id}},'shop')" class="shop">shop now</a>
                                                    <a href="{{route('compare.store',$product->id)}}" class="add">add to compare</a>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="container">
            <div class="buy-back">
                <a href="">
                    <img src="{{asset('assets/img/buy-back-exchange.png')}}" alt="" class="img-fluid w-100">
                </a>
            </div>

        </div>

    </div>
@endsection
@section('additionalScripts')
    <script defer>
        function addToCart(id,tag) {
            $('#msg' + id).html('<i class="fa fa-cog fa-spin"></i> Adding');
            console.log(id);
            variant = null;
            rating = null;
            url = "/add-to-cart";
            var name = document.getElementById('name[' + id + ']').textContent;
            console.log(name);

            var quantity = 1;
            var price = document.getElementById('price[' + id + ']').textContent;
            price = price.slice(2);
            var variant = document.getElementById('variant[' + id + ']').textContent;
            var image = $('#image' + id).attr('src');
            var rating = document.getElementById('rating[' + id + ']').textContent;
            var slug = document.getElementById('slug[' + id + ']').textContent;
            var brand = document.getElementById('brand[' + id + ']').textContent;
            //console.log(rating);
            console.log(slug);
            console.log(image);
            //console.log(variant);

            $.ajax({
                url: url,
                method: 'post',
                data: {
                    product_id: id,
                    name: name,
                    quantity: quantity,
                    price: price,
                    variant: variant,
                    image: image,
                    rating: rating,
                    slug: slug,
                    brand: brand,
                    "_token": "{{ csrf_token() }}",
                },
                success: function (response) {
                    console.log(response)
                    if (response.success == 'success') {
                        $('#msg' + id).hide();
                        fireTost('Item Added To Cart');
                        console.log(response.cart)
                        $('#c_count span').text(response.cart);
                        if(tag == 'shop')
                            location.href= '/cart';
                    }
                },

            });
        }

        function fireTost(message) {
            const Toast1 = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
            Toast1.fire({
                icon: 'success',
                title: message,
            })
        }

        $('document').ready(function(){

        });
    </script>
@endsection
