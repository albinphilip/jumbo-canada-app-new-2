@extends('front.layouts.app')
@section('title','Jumbo Canada | Product Warranty re-registration')
@section('description','')
@section('keywords','')
@section('content')
    <div class="register page">
        <div class="register-inner">
            <h1>Re Register your product</h1>
            <form action="{{route('warranty-re-registration.store')}}" method="post" class="product-registration-form"
                  id="product-registration-form">
                @csrf
                <div class="row">
                    <div class="col-sm-6">
                        <label class="control-label" for="rma">RMA Number <small>*</small></label>
                        <div class="serial">
                            <input type="text" class="form-control" placeholder="1000"
                                   name="rma" value="{{ old('rma') }}" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label" for="email">Email Address <small>*</small></label>
                        <input type="email" class="form-control" required placeholder="Email Address" name="email"
                               value="{{ old('email') }}">
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label" for="serial_number">Serial Number <small>*</small></label>
                        <div class="serial">
                            <input type="text" class="form-control" required pattern=".{10,16}" placeholder="2005461413"
                                   name="serial_number" value="{{ old('serial_number') }}" required>

                            <img src="{{asset('assets/img/serailnum.png')}}" alt="" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label" for="safety_code">Safety Code <small>*</small></label>
                        <div class="serial">
                            <input type="text" class="form-control" required pattern="(PT21ULT.[0-9]{2,}|PT20ULT.[0-9]{3,4})"
                                   placeholder="PT20ULT9978"
                                   name="safety_code" value="{{ old('safety_code') }}" required>

                            <img src="{{asset('assets/img/safetycode.png')}}" alt="" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-6" style="margin-top: 20px">
                        <input type="checkbox" style="height: 20px; width: 10%" required="" id="agree"> <label
                            for="agree">I Agree
                            <a target="_blank"
                               href="{{route('policy','terms-and-conditions-of-warranty-and-safe-appliances-usage')}}">
                                Terms and Conditions</a></label>
                    </div>
                    <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
                    <div class="col-12">
                        <input type="submit" value="submit">
                    </div>
                </div>
            </form>
            <div class="col-12">
                <div class="response" id="pleaseWait" style="display: none;">
                    Please wait <i class="fa fa-cog fa-spin"></i>
                </div>
                <div id="subscribed"></div>
            </div>
        </div>
    </div>
@endsection
@section('additionalScripts')
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute("{{env('GOOGLE_RECAPTCHA_KEY')}}", {action: "productRegistration"}).then(function (token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                recaptchaResponse.value = token;
            });
        });
    </script>
    <!-- InputMask -->
    <script src="{{asset('/plugins/inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>
    <script>
        $('[data-mask]').inputmask()
    </script>
@endsection

