@extends('front.emails.user-mail')
@section('preText')
    Retrun request for {{$return->product_registration->product->name}} has been approved by the store.
@section('body')
    Hi ,
    <p>Return request for {{$return->product_registration->product->name}} has been approved at the store
        ({{$return->product_registration->store->address}}) and faulty product is replaced.</p>
    <p>Comments from the store: {{$return->reject_reason}}</p>
    <p><b>Return request detail</b></p>
    RMA: {{$return->return_id}}<br/>
    Item: {{$return->product_registration->product->name}}<br/>
    Serial No: {{$return->product_registration->serial_number}}<br/>
    Safety Code: {{$return->product_registration->safety_code}}<br/>
    Product Code: {{$return->product_registration->product_code}}<br/>
    Purchase Date: {{date('M d, Y',strtotime($return->product_registration->purchase_date))}}<br/>
    Purchased From: {{$return->product_registration->store->address}}, {{$return->product_registration->store->city}}, <a
        href="tel:{{$return->product_registration->store->phone}}">{{$return->product_registration->store->phone}}</a><br/>
    Warranty Reg No: {{$return->product_registration->reg_no}}<br/>
    Registration Date: {{date('M d, Y',strtotime($return->product_registration->created_at))}}<br/>
    Reason for returning: {{$return->dead}}<br/>
    {{$return->faulty}}<br/>
    Customer: {{$return->product_registration->first_name}} {{$return->product_registration->last_name}}<br/>
    Phone: <a href="tel:{{$return->product_registration->phone ?? ''}}">{{$return->product_registration->phone ?? ''}}</a><br/>
    Email: {{$return->product_registration->email}}

@endsection
