@extends('front.emails.user-mail')
@section('preText')
    We have received your warranty registration request for {{$data->product->name}}. You will be receiving a confirmation e-mail soon.
@endsection
@section('body')
    <P>
        Dear {{$data['first_name'] ?? ''}}, <br/>
        Thank you for registering your {{$data->product->name}} for warranty. <br>
        This email is to acknowledge that we have received your product registration request and are currently reviewing
        it.You will be receiving a confirmation e-mail soon.<br>
        <b>Registration Details are given below</b><br/>
        Product: {{$data->product->name}}<br/>
        Serial Number: {{$data->serial_number}}<br/>
        Product Code: {{$data->product_code}}<br/>
        Safety Code: {{$data->safety_code}}<br/>
        Date of Purchase: {{date('M d, Y',strtotime($data->purchase_date))}}<br/>
        Place of Purchase:{{$data->store->address}}, {{$data->store->city}}<br/>
    </P>
@endsection
