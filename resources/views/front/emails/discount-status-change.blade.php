@extends('front.emails.user-mail')
@section('preText')
   Discount request status update from jumbocanada
@endsection
@section('body')
    Hi, {{$data ?? ''}},
    <p>{{$msg ?? ''}}</p>
@endsection
