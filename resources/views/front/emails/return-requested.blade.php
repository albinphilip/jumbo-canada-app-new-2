@extends('front.emails.user-mail')
@section('preText')
    We have received your request to return the product {{$registration->product->name}}. Our representative will contact you soon
        @endsection
        @section('body')
            Hi {{$registration->first_name}} {{$registration->last_name}},
            <p>We have received your request to return the product <b>{{$registration->product->name}}</b> with product warranty registration number: {{$registration->reg_no}}, registered on {{date('M d, Y',strtotime($registration->created_at))}}.</p>
            <p>We are processing your request and our representatives will get back to you soon. In case of emergency please give us a call on <a href="tel:+1 8557733844"> +1 (855) 773 3844</a></p>
@endsection
