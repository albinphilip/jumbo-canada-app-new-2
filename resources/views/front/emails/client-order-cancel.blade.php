@extends('front.emails.user-mail')
@section('preText')
    {{$order->order_id}} - Cancel request Product: {{$order->product->name}} Customer: {{$order->customer->firstname}} {{$order->customer->lastname}}  Email: {{$order->customer->email}} Phone: {{$order->customer->telephone}}
@endsection
@section('body')
    Hi, you have new order cancel request <br/>
    Order ID: {{$order->order_id}}<br/>
    Product: {{$order->product->name}}<br/>
    Customer: {{$order->customer->firstname}} {{$order->customer->lastname}}<br/>
    Email: {{$order->customer->email}}<br/>
    Phone: {{$order->customer->telephone}}<br/>
@endsection
