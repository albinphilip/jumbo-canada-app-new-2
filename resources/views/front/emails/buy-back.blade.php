@extends('front.emails.user-mail')
@section('preText')
    Thank you for participating in the Go-Green initiative with jumbocanada.com. We have received your invoice copy and are currently processing it.
    @endsection
@section('body')
    Dear {{$name ?? ''}}, <br/>
    Thank you for participating in the <b>Go-Green initiative with jumbocanada.com</b><br/>
    We have received your invoice copy and are currently processing it. Discount coupon will be sent you after verification of your details.
    <br>
    <p>If you have any query, please give us a call on <a
            href="tel:+1 8557733844"> +1 (855) 773 3844</a></p>
@endsection
 Thank you for participating in the Go-Green initiative with jumbocanada.com. We have received your invoice copy and are currently processing it.
