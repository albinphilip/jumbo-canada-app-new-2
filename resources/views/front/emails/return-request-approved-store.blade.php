@extends('front.emails.user-mail')
@section('preText')
    Your request to return {{$return->product_registration->product->name}} has been approved by store.
@section('body')
    Hi {{$return->product_registration->first_name}} {{$return->product_registration->last_name}},
    <p>Your request to return {{$return->product_registration->product->name}} has been approved at the store and hope you received the replacement.</p>
    <p>Please register your replaced product for warranty by following <a href="{{route('warranty-re-registration')}}"> the link.</a></p>
    <p>Please note your RMA number is <b>{{$return->return_id}}.</b></p>
    <p>You may contact our customer care <a href="tel:+1 8557733844"> +1 (855) 773 3844</a> for assistance.
    </p>
@endsection
