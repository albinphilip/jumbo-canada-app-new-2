@extends('front.emails.user-mail')
@section('preText')
    We are sorry to inform you that your request to return the product {{$return->product_registration->product->name}} has been rejected.
@section('body')
    Hi {{$return->product_registration->first_name}} {{$return->product_registration->last_name}},
    <p>We are sorry to inform you that your request to return the
        product {{$return->product_registration->product->name}} has been rejected due to the following reason:<br/>
        {{$return->reject_reason}}</p>
    <p>You may contact our customer care <a href="tel:+1 8557733844"> +1 (855) 773 3844</a> for further clarification.
    </p>
    <p>Please note your return request id is <b>{{$return->return_id}}</b></p>
@endsection
