@extends('front.emails.user-mail')
@section('preText')
    We have shipped your {{$order->product->name}} via {{$order->shipping_partner}} Shipping Number is: {{$order->shippment_number}} on {{date('d-M-Y',strtotime($order->shipping_date))}}
@endsection
@section('body')
    Hi {{$order->customer->firstname ?? ''}},
    <p>We have shipped your {{$order->product->name}} via {{$order->shipping_partner}} on {{date('d-M-Y',strtotime($order->shipping_date))}}. Your order is on the way.</p>
    @if($order->shipping_partner != 'Canada Post')
        <p>Shipping Number is: {{$order->shippment_number}}</p>
    @else
        <p>Shipping Number is: {{$order->shipment_id}}</p>
        <p> Tracking pin is : {{$order->tracking_pin}} <br/>
            @php($id = base64_encode($order->order_id))
            <a href="{{route('order-confirmation',$id)}}#track_button" target="_blank">Click here to track your shipment</a>.</p>
    @endif
    <p>After receiving the product please register the product for warranty by following <a
            href="{{route('warranty-registration')}}"> the link.</a></p>
    <p><i>Please choose Place Of Purchase as Preethi Canada Online Store, Toronto in warranty registration page.</i></p>
@endsection
