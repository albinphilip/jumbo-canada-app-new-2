@extends('front.emails.user-mail')
@section('preText')
    Link to reset your password of jumbocanada.com account. This link will remain valid for 24 hours after receiving this email. If you didn’t make this request, please
    ignore this email, your password has not been changed.
@endsection
@section('body')
    <p>Hi {{$name ?? ''}},</p>
    <p>Click on the button below to reset the password for your jumbocanada.com account.</p>
    <a href="{{$reset_link ?? ''}}"
       target="_blank"
       style="padding: 8px 12px; border: 1px solid #ED2939;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;text-decoration: none;font-weight:bold;display: inline-block; background-color: #E51F25">
        Reset password link
    </a>
    <p>This link will remain valid for 24 hours after receiving this email. If you didn’t make this request, please
        ignore this email, your password has not been changed.</p>
@endsection
