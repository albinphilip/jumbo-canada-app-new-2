@extends('front.emails.user-mail')
@section('preText')
    Your request to register {{$data['product_name'] ?? ''}} for warranty has been approved and it is applicable for 1 year from the date of purchase
@endsection
@section('body')
    <P>
        Dear {{$data['first_name'] ?? ''}}, <br/>
        Your request to register {{$data['product_name'] ?? ''}} for warranty has been approved and it is applicable for
        1 year from the date of purchase. <br>
        <b>Product Detail</b><br/>
        Product: {{$data['product_name'] ?? ''}}<br/>
        Serial Number: {{$data['serial_number']}}<br/>
        Product Code: {{$data['product_code']}}<br/>
        Safety Code: {{$data['safety_code']}}<br/>
        Date of Purchase: {{date('M d,Y',strtotime($data['purchase_date']))}}<br/>
        <b>Registration Number: {{$data['reg_no']}}</b>
    </P>
@endsection
