@extends('front.emails.user-mail')
@section('preText')
    Product Repair request status update from Jumbocanada
@endsection
@section('body')
    Hi, {{$data['first_name'] ?? ''}},
    <p>{{$msg ?? ''}} <br/>
    For more details you can call us on <a
            href="tel:+1 8557733844"> +1 (855) 773 3844</a></p>
@endsection
