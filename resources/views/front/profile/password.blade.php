@extends('front.layouts.profile')
@section('subtitle','Forgot Password | Jumbo Canada')
@section('description',"Jumbo Canada Account. Forgot your password? Don’t worry you can reset the password here.")
@section('keywords','')
@section('profileHead','Reset Password')
@section('profileContent')
    @php($customer=Auth::guard('customer')->user())
    <form action="{{route('resetPassword')}}" method="POST">
        @csrf
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12"><input type="password" placeholder="Current Password"
                                                  class="form-control {{ $errors->has('current') ? 'has-error' : ''}}"
                                                  name="current" required>
                        {!! $errors->first('current', '<p class="help-block" style="color:red;">:message</p>') !!}
                    </div>
                    <div class="col-sm-12"><input type="password" placeholder="New Password"
                                                  class="form-control {{ $errors->has('password') ? 'has-error' : ''}}"
                                                  name="password" required>
                        {!! $errors->first('password', '<p class="help-block" style="color:red;">:message</p>') !!}
                    </div>
                    <div class="col-sm-12">
                        <p class="password">Password must be of minimum length 8-16 and must has at least 1 digit
                            and 1 character</p>
                    </div>
                    <div class="col-sm-12"><input type="password" placeholder="Confirm New Password"
                                                  class="form-control {{ $errors->has('confirm_password') ? 'has-error' : ''}}"
                                                  name="confirm_password" required>
                        {!! $errors->first('confirm_password', '<p class="help-block" style="color:red;">:message</p>') !!}
                    </div>
                </div>
            </div>
            <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
        </div>
        <input type="submit" value="update">
    </form>


@endsection
@section('additionalScripts')
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute("{{env('GOOGLE_RECAPTCHA_KEY')}}", {action: "PasswordReset2"}).then(function (token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                recaptchaResponse.value = token;
            });
        });
    </script>
@endsection
