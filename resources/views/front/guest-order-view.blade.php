@extends('front.layouts.app')
@section('title','Order Details | Jumbo Canada')
@section('description','Check the order summary here. Jumbo Canada order details')
@section('keywords','')
@section('content')
    <div class="profile page">
        <div class="container-fluid">
            <h1>View Order Details</h1>
            <form action="{{route('guest.orderDetails')}}" method="POST" name="orderDetails" id="orderDetails">
                @csrf
                @if(Session::has('email') && Session::has('order_id'))
                    @php($email = Session::get('email'))
                    @php($order_id = Session::get('order_id'))
                    @php(session()->forget('email'))
                    @php(session()->forget('order_id'))
                @endif
                <div class="row">
                    <div class="col-sm-4">
                        <input type="text"  class="form-control" required placeholder="Order Id" id="order_id" name="order_id" @isset($order_id) value="{{$order_id}}" @endisset>
                        {!! $errors->first('order_id', '<p class="help-block" style="color:red;">:message</p>') !!}
                    </div>
                    <div class="col-sm-4">
                        <input type="email"  class="form-control" required placeholder="Email Id" name="email" id="email"
                               @isset($email) value="{{$email}}" @endisset>
                        {!! $errors->first('email', '<p class="help-block" style="color:red;">:message</p>') !!}

                    </div>
                    <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
                    <div class="col-sm-4 pb-5" style="margin-top: -5px;!important;">
                        <input type="submit"  value="submit">
                    </div>

                </div>
            </form>
        </div>
    </div>
@endsection
@section('additionalScripts')
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute("{{env('GOOGLE_RECAPTCHA_KEY')}}", { action: "OrderDetail" }).then(function (token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                recaptchaResponse.value = token;
            });
        });


        window.onload=function(){
            if (($('#email').val() !== '')&&($('#order_id').val() !== '') ){
                window.setTimeout(function() { document.orderDetails.submit(); }, 2000);
            }
        };
    </script>
@endsection
