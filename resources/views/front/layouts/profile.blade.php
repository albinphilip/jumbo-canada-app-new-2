@extends('front.layouts.app')
@php($customer=Auth::guard('customer')->user())
@section('title'){{$customer->firstname}}  {{$customer->lastname}} @yield('subtitle')@endsection
@section('description') @yield('description') @endsection
@section('keywords') @yield('keywords') @endsection

@section('content')
    <div class="profile page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="user">
                        <h2><img src="{{asset('assets/img/sign-in.png')}}" alt="" class="img-fluid"> {{$customer->firstname.' '.$customer->lastname}} !!</h2>
                    </div>
                </div>
                <div class="col-sm-3">
                    <ul class="profile-links">
                        <li><a href="{{route('edit-profile')}}" class="{{ Request::path() == 'edit-profile' ? 'active' : '' }}">My Profile</a></li>
                        <li><a href="{{route('address')}}" class="{{ Request::path() == 'address' ? 'active' : '' }}" >My Address Book</a></li>
                        <li><a href="{{route('customer-password')}}" class="{{ Request::path() == 'reset-customer-password' ? 'active' : '' }}" >Password</a></li>
                        <li><a href="{{route('wishlist.index')}}" class="{{ Request::path() == 'wishlist' ? 'active' : '' }}" >My Wish Lists</a></li>
                        <li><a href="{{route('cart.index')}}" class="{{ Request::path() == 'cart' ? 'active' : '' }}">My Saved Carts</a></li>
                        <li><a href="{{route('orderDetails')}}" class="{{ Request::path() == 'order-details' ? 'active' : '' }}">Order History</a></li>
                    </ul>
                </div>
                <div class="col-sm-9">
                    <h1>@yield('profileHead')</h1>
                    <div class="profile-inner">
                        @yield('profileContent')
                    </div>
                    @if(Session::has('message'))
                        <p style="color:red;">{{Session::get('message')}}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
