@extends('admin.layouts.datatable')
@section('title','Product Banner')
@section('tableTitle','Product Banner')
@section('createRoute')
    {{route('product-banner.create')}}
@endsection
@section('button')
    <a class="btn btn-primary btn-sm mr-2" href="@yield('createRoute')">Add new</a>
@endsection
@section('tableHead')
    <th>#</th>
    <th>Photo</th>
    <th>Action</th>
@endsection
@section('tableBody')
    @foreach($banners->sortByDesc('id') as $banner)
        <tr>
            <td>{{$banner->id}}</td>
            <td><img src="{{asset($banner->banner_image)}}" width="200px" class="banner_image"></td>
            <td>
                <form action="{{route('product-banner.destroy',$banner->id)}}" method="POST" id="delete-form-{{$banner->id}}">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <a href="#" onclick="return confirmation({{$banner->id}});"><i class="fa fa-trash"></i> </a>
                </form>
            </td>
        </tr>
    @endforeach
@endsection
