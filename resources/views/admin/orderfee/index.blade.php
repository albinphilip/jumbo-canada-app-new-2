@extends('admin.layouts.datatable')
@section('title','Order Fee Split up')
@section('tableTitle','Order Fee Split up')
@section('tableHead')
    <th>#</th>
    <th>Order Id</th>
    <th>Products</th>
    <th>Product Price</th>
    <th>Quantity</th>
    <th>Sub Total</th>
    <th>Shipping Charge</th>
    <th>Tax (%)</th>
    <th>Reduction</th>
    <th>Order Total</th>
@endsection
@section('tableBody')
    @foreach($items as $item)
        <tr>
            <td></td>
            <td>{{$item->order_id}}</td>
            <td>{{$item->product->name}}</td>
            <td>{{$item->product_price}}</td>
            <td>{{$item->quantity}}</td>
            <td>{{$item->orderFeesplitup->sub_total}}</td>
            <td>{{$item->orderFeesplitup->shipping_charge}}</td>
            <td>{{$item->orderFeesplitup->tax}}</td>
            <td>{{$item->orderFeesplitup->reduction}}</td>
            <td>{{$item->order_total}}</td>
        </tr>
    @endforeach


@endsection
