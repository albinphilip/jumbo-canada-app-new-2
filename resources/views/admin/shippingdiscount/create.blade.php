@extends('admin.layouts.form')
@section('additionalStyles')
    <link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.css')}}">
@endsection
@section('title','Add new Flat Shipping Rate')
@section('actionUrl')
    {{route('shippingdiscount.store')}}
@endsection

<!-- use only for form with file upload -->
@section('encrypt','enctype=multipart/form-data')
<!------  --    ----------------------- -->
@section('actionName','Add')
@section('indexRoute')
    {{route('shippingdiscount.index')}}
@endsection
@section('formBody')
   <!-- <div class="col-6">
        <div class="form-group">
            <label for="code" class="control-label">Code</label>
            <input type="text" id="code" name="code" required class="form-control {{$errors->has('code') ? 'is-invalid' : ''}}" value="{{old('code')}}">
            @if($errors->has('code'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('code')}}</strong>
                </span>
            @endif
        </div>
    </div>-->
    <div class="col-6">
        <div class="form-group">
            <label for="expires_on" class="control-label">Expires On</label>
            <input type="date" id="expires_on" name="expires_on" required class="form-control {{$errors->has('expires_on') ? 'is-invalid' : ''}}" value="{{old('expires_on')}}">
            @if($errors->has('expires_on'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('expires_on')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="applicable_to" class="control-label">Applicable To</label>
            <select id="applicable_to" name="applicable_to" required class="form-control {{$errors->has('applicable_to') ? 'is-invalid' : ''}}">
                <option value="">Select</option>
                <option value="All">All</option>
                <option value="New Customers">New Customers</option>
                <option value="Existing Customers">Existing Customers</option>
            </select>
            @if($errors->has('applicable_to'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('applicable_to')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="value" class="control-label">Flat Shipping Rate</label>
            <input type="number" id="value" name="value" required class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}" value="{{old('value')}}">
            @if($errors->has('value'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('value')}}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="col-6">
        <div class="form-group">
            <label for="category" class="control-label">Product Category</label>
            <select name="category" class="form-control" id="category">
                <option value="">Select</option>
                <option value="All">All</option>
                    @foreach($categories->sortBy('name') as $category)
                        <option value="{{$category->name}}">{{$category->name}}</option>
                    @endforeach
            </select>
            @if($errors->has('category'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('category')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="applicable_product" class="control-label">Applicable Product <small> (If discount applicable to a specific product)</small></label>
            <select class="select2bs4 form-control"  name="applicable_product" id="product">
                <option value="" selected>Select</option>

                @foreach($products->sortBy('name') as $product)
                    <option value="{{$product->id}}">{{$product->name}}</option>
                @endforeach
            </select>
            @if($errors->has('applicable_product'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('applicable_product')}}</strong>
                </span>
            @endif
        </div>
    </div>
@endsection
@section('additionalScripts')
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2();

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        });
        $('#product').on('change',function(){
            console.log('pp');
            if($(this).val() != '') {
                $('#category').attr({'disabled': 'disabled'});
                $('#category').val('');
            }
            else{
                console.log('kkk');
                $('#category').removeAttr('disabled');

            }
        });
    </script>
@endsection
