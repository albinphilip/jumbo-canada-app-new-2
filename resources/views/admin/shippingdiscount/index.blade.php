@extends('admin.layouts.datatable')
@section('title','Flat Shipping Rate')
@section('tableTitle','Flat Shipping Rate')
@section('createRoute')
    {{route('shippingdiscount.create')}}
@endsection
@section('button')
    <a class="btn btn-primary btn-sm mr-2" href="@yield('createRoute')">Add new</a>
@endsection
@section('tableHead')
    <th>#</th>
    <th>Code</th>
    <th>Expiry Date</th>
    <th>Applicable To</th>
    <th>Category</th>
    <th>Value</th>
    <th>Action</th>
    <th>Applicable Product</th>
    <th>Status</th>
@endsection
@section('tableBody')
    @foreach($codes->sortByDesc('id') as $code)
        <tr>
            <td></td>
            <td>{{$code->code}}</td>
            <td>{{$code->expires_on}}</td>
            <td>{{$code->applicable_to}}</td>
            <td>{{$code->category}}</td>
            <td>{{$code->value}}</td>
            <td>
                <form action="{{route('shippingdiscount.destroy',$code->id)}}" method="POST" id="delete-form-{{$code->id}}">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <a href="#" onclick="return confirmation({{$code->id}});"><i class="fa fa-trash"></i> </a>
                </form>
                <a href="{{route('shippingdiscount.edit',$code->id)}}"><i class="fa fa-edit"></i> </a>
            </td>
            <td>{{($code->applicable_product != '' )?  $code->product->name : ''}}</td>
            <td>{{$code->status}}</td>

        </tr>
    @endforeach
@endsection
