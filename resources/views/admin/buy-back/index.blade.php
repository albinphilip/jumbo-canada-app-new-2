@extends('admin.layouts.datatable')
@section('title','Buy back')
@section('tableTitle','Buy back')
@section('tableHead')
    <th>#</th>
    <th>Product Condition</th>
    <th>Serial Number</th>
    <th>Attachment</th>
    <th>Name</th>
    <th>Phone</th>
    <th>Email</th>
    <th>Action</th>
@endsection
@section('tableBody')
    @foreach($buy_backs->sortByDesc('id') as $buy_back)
        <tr>
            <td></td>
            <td>{{$buy_back->reason}}</td>
            <td>{{$buy_back->serial_number}}</td>
            <td><a href="{{asset($buy_back->attached_file)}}" target="_blank"><i class="fa fa-paperclip"></i></a> </td>
            <td>{{$buy_back->name}}</td>
            <td>{{$buy_back->phone}}</td>
            <td>{{$buy_back->email}}</td>
            <td><button class="btn btn-primary btn-sm" data-toggle="modal"
                        data-id="{{$buy_back->id}}" data-target="#couponModel"
                        @if($buy_back->status == 'Approved') disabled @endif>{{$buy_back->status == 'Approved' ? 'Approved' : 'Approve'}}</button> </td>
        </tr>
    @endforeach
    <!-- Modal -->
    <div class="modal fade" id="couponModel" tabindex="-1" role="dialog"
         aria-labelledby="consultation-modal"  data-backdrop="false"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Discount Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="body-inner">
                        <form action="{{route('buyback.approve')}}" method="post" name="buyback" id="buybackform">
                            @csrf
                            <div class="row">
                                <div class="col-sm-6" hidden>
                                    <input type="text" name="buyback_id" id="buyback_id" value="" class="form-control">
                                    <span class="text-danger">
                                        <small><strong id="buyback_id"></strong></small>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <input type="number" placeholder="Discount %" name="discount" id="discount"  required class="form-control"
                                           value="">
                                    <span class="text-danger">
                                        <small><strong id="discount-error"></strong></small>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <input type="date" placeholder="Expiry Date" name="expiry" id="expiry"  required class="form-control"
                                           value="">
                                    <span class="text-danger">
                                        <small><strong id="expiry-error"></strong></small>
                                    </span>
                                </div>
                                <div class="col-6 pt-2">
                                    <select name="category" id="category" class="form-control" required>
                                        <option value="">Select Category</option>
                                        <option value="All">All</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->name}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger">
                                        <small><strong id="category-error"></strong></small>
                                    </span>
                                </div>
                            </div>

                            <div class="actions">
                                <ul class="list-inline pt-2">
                                    <li class="list-inline-item">
                                        <button type="button" data-dismiss="modal"
                                                aria-label="Close" class="form-control">cancel
                                        </button>
                                    </li>
                                    <li class="list-inline-item">
                                        <a class="btn btn-primary" onclick="formSubmit();">Submit</a>
                                    </li>
                                    <li style="display:none;" id="response"><i class="fa fa-cog fa-spin"></i>Please Wait</li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('additionalScripts')
    <script>
        $(document).ready(function(){
            $('#couponModel').on("show.bs.modal", function (e) {

                $("#buyback_id").val($(e.relatedTarget).data('id'));
            });
        });

        function formSubmit()
        {
            buyback_id = $('#buyback_id').val();
            discount = $('#discount').val();
            expiry = $('#expiry').val();
            category = $('#category').val();

            if(!discount) $('#discount-error').html('Enter Discount %');
            else if(!expiry) $('#expiry-error').html('Enter Expiry Date');
            else if(!category) $('#category-error').html('Select Category %');
            else {
                $('#response').css('display','block');
                url = '/admin/buyback/approve';
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
                    data: {_token: '{{csrf_token()}}',_method:"post",discount: discount,expiry:expiry,buyback_id:buyback_id,category:category},
                    success: function (result) {
                        if(result == 'success')
                        {
                            $('#couponModel').modal('hide');
                            fireTost('Buyback Request Approved');
                            setInterval('location.reload()', 2000);
                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("Failed to Approve buyback");
                        $('#response').css('display','none');
                        //$('#buybackform')[0].reset();

                    }
                });
            }

        }

        function fireTost(message) {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
            Toast.fire({
                icon: 'success',
                title: message,
            })
        }
    </script>
@endsection
