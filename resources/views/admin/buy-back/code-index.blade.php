@extends('admin.layouts.datatable')
@section('title','Buy back discount code')
@section('tableTitle','Buy back discount code')
@section('tableHead')
    <th>#</th>
    <th>Buy back id</th>
    <th>code</th>
    <th>Discount</th>
    <th>Status</th>
@endsection
@section('tableBody')
    @foreach($buy_back_codes->sortByDesc('id') as $buy_back_code)
        <tr>
            <td>{{$buy_back_code->id}}</td>
            <td>{{$buy_back_code->buy_back_id}}</td>
            <td>{{$buy_back_code->code}}</td>
            <td>{{$buy_back_code->discount}}</td>
            <td>{{$buy_back_code->status}}</td>
        </tr>
    @endforeach
@endsection
