@extends('admin.layouts.form')
@section('title','Edit Category')
@section('actionUrl')
    {{route('category.update',$category)}}
@endsection

<!-- use only for form with file upload -->
@section('encrypt','enctype=multipart/form-data')
<!------  --    ----------------------- -->
@section('actionName','Update Category')
@section('indexRoute')
    {{route('category.index')}}
@endsection
@section('formBody')
    <!-- this is needed in edit form -->
    <input type="hidden" name="_method" value="PUT">
    <!-- ---------------------------- -->
    <div class="col-6">
        <div class="form-group">
            <label for="name" class="control-label">Name</label>
            <input type="text" id="name" name="name" required class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" value="{{$category->name}}">
            @if($errors->has('name'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('name')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="icon">Icon <small>(Not less than 38 X 47 px)</small></label>
            <div class="input-group">
                <div class="custom-file">
                    <input type="file" class="custom-file-input {{$errors->has('icon') ? 'is-invalid' : ''}}" id="icon" name="icon" value="{{old('icon')}}" >
                    <label class="custom-file-label" for="icon">Choose file</label>
                </div>
            </div>
            @if($errors->has('icon'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('icon')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="description" class="control-label">Description <small>(Do not capitalise entire sentence)</small></label>
            <textarea id="description" name="description"
                      class="form-control  {{$errors->has('description') ? 'is-invalid' : ''}}">{{$category->description}}
            </textarea>
            @if($errors->has('description'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('description')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label for="keywords" class="control-label">Keywords <small>(Do not capitalise entire sentence)</small></label>
            <textarea id="keywords" name="keywords"
                      class="form-control  {{$errors->has('keywords') ? 'is-invalid' : ''}}">{{$category->keywords}}
            </textarea>
            @if($errors->has('keywords'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('keywords')}}</strong>
                </span>
            @endif
        </div>
    </div>

@endsection
