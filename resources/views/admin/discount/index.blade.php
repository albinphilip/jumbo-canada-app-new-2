@extends('admin.layouts.datatable')
@section('title','Discount Request')
@section('tableTitle','Discount Request')
@section('tableHead')
    <th>#</th>
    <th>Name</th>
    <th>Email</th>
    <th>Phone</th>
    <th>Type</th>
    <th>Proof</th>
    <th>Approve/Reject</th>
    <th>Address</th>
    <th>Province</th>
@endsection
@section('tableBody')
    @foreach($discounts->sortByDesc('id') as $discount)
        <tr>
            <td></td>
            <td>{{$discount->name}}</td>
            <td>{{$discount->email}}</td>
            <td>{{$discount->phone}}</td>
            <td>{{$discount->current_status}}</td>
            <td><a href="{{asset($discount->proof)}}" target="_blank"><i class="fa fa-paperclip"></i></a></td>
            <td>
                <div class="col-sm-6" @if($discount->status == 'Rejected') hidden @endif >
                    <button class="btn btn-primary btn-sm" data-toggle="modal"
                            data-id="{{$discount->id}}" data-target="#couponModel"
                            @if($discount->status == 'Approved') disabled @endif>{{$discount->status == 'Approved' ? 'Approved' : 'Approve'}}</button>

                </div>
                <div class="col-sm-6 pt-3" @if($discount->status == 'Approved') hidden @endif>
                    <button class="btn btn-primary btn-sm pt-2" data-toggle="modal"
                            data-dis="{{$discount->id}}" data-target="#rejectionModal"
                            @if($discount->status == 'Rejected') disabled @endif>{{$discount->status == 'Rejected' ? 'Rejected' : 'Reject'}}</button>

                    @if($discount->status == 'Rejected')
                        <span><a href="{{route('discount.edit',$discount->id)}}"><i class="fa fa-edit"></i> </a></span>
                    @endif
                </div>
            </td>
            <td>
                {{$discount->address_1}}<br/>
                {{$discount->address_2}}<br/>
                {{$discount->city}}<br/>
                {{$discount->postcode}}<br/>
            </td>
            <td>{{$discount->province}}</td>
        </tr>
    @endforeach
    <!-- approval Modal -->
    <div class="modal fade" id="couponModel" tabindex="-1" role="dialog"
         aria-labelledby="consultation-modal" data-backdrop="false"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Discount Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="body-inner">
                        <form action="{{route('discount.approve')}}" method="post" name="discount" id="approvalForm">
                            @csrf
                            <div class="row">
                                <div class="col-sm-6" hidden>
                                    <input type="text" name="discount_id" id="discount_id" value=""
                                           class="form-control">
                                    <span class="text-danger">
                                        <small><strong id="discount_id"></strong></small>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <input type="number" placeholder="Discount %" name="discount" id="discount" required
                                           class="form-control"
                                           value="">
                                    <span class="text-danger">
                                        <small><strong id="discount-error"></strong></small>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <input type="date" placeholder="Expiry Date" name="expiry" id="expiry" required
                                           class="form-control"
                                           value="">
                                    <span class="text-danger">
                                        <small><strong id="expiry-error"></strong></small>
                                    </span>
                                </div>
                                <div class="col-6 pt-2">
                                    <select name="category" id="category" class="form-control" required>
                                        <option value="">Select Category</option>
                                        <option value="All">All</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->name}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger">
                                        <small><strong id="category-error"></strong></small>
                                    </span>
                                </div>
                            </div>

                            <div class="actions">
                                <ul class="list-inline pt-2">
                                    <li class="list-inline-item">
                                        <button type="button" data-dismiss="modal"
                                                aria-label="Close" class="form-control">cancel
                                        </button>
                                    </li>
                                    <li class="list-inline-item">
                                        <a class="btn btn-primary" onclick="formSubmit();">Submit</a>
                                    </li>
                                    <li style="display:none;" id="response"><i class="fa fa-cog fa-spin"></i>Please Wait
                                    </li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- rejection Modal -->
    <div class="modal fade" id="rejectionModal" tabindex="-1" role="dialog"
         aria-labelledby="consultation-modal" data-backdrop="false"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Rejection Reason</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="body-inner">
                        <form action="{{route('discount.reject')}}" method="post" name="discount" id="approvalForm">
                            @csrf
                            <div class="row">
                                <div class="col-sm-6" hidden>
                                    <input type="text" name="rej_discount_id" id="rej_discount_id" value=""
                                           class="form-control">
                                    <span class="text-danger">
                                        <small><strong id="rej_discount_id"></strong></small>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <select name="reason" id="reason" class="form-control" required>
                                        <option value="">Select Reason</option>
                                        <option value="Id Proof not clear">Id Proof not clear</option>
                                        <option value="Wrong ID Proof">Wrong ID Proof</option>
                                        <option value="Currently No offers">Currently No offers</option>
                                        <option value="Fraudulent entry">Fraudulent entry</option>
                                        <option value="Duplicate entry">Duplicate entry</option>
                                        <option value="Rejected with no reason">Rejected with no reason</option>
                                    </select>
                                    <span class="text-danger">
                                        <small><strong id="reason-error"></strong></small>
                                </span>
                                </div>
                            </div>
                            <div class="actions">
                                <ul class="list-inline pt-2">
                                    <li class="list-inline-item">
                                        <button type="button" data-dismiss="modal"
                                                aria-label="Close" class="form-control">cancel
                                        </button>
                                    </li>
                                    <li class="list-inline-item">
                                        <a class="btn btn-primary" onclick="reasonSubmit();">Submit</a>
                                    </li>
                                    <li style="display:none;" id="rej-response"><i class="fa fa-cog fa-spin"></i>Please
                                        Wait
                                    </li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('evenMoreScripts')
    <script>
        $(document).ready(function () {
            $('#couponModel').on("show.bs.modal", function (e) {

                $("#discount_id").val($(e.relatedTarget).data('id'));
            });
            $('#rejectionModal').on("show.bs.modal", function (e) {

                $("#rej_discount_id").val($(e.relatedTarget).data('dis'));
            });
        });

        function formSubmit() {
            discount_id = $('#discount_id').val();
            discount = $('#discount').val();
            expiry = $('#expiry').val();
            category = $('#category').val();

            if (!discount) $('#discount-error').html('Enter Discount %');
            else if (!expiry) $('#expiry-error').html('Enter Expiry Date');
            else if (!category) $('#category-error').html('Select Category');
            else {
                $('#response').css('display', 'block');
                url = '/admin/discount/approve';
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
                    data: {
                        _token: '{{csrf_token()}}',
                        _method: "post",
                        discount: discount,
                        expiry: expiry,
                        id: discount_id,
                        category: category
                    },
                    success: function (result) {
                        if (result == 'success') {
                            $('#couponModel').modal('hide');
                            fireTost('Discount Request Approved');
                            setInterval('location.reload()', 2000);
                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("Failed to Approve request");
                        $('#response').css('display', 'none');
                        //$('#buybackform')[0].reset();

                    }
                });
            }

        }

        function reasonSubmit() {
            discount_id = $('#rej_discount_id').val();
            console.log(discount_id);
            reason = $('#reason').val();

            if (!reason) $('#reason-error').html('Select reason');
            else {
                $('#rej-response').css('display', 'block');
                url = '{{route('discount.reject')}}';
                $.ajax({
                    type: "POST",
                    url: url,
                    cache: false,
                    data: {_token: '{{csrf_token()}}', _method: "post", id: discount_id, reason: reason},
                    success: function (result) {
                        if (result == 'success') {
                            $('#couponModel').modal('hide');
                            fireTost('Discount Request rejected');
                            setInterval('location.reload()', 2000);
                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("Failed to reject request");
                        $('#response').css('display', 'none');
                        //$('#buybackform')[0].reset();

                    }
                });
            }

        }

        function fireTost(message) {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
            Toast.fire({
                icon: 'success',
                title: message,
            })
        }
    </script>
@endsection

