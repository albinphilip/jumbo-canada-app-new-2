@extends('admin.layouts.form')
@section('title','Edit Store')
@section('actionUrl')
    {{route('store.update',$store)}}
@endsection

<!-- use only for form with file upload -->
@section('encrypt','enctype=multipart/form-data')
<!------  --    ----------------------- -->
@section('actionName','Update Store')
@section('indexRoute')
    {{route('store.index')}}
@endsection
@section('formBody')
    <!-- this is needed in edit form -->
    <input type="hidden" name="_method" value="PUT">
    <!-- ---------------------------- -->
    <div class="col-6">
        <div class="form-group">
            <label for="city" class="control-label">City</label>
            <input type="text" id="city" name="city" required class="form-control {{$errors->has('city') ? 'is-invalid' : ''}}" value="{{$store->city}}">
            @if($errors->has('city'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('city')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="address" class="control-label">Address</label>
            <textarea type="text" id="address" name="address" required class="form-control {{$errors->has('address') ? 'is-invalid' : ''}}" >{{$store->address}}</textarea>
            @if($errors->has('address'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('address')}}</strong>
                </span>
            @endif
        </div>
    </div><div class="col-6">
        <div class="form-group">
            <label for="phone" class="control-label">Phone</label>
            <input type="text" id="phone" name="phone" required class="form-control {{$errors->has('phone') ? 'is-invalid' : ''}}" value="{{$store->phone}}"
                   data-inputmask='"mask": "(999) 999-9999"' data-mask>
            @if($errors->has('phone'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('phone')}}</strong>
                </span>
            @endif
        </div>
    </div><div class="col-6">
        <div class="form-group">
            <label for="email" class="control-label">Email</label>
            <input type="text" id="email" name="email" required class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" value="{{$store->email}}">
            @if($errors->has('email'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('email')}}</strong>
                </span>
            @endif
        </div>
    </div><div class="col-6">
        <div class="form-group">
            <label for="mapLocation" class="control-label">Google Map Location</label>
            <input type="text" id="mapLocation" name="mapLocation" required class="form-control {{$errors->has('mapLocation') ? 'is-invalid' : ''}}" value="{{$store->mapLocation}}">
            @if($errors->has('mapLocation'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('mapLocation')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="website_url" class="control-label">Website URL</label>
            <input type="text" id="website_url" name="website_url"  class="form-control {{$errors->has('website_url') ? 'is-invalid' : ''}}" value="{{$store->website_url}}">
            @if($errors->has('website_url'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('website_url')}}</strong>
                </span>
            @endif
        </div>
    </div>
@endsection
@section('additionalScripts')
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDjOtRANmY29jxlasbV7XnGD0UfzBiOS3E&sensor=false&libraries=places"></script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var options = {
                componentRestrictions: {country: ['CA']},
            };
            var places = new google.maps.places.Autocomplete(document.getElementById('mapLocation'),options);
            google.maps.event.addListener(places, 'place_changed', function () {
            });
        });
    </script>
@endsection
