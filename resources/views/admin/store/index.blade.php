@extends('admin.layouts.datatable')
@section('title','Store')
@section('tableTitle','Store')
@section('createRoute')
    {{route('store.create')}}
@endsection
@section('button')
    <a class="btn btn-primary btn-sm mr-2" href="@yield('createRoute')">Add new</a>
@endsection
@section('tableHead')
    <th>#</th>
    <th>City</th>
    <th>Address</th>
    <th>Location</th>
    <th>Website URL</th>
    <th>Action</th>
@endsection
@section('tableBody')
    @foreach($stores->sortByDesc('id') as $store)
        <tr>
            <td></td>
            <td>{{$store->city}}</td>
            <td>{{$store->address}}</td>
            <td>{{$store->mapLocation}}</td>
            <td><a href="{{$store->website_url}}" target="_blank">{{$store->website_url}}</th>
            <td>
                <form action="{{route('store.destroy',$store->id)}}" method="POST" id="delete-form-{{$store->id}}">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <a href="#" onclick="return confirmation({{$store->id}});"><i class="fa fa-trash"></i> </a>
                </form>
                <a href="{{route('store.edit',$store->id)}}"><i class="fa fa-edit"></i> </a>
            </td>
        </tr>
    @endforeach
@endsection
