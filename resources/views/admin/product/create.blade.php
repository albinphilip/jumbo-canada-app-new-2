@extends('admin.layouts.app')
@section('title','Add Product')
@section('additionalStyles')
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.css')}}">

    @yield('evenMoreStyles')
@endsection

@section('content')
    <div class="row">
        <!-- /.col -->
        <div class="col-md-12">
            <div class="card">
                <div class="card-header p-2">
                    <div class="d-flex justify-content-between">
                        <div>
                            <ul class="nav nav-pills">
                                <li class="nav-item"><a class="nav-link active" href="#general"
                                                        data-toggle="tab">General</a></li>
                                <li class="nav-item"><a class="nav-link" href="#specification" data-toggle="tab"  >Specification</a>
                                </li>
                                <li class="nav-item"><a class="nav-link" href="#image" data-toggle="tab">Images</a></li>
                                <li class="nav-item"><a class="nav-link" href="#video" data-toggle="tab">Video</a></li>
                                <li class="nav-item"><a class="nav-link" href="#varient" data-toggle="tab">Variant</a></li>
                                <li class="nav-item"><a class="nav-link" href="#metadata" data-toggle="tab">Meta Data</a>
                                <li class="nav-item"><a class="nav-link" href="#shipment" data-toggle="tab">For Shipment</a>
                                </li>
                            </ul>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-primary" form="add-form">
                                Save
                            </button>
                        </div>
                    </div>
                </div><!-- /.card-header -->
                <form method="POST" action="{{route('product.store')}}" enctype="multipart/form-data" id="add-form">
                    {{ csrf_field() }}
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="active tab-pane" id="general">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="brand" class="control-label">Brand Name<small
                                                    style="color:red;">*</small></label>
                                            <select name="brand_id" class="form-control" required id="brand_id">
                                                <option value="">Select</option>
                                                @foreach($brands->sortBy('name') as $brand)
                                                    <option value="{{$brand->id}}">{{$brand->name}}</option>
                                                @endforeach
                                            </select>
                                            @if($errors->has('brand_id'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('brand_id')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="category" class="control-label">Product Category<small
                                                    style="color:red;">*</small></label>
                                            <select name="category_id" class="form-control" required id="category_id" onchange="change_category();">
                                                <option value="">Select</option>
                                                @foreach($categories->sortBy('name') as $category)
                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                @endforeach
                                            </select>
                                            @if($errors->has('category_id'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('category_id')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="name" class="control-label">Product Name<small
                                                    style="color:red;">*</small></label>
                                            <input type="text" id="name" name="name" required
                                                   class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}"
                                                   value="{{old('name')}}">
                                            @if($errors->has('name'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('name')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="offer" class="control-label">Offer % <small>(If the product is
                                                    sold on offer price)</small></label>
                                            <input type="text" id="offer" name="offer"
                                                   class="form-control {{$errors->has('offer') ? 'is-invalid' : ''}}"
                                                   value="{{old('offer')}}">
                                            @if($errors->has('offer'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('offer')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="offer_expire" class="control-label">Offer Price Expires
                                                on</label>
                                            <input type="date" id="offer_expire" name="offer_expire"
                                                   class="form-control {{$errors->has('offer_expire') ? 'is-invalid' : ''}}"
                                                   value="{{old('offer_expire')}}">
                                            @if($errors->has('offer_expire'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('offer_expire')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="model" class="control-label">Product Model</label>
                                            <input type="text" id="model" name="model"
                                                   class="form-control {{$errors->has('model') ? 'is-invalid' : ''}}"
                                                   value="{{old('model')}}">
                                            @if($errors->has('model'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('model')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="upc" class="control-label">Product UPC</label>
                                            <input type="text" id="upc" name="upc"
                                                   class="form-control {{$errors->has('upc') ? 'is-invalid' : ''}}"
                                                   value="{{old('upc')}}">
                                            @if($errors->has('upc'))
                                                <span class="help-block error invalid-feedback">
                                                         <strong>{{$errors->first('upc')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="weight" class="control-label">Product Weight <small>in
                                                    Kg</small></label>
                                            <input type="text" id="weight" name="weight"
                                                   class="form-control {{$errors->has('weight') ? 'is-invalid' : ''}}"
                                                   value="{{old('weight')}}">
                                            @if($errors->has('weight'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('weight')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="dimension" class="control-label">Product Dimension <small>(lXbXh
                                                    cm)</small></label>
                                            <input type="text" id="dimension" name="dimension"
                                                   class="form-control {{$errors->has('dimension') ? 'is-invalid' : ''}}"
                                                   value="{{old('dimension')}}">
                                            @if($errors->has('dimension'))
                                                <span class="help-block error invalid-feedback">
                                                         <strong>{{$errors->first('dimension')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="description" class="control-label">Description <small>(Do
                                                    not capitalise entire sentence)</small><small
                                                    style="color:red;">*</small></label>
                                            <textarea id="description" name="description" required
                                                      class="form-control textarea {{$errors->has('description') ? 'is-invalid' : ''}}">{{old('description')}}
                                                </textarea>
                                            @if($errors->has('description'))
                                                <span class="help-block error invalid-feedback">
                                                         <strong>{{$errors->first('description')}}</strong>
                                                     </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-12" hidden>
                                        <div class="form-group">
                                            <label for="keywords" class="control-label">Keywords <small>(Seperate each
                                                    by comma)</small></label>
                                            <textarea id="keywords" name="keywords"
                                                      class="form-control  {{$errors->has('keywords') ? 'is-invalid' : ''}}">{{old('keywords')}}
                                                 </textarea>
                                            @if($errors->has('keywords'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('keywords')}}</strong>
                                                     </span>
                                            @endif
                                        </div>
                                    </div>
                                </div><!-- row-->
                            </div>
                            <!-- /.tab-pane -->
                            <!-- specification -->
                            <div id="specification" class="tab-pane">
                                <div id="mxr" hidden>
                                    <!-- <div class="col-12 text-right">
                                    <a class="btn btn-default btn-sm m-2" id="addspc">Add new Specification</a>
                                </div>-->
                                <div class="row" id="spec_rows">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="specification" class="control-label">Specification <small>(Height
                                                    / Weight / ..)</small></label>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="value" class="control-label">Value <small>(30 / 40 /
                                                    ...)</small></label>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label for="unit" class="control-label">Unit <small>(Kg / Cm / ...)</small></label>
                                        </div>
                                    </div>
                                    <!-- spec starts -->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="No.Of Jars" name="specification[1][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <select name="valu[1][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                <option value="">Select</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                            </select>
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[1][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('unit')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Super Extractor Juicer" name="specification[2][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <select name="valu[2][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                <option value="">Select</option>
                                                <option value="Included">Included</option>
                                                <option value="Not Included">Not Included</option>
                                                <option value="Optional">Optional</option>
                                            </select>
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[2][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif

                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Chutney Jar Size" name="specification[3][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <select name="valu[3][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                <option value="">Select</option>
                                                <option value="0.1">0.1</option>
                                                <option value="0.2">0.2</option>
                                                <option value="0.3">0.3</option>
                                                <option value="0.4">0.4</option>
                                                <option value="0.5">0.5</option>
                                                <option value="0.6">0.6</option>
                                                <option value="0.7">0.7</option>
                                                <option value="0.8">0.8</option>
                                                <option value="0.9">0.9</option>
                                                <option value="1">1</option>
                                            </select>
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[3][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('unit')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Medium Jar Size" name="specification[4][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <select name="valu[4][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                <option value="">Select</option>
                                                <option value="0.5">0.5</option>
                                                <option value="0.75">0.75</option>
                                                <option value="1">1</option>
                                                <option value="1.25">1.25</option>
                                                <option value="1.5">1.5</option>
                                                <option value="1.75">1.75</option>
                                                <option value="2">2</option>
                                                <option value="2.25">2.25</option>
                                                <option value="2.5">2.5</option>
                                                <option value="2.75">2.75</option>
                                                <option value="3">3</option>
                                            </select>
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[4][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('unit')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Large Jar Size" name="specification[5][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <select name="valu[5][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                <option value="">Select</option>
                                                <option value="1">1</option>
                                                <option value="1.25">1.25</option>
                                                <option value="1.5">1.5</option>
                                                <option value="1.75">1.75</option>
                                                <option value="2">2</option>
                                                <option value="2.25">2.25</option>
                                                <option value="2.5">2.5</option>
                                                <option value="2.75">2.75</option>
                                                <option value="3">3</option>
                                                <option value="3.25">3.25</option>
                                                <option value="3.5">3.5</option>
                                                <option value="3.75">3.75</option>
                                                <option value="4">4</option>
                                            </select>
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[5][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('unit')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Grind N' Store Containers" name="specification[6][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <select name="valu[6][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                <option value="">Select</option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                                <option value="Optional">Optional</option>
                                            </select>
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[6][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif

                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Warranty" name="specification[7][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <select name="valu[7][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                <option value="">Select</option>
                                                <option value="1 Year">1 Year</option>
                                                <option value="2 Year">2 Year</option>
                                                <option value="3 Year">3 Year</option>
                                                <option value="4 Year">4 Year</option>
                                                <option value="5 Year">5 Year</option>
                                            </select>
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[7][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('unit')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Voltage, Wattage" name="specification[8][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="value" name="valu[8][]"
                                                   class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                   value="{{old('value')}}">
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[8][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif

                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Chef Jar" name="specification[9][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                        <!-- <input type="text" id="value" name="valu[10][]"
                                                       class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                       value="{{old('value')}}"> -->
                                            <select name="valu[9][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                <option value="">Select</option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[9][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('unit')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Coconut Scraper" name="specification[10][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <select name="valu[10][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                <option value="">Select</option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                                <option value="Optional">Optional</option>
                                            </select>
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[10][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif

                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Local customization" name="specification[11][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <select name="valu[11][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                <option value="">Select</option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[11][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif

                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Sound score" name="specification[12][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="value" name="valu[12][]"
                                                   class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                   value="{{old('value')}}">
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[12][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif

                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Performance Score" name="specification[13][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="value" name="valu[13][]"
                                                   class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                   value="{{old('value')}}">
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[13][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif

                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Design Score" name="specification[14][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="value" name="valu[14][]"
                                                   class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                   value="{{old('value')}}">
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[14][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif

                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Return Ratio" name="specification[15][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="value" name="valu[15][]"
                                                   class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                   value="{{old('value')}}">
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[15][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif

                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Durability Score Jars" name="specification[16][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="value" name="valu[16][]"
                                                   class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                   value="{{old('value')}}">
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[16][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif

                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Sellers Warranty Motor" name="specification[17][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="value" name="valu[17][]"
                                                   class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                   value="{{old('value')}}">
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[17][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif

                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Sellers Warranty Jars" name="specification[18][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="value" name="valu[18][]"
                                                   class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                   value="{{old('value')}}">
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[18][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif

                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Sellers Warranty Parts" name="specification[19][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="value" name="valu[19][]"
                                                   class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                   value="{{old('value')}}">
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[19][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif

                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!-- SPC -->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Certification" name="specification[20][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <select name="valu[20][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                <option value="">Select</option>
                                                <option value="NON-UL">NON-UL</option>
                                                <option value="UL Motor">UL Motor</option>
                                                <option value="SGS">SGS</option>
                                            </select>
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[20][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('unit')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- SPEC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Motor Type" name="specification[21][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <select name="valu[21][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                <option value="">Select</option>
                                                <option value="Turbo">Turbo</option>
                                                <option value="Aria Cool Tec Motor">Aria Cool Tec Motor</option>
                                                <option value="Universal 550 Watt High Power Motor">Universal 550 Watt High Power Motor</option>
                                                <option value="Universal 600 Watt High Power Motor">Universal 600 Watt High Power Motor</option>
                                            </select>
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[21][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('unit')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Mixer Shape" name="specification[22][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <select name="valu[22][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                <option value="">Select</option>
                                                <option value="A Shape">A Shape</option>
                                                <option value="L Shape">L Shape</option>
                                                <option value="Cube Shape">Cube Shape</option>
                                                <option value="Bottle Shape">Bottle Shape</option>
                                            </select>
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[22][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('unit')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--SPC END-->

                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Colour" name="specification[23][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <select name="valu[23][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                <option value="">Select</option>
                                                <option value="White">White</option>
                                                <option value="Ocean Green">Ocean Green</option>
                                                <option value="Blue">Blue</option>
                                                <option value="Red">Red</option>
                                                <option value="Black">Black</option>
                                                <option value="Metallic Silver">Metallic Silver</option>
                                            </select>
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[23][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('unit')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="OLP" name="specification[24][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <select name="valu[24][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                <option value="">Select</option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[24][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('unit')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Manufactured By" name="specification[25][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <select name="valu[25][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                <option value="">Select</option>
                                                <option value="Maya Appliances">Maya Appliances</option>
                                                <option value="Others">Others</option>
                                            </select>
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[25][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif

                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Brand name" name="specification[26][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <select name="valu[26][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                <option value="">Select</option>
                                                <option value="Preethi">Preethi</option>
                                                <option value="Butterfly">Butterfly</option>
                                                <option value="Vidiem">Vidiem</option>
                                                <option value="Others">Others</option>
                                            </select>
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[26][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif

                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Price" name="specification[27][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="value" name="valu[27][]"
                                                   class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                   value="{{old('value')}}">
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[27][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif

                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Remarks" name="specification[28][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="value" name="valu[28][]"
                                                   class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                   value="{{old('value')}}">
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[28][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif

                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Seller Rating" name="specification[29][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                        <!-- <input type="text" id="value" name="valu[29][]"
                                                       class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                       value="{{old('value')}}"> -->
                                            <select name="valu[29][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                <option value="">Select</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                            </select>
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[29][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('unit')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Packaging" name="specification[30][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                        <!-- <input type="text" id="value" name="valu[30][]"
                                                       class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                       value="{{old('value')}}"> -->
                                            <select name="valu[30][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                <option value="">Select</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                            </select>
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="units/m carton" name="unit[30][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('unit')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Jar Lid Type" name="specification[31][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                        <!-- <input type="text" id="value" name="valu[31][]"
                                                       class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                       value="{{old('value')}}"> -->
                                            <select name="valu[31][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                <option value="">Select</option>
                                                <option value="Clear Dome">Clear Dome</option>
                                                <option value="Flat Cover with Lock">Flat Cover with Lock</option>
                                                <option value="Flat Dome">Flat Dome</option>
                                                <option value="Flat">Flat</option>
                                            </select>
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[31][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('unit')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--SPC END-->

                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Body made by" name="specification[32][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <select name="valu[32][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                <option value="">Select</option>
                                                <option value="ABS Body">ABS Body</option>
                                                <option value="SS">SS</option>
                                            </select>
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[32][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('unit')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Flow Braker" name="specification[33][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <select name="valu[33][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                <option value="">Select</option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[33][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('unit')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Features" name="specification[34][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <select name="valu[34][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                <option value="">Select</option>
                                                <option value="Overload and Voltage Fluctation Cut Off">Overload and Voltage Fluctation Cut Off</option>
                                            </select>
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[34][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('unit')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Customer rating" name="specification[35][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <select name="valu[35][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                <option value="">Select</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                            </select>
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[35][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('unit')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!--SPEC-->
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" id="specification" class="form-control"
                                                   value="Customized" name="specification[36][]" readonly>
                                            @if($errors->has('specification'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <select name="valu[36][]" id="value"  class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}">
                                                <option value="">Select</option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="unit"
                                                   class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                   value="" name="unit[36][]">
                                            @if($errors->has('unit'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif

                                        </div>
                                    </div>
                                    <!--SPC END-->
                                    <!-- spec ends -->
                                </div><!--row-->
                                </div>
                            <!-- /.tab-pane -->
                            <!-- specification -->
                                <div id="other">
                                <div class="col-12 text-right">
                                    <a class="btn btn-default btn-sm m-2" id="addspc">Add new Specification</a>
                                </div>
                                <div class="row" id="spec_row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="specification" class="control-label">Specification <small>(Height
                                                    / Weight / ..)</small></label>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="value" class="control-label">Value <small>(30 / 40 /
                                                    ...)</small></label>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label for="unit" class="control-label">Unit <small>(Kg / Cm / ...)</small></label>
                                        </div>
                                    </div>
                                        <div class="col-4">
                                            <div class="form-group">
                                                <input type="text" id="specification" class="form-control"
                                                       value="{{old('specification')}}" name="specification[1][]">
                                                @if($errors->has('specification'))
                                                    <span class="help-block error invalid-feedback">
                                    <strong>{{$errors->first('specification')}}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="form-group">
                                                <input type="text" id="value" name="valu[1][]"
                                                       class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                       value="{{old('value')}}">
                                                @if($errors->has('value'))
                                                    <span class="help-block error invalid-feedback">
                                        <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="text" id="unit"
                                                       class="form-control {{$errors->has('unit') ? 'is-invalid' : ''}}"
                                                       value="{{old('unit')}}" name="unit[1][]">
                                                @if($errors->has('unit'))
                                                    <span class="help-block error invalid-feedback">
                                        <strong>{{$errors->first('unit')}}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                </div>
                                </div>
                            </div><!--specification end-->

                            <!-- image -->
                            <div id="image" class="tab-pane">
                                <div class="col-12 text-right">
                                    <a class="btn btn-default btn-sm m-2" id="addimage">Add new image</a>
                                </div>
                                <div class="row" id="image_row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="image" class="control-label">Image<small> (Not less than
                                                    812X590 px , Maximum 500Kb)</small></label>
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <div class="form-group">
                                            <label for="sort_order" class="control-label">Sort Order</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file"
                                                           class="custom-file-input {{$errors->has('image_path') ? 'is-invalid' : ''}}"
                                                           id="image_path" name="image_path[1][]"
                                                           value="{{old('image_path')}}">
                                                    <label class="custom-file-label" for="image_path">Choose
                                                        file</label>
                                                </div>
                                            </div>
                                            @if($errors->has('image_path'))
                                                <span class="help-block error invalid-feedback">
                                                            <strong>{{$errors->first('image_path')}}</strong>
                                                        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <div class="form-group">
                                            <input type="number" id="sort_order" name="sort_order[1][]" min="0"
                                                   class="form-control {{$errors->has('sort_order') ? 'is-invalid' : ''}}"
                                                   value="{{old('sort_order')}}">
                                            @if($errors->has('sort_order'))
                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('sort_order')}}</strong>
                                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div><!-- image end-->

                            <div id="video" class="tab-pane">
                                <div class="col-12 text-right">
                                    <a class="btn btn-default btn-sm m-2" id="addvideo">Add new Video</a>
                                </div>
                                <div class="row" id="video_row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="url" class="control-label">Youtube Url <small>Full URL (eg: https://www.youtube.com/watch?v=TBKU1Bh50-g)</small></label>
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <div class="form-group">
                                            <label for="v_sort_order" class="control-label">Sort Order</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <input type="url"
                                                   class="form-control {{$errors->has('url') ? 'is-invalid' : ''}}"
                                                   id="url" name="url[1][]"
                                                   value="{{old('url')}}" placeholder="Enter Youtube Url">
                                            @if($errors->has('url'))
                                                <span class="help-block error invalid-feedback">
                                                            <strong>{{$errors->first('url')}}</strong>
                                                        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <div class="form-group">
                                            <input type="number" id="sort_order" name="v_sort_order[1][]" min="0"
                                                   class="form-control {{$errors->has('v_sort_order') ? 'is-invalid' : ''}}"
                                                   value="{{old('v_sort_order')}}">
                                            @if($errors->has('v_sort_order'))
                                                <span class="help-block error invalid-feedback">
                                                                <strong>{{$errors->first('v_sort_order')}}</strong>
                                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div><!-- video end-->

                            <!-- varient -->
                            <div id="varient" class="tab-pane">
                                <div class="col-12 text-right">
                                    <a class="btn btn-default btn-sm m-2" id="addvariant">Add new product variant</a>
                                </div>
                                <div class="row" id="variant_row">
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label for="Variant" class="control-label">Variance on <small>(color / Sie /
                                                    ..)</small></label>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label for="value" class="control-label">Variant Value <small>(Red / 45 /
                                                    ..)</small></label>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label for="Price" class="control-label">Price of this variant
                                                <small>(C$)</small> </label>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label for="Quantity" class="control-label">Stock</label>
                                        </div>
                                    </div>

                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="variant" name="variant[1][]"
                                                   class="form-control {{$errors->has('variant') ? 'is-invalid' : ''}}"
                                                   value="{{old('variant')}}">
                                            @if($errors->has('variant'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('variant')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="value" name="val[1][]"
                                                   class="form-control {{$errors->has('value') ? 'is-invalid' : ''}}"
                                                   value="{{old('value')}}">
                                            @if($errors->has('value'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('value')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" id="price" name="price[1][]"
                                                   class="form-control {{$errors->has('price') ? 'is-invalid' : ''}}"
                                                   value="{{old('price')}}">
                                            @if($errors->has('price'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('price')}}</strong>
                                                        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="form-group">
                                            <input type="number" id="quantity" name="quantity[1][]"
                                                   class="form-control {{$errors->has('quantity') ? 'is-invalid' : ''}}"
                                                   value="{{old('quantity')}}">
                                            @if($errors->has('quantity'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('quantity')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div><!-- VARIANT END-->
                            <!-- metadata -->
                            <div class="tab-pane" id="metadata">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="title" class="control-label">Title</label>
                                            <input type="text" id="title" name="title" maxlength="60"
                                                   class="form-control {{$errors->has('title') ? 'is-invalid' : ''}}"
                                                   value="{{old('title')}}">
                                            @if($errors->has('title'))
                                                <span class="help-block error invalid-feedback">
                                                    <strong>{{$errors->first('title')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                        <div><span>(</span><span id="c_count"></span><span>/60)</span></div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="description" class="control-label">Meta Description <small>(Do
                                                    not capitalise entire sentence)</small></label>
                                            <textarea id="meta_description" name="meta_description" maxlength="160"
                                                      class="form-control  {{$errors->has('meta_description') ? 'is-invalid' : ''}}">{{old('meta_description')}}
                                                </textarea>
                                            @if($errors->has('meta_description'))
                                                <span class="help-block error invalid-feedback">
                                                         <strong>{{$errors->first('meta_description')}}</strong>
                                                     </span>
                                            @endif
                                        </div>
                                        <div><span>(</span><span id="current_count"></span><span>/160)</span></div>

                                    </div>

                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="keywords" class="control-label">Keywords <small>(Seperate each
                                                    by comma)</small></label>
                                            <textarea id="keywords" name="keywords"
                                                      class="form-control  {{$errors->has('keywords') ? 'is-invalid' : ''}}">{{old('keywords')}}
                                                 </textarea>
                                            @if($errors->has('keywords'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('keywords')}}</strong>
                                                     </span>
                                            @endif
                                        </div>
                                    </div>
                                </div><!-- row-->
                            </div>
                            <!-- metadata end -->

                            <!-- shippment -->
                            <div class="tab-pane" id="shipment">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="weight" class="control-label"> Weight <small>in
                                                    Kg</small></label>
                                            <input type="text" id="weight" name="shipment_weight"
                                                   class="form-control {{$errors->has('weight') ? 'is-invalid' : ''}}"
                                                   value="{{old('weight')}}">
                                            @if($errors->has('weight'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('weight')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="name" class="control-label">Height <small>in
                                                    inch</small></label>
                                            <input type="text" id="height" name="height"
                                                   class="form-control {{$errors->has('height') ? 'is-invalid' : ''}}"
                                                   value="{{old('height')}}">
                                            @if($errors->has('height'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('height')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="name" class="control-label">Length <small>in
                                                    inch</small></label>
                                            <input type="text" id="length" name="length"
                                                   class="form-control {{$errors->has('length') ? 'is-invalid' : ''}}"
                                                   value="{{old('length')}}">
                                            @if($errors->has('length'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('length')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="name" class="control-label">Width <small>in
                                                    inch</small></label>
                                            <input type="text" id="breadth" name="breadth"
                                                   class="form-control {{$errors->has('breadth') ? 'is-invalid' : ''}}"
                                                   value="{{old('breadth')}}">
                                            @if($errors->has('breadth'))
                                                <span class="help-block error invalid-feedback">
                                                        <strong>{{$errors->first('breadth')}}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                            <!-- /.tab-pane -->
                            <!-- shipment end -->
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div><!-- /.card-body -->
                </form>
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>

@endsection
@section('additionalScripts')
    <script type="text/javascript">
        $(document).ready(function () {
            bsCustomFileInput.init();
            category = $('#category_id option:selected').text();
            console.log(category);

                $('#other').attr('hidden','');
                $('#other').find('input,textarea,select').attr({'disabled':'disabled'});

            $('#current_count').text(0);
            $('#c_count').text(0);

        });
    </script>
    <script>
        $(function () {
            // Summernote
            $('.textarea').summernote()
            $('.textarea').css('font-family', 'Cario')
        })
    </script>
    <script>
        var i = 2;
        var j = 2;
        var k = 2;
        var l = 2;
        $("#addspc").on("click", function () {

            $("#spec_row").append('<div class="col-4"><div class="form-group"><input type="text" name="specification[' + i + '][]" class="form-control"/></div></div>' +
                '<div class="col-4"><div class="form-group"><input type="text" name="valu[' + i + '][]" class="form-control"/></div></div>' +
                '<div class="col-3"><div class="form-group"><input type="text" name="unit[' + i + '][]" class="form-control"/></div></div>');
            i++;

        });
        $("#addimage").on("click", function () {

            $("#image_row").append('<div class="col-6"><div class="form-group"><div class="input-group"><div class="custom-file">' +
                '<input type="file" class="custom-file-input"  name="image_path[' + j + '][]" id="image_path[' + j + ']" onchange="showfilename(j)"> ' +
                '<label class="custom-file-label" for="image_path" id ="label' + j + '">Choose file</label></div></div></div></div>' +
                '<div class="col-5"><div class="form-group"><input type="number" name="sort_order[' + j + '][]" class="form-control" min="0"/></div></div>');
            j++;

        });
        $("#addvideo").on("click", function () {

            $("#video_row").append('<div class="col-6"><div class="form-group">' +
                '<input placeholder="Enter Youtube Url" type="url" class="form-control"  name="url[' + l + '][]" id="url[' + l + ']"> ' +
                '</div></div>' +
                '<div class="col-5"><div class="form-group"><input type="number" name="v_sort_order[' + l + '][]" class="form-control" min="0"/></div></div>');
            l++;

        });
        $("#addvariant").on("click", function () {

            $("#variant_row").append('<div class="col-3"><div class="form-group"><input type="text" name="variant[' + k + '][]" class="form-control"/></div></div>' +
                '<div class="col-3"><div class="form-group"><input type="text" name="val[' + k + '][]" class="form-control"/></div></div>' +
                '<div class="col-3"><div class="form-group"><input type="number" name="price[' + k + '][]" class="form-control"/></div></div>' +
                '<div class="col-2"><div class="form-group"><input type="number" name="quantity[' + k + '][]" class="form-control"/></div></div>');
            k++;


        });
    </script>

    <script src="{{asset('plugins/summernote/summernote-bs4.min.js')}}"></script>

    <script src="{{asset('plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
    <script>
        function showfilename(j) {
            console.log(j - 1);
            j = j - 1;
            var fileName = document.getElementById('image_path[' + j + ']').files[0].name;
            console.log(fileName);
            //replace the "Choose a file" label
            $('#label' + j).text(fileName);

        }
    </script>
    <script>
       function change_category(){
            category = $('#category_id option:selected').text();
            console.log(category.includes("Mixer"));
            if(category.includes("Mixer") == true ){
                console.log('vv');
                $('#mxr').removeAttr('hidden');
                $('#mxr').find('input,textarea,select').removeAttr('disabled');
                $('#other').attr('hidden','');
                $('#other').find('input,textarea,select').attr({'disabled':'disabled'});
            }
            else{
                $('#mxr').attr('hidden','');
                $('#mxr').find('input,textarea,select').attr({'disabled':'disabled'});
                $('#other').removeAttr('hidden');
                $('#other').find('input,textarea,select').removeAttr('disabled');
            }
           //return true;
        }
    </script>
    <script>
        $('#meta_description').keyup(function() {
            console.log($(this).val().length);
            characterCount = $(this).val().length;
            $('#current_count').text(characterCount);
        });
        $('#title').keyup(function() {
            console.log($(this).val().length);
            characterCount = $(this).val().length;
            $('#c_count').text(characterCount);
        });
    </script>
@endsection


