@extends('admin.layouts.datatable')
@section('title','Partner Registrations')
@section('tableTitle','Partner Registrations')
@section('tableHead')
    <th>#</th>
    <th>Name</th>
    <th>Email</th>
    <th>Phone</th>
    <th>Address</th>
    <th>City</th>
    <th>Action</th>
@endsection
@section('tableBody')
    @foreach($partners->sortByDesc('id') as $partner)
        <tr>
            <td></td>
            <td>{{$partner->name}}</td>
            <td>{{$partner->email}}</td>
            <td>{{$partner->phone}}</td>
            <td>{{$partner->address }}<br/>
                {{$partner->province}}
            </td>
            <td>{{$partner->city }}</td>
            <td>
                <a href="{{route('partner.edit',$partner->id)}}"><i class="fa fa-edit"></i> </a>
            </td>
        </tr>
    @endforeach
@endsection
