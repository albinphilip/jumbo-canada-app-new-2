@extends('admin.layouts.datatable')
@section('title','Recipe')
@section('tableTitle','Recipe')
@section('createRoute')
    {{route('recipe.create')}}
@endsection
@section('button')
    <a class="btn btn-primary btn-sm mr-2" href="@yield('createRoute')">Add new</a>
@endsection
@section('tableHead')
    <th>#</th>
    <th>Title</th>
    <th>Photo</th>
    <th>Action</th>
@endsection
@section('tableBody')
    @foreach($recipes->sortByDesc('id') as $recipe)
        <tr>
            <td></td>
            <td>{{$recipe->title}}</td>
            <td><img src="{{asset($recipe->cover_photo)}}" width="200px" class="img-thumbnail"></td>
            <td>
                <form action="{{route('recipe.destroy',$recipe->id)}}" method="POST" id="delete-form-{{$recipe->id}}">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <a href="#" onclick="return confirmation({{$recipe->id}});"><i class="fa fa-trash"></i> </a>
                </form>
                <a href="{{route('recipe.edit',$recipe->id)}}"><i class="fa fa-edit"></i> </a>
            </td>
        </tr>
    @endforeach
@endsection
