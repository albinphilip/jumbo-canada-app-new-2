@extends('admin.layouts.form')
@section('title','Add new Video')
@section('actionUrl')
    {{route('recipe.store')}}
@endsection

<!-- use only for form with file upload -->
@section('encrypt','enctype=multipart/form-data')
<!------  --    ----------------------- -->
@section('actionName','Add Video')
@section('indexRoute')
    {{route('recipe.index')}}
@endsection
@section('formBody')
    <div class="col-6">
        <div class="form-group">
            <label for="title" class="control-label">Title</label>
            <input type="text" id="title" name="title" required class="form-control {{$errors->has('title') ? 'is-invalid' : ''}}" value="{{old('title')}}">
            @if($errors->has('title'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('title')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="video_id" class="control-label">Youtube URL</label>
            <input type="text" id="video_id" name="video_id" required class="form-control {{$errors->has('video_id') ? 'is-invalid' : ''}}" value="{{old('video_id')}}">
            @if($errors->has('video_id'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('video_id')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="category" class="control-label">Category</label>
            <div class="row">
                <div class="col-6">
                    <input type="radio" class="{{$errors->has('category') ? 'is-invalid' : ''}}"  name="category" value="Media" checked> Media
                </div>
                <div class="col-6">
                    <input type="radio" class="{{$errors->has('category') ? 'is-invalid' : ''}}"  name="category" value="Recipe"> Recipe
                </div>
            </div>
            @if($errors->has('category'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('category')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="cover_photo">Cover Photo <small>(Not less than 536X343 px / Maximum 500Kb)</small></label>
            <div class="input-group">
                <div class="custom-file">
                    <input type="file" class="custom-file-input {{$errors->has('cover_photo') ? 'is-invalid' : ''}}" id="cover_photo" name="cover_photo" value="{{old('cover_photo')}}" required>
                    <label class="custom-file-label" for="cover_photo">Choose file</label>
                </div>
            </div>
            @if($errors->has('cover_photo'))
                <span class="help-block error" style="color:#c9352a">
                    <strong>{{$errors->first('cover_photo')}}</strong>
                </span>
            @endif
        </div>
    </div>
@endsection
@section('additionalScripts')
    <script src="{{asset('plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            bsCustomFileInput.init();
        });
    </script>
@endsection
