@extends('admin.layouts.datatable')
@section('title','Customers')
@section('tableTitle','Customers')
@section('tableHead')
    <th>#</th>
    <th>Name</th>
    <th>Email</th>
    <th>Phone Number</th>
    <th>Address</th>
    <th>No.of Products Purchased</th>
    <th>Created date</th>
    <th>Action</th>
@endsection
@section('tableBody')
    @foreach($customers->sortByDesc('id') as $customer)
        <tr>
            <td></td>
            <td><a href="{{route('customer-order',$customer->id)}}">{{$customer->firstname.' '.$customer->lastname}}</a></td>
            <td>{{$customer->email}}</td>
            <td>{{$customer->telephone}}</td>
            <td>
            @foreach($customer->addresses->sortByDesc('id') as $address)
                @if($address->type != 'billing' && $address->type != 'hidden')
                    {{$address->firstname.' '.$address->lastname}} ,
                    {{$address->address_1}},
                    {{$address->address_2}},
                    {{$address->province}},
                    {{$address->city .' - '}}{{$address->postcode}} ,
                    {{$address->company}} <br/>
                    <hr/>
                @endif
            @endforeach
            </td>
            <td>{{$customer->orders->count()}}</td>
            <td>{{date_format($customer->created_at,'Y-m-d')}}</td>
            <td>
                <form action="{{route('customer.destroy',$customer->id)}}" method="POST" id="delete-form-{{$customer->id}}">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <a href="#" onclick="return confirmation({{$customer->id}});"><i class="fa fa-trash"></i> </a>
                </form>
            </td>
        </tr>
    @endforeach
    <!-- date filter -->
    <div class="input-daterange">
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="from" class="control-label">From</label>
                    <input type="text" id="from_date" name="from_date" class="form-control dateFilter" value="{{old('from_date')}}" >
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="to" class="control-label">To</label>
                    <input type="text" id="to_date" name="to_date" required class="form-control dateFilter" value="{{old('to_date')}}" >
                </div>
            </div>
            <div class="col-1 pt-4">
                <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
            </div>
            <div class="col-2 pt-4" id="download_div">
                <div class="form-group">
                    <button type="button" class="btn btn-success" onclick="exportForm()" target="_blank"><i class="fa fa-download"></i></button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('evenMoreScripts')
    <!-- date filter -->
    <script>
        var total_amount = 0;
        $(document).ready(function(){
            $('.dateFilter').datepicker({
                dateFormat: "yy-m-dd"
            });
            var id_prev = 0;
            $.fn.dataTableExt.afnFiltering.push(
                function (settings, data, dataIndex) {
                    var min = $('#from_date').datepicker("getDate");
                    var max = $('#to_date').datepicker("getDate");
                    var hireDate = new Date(data[6]); //date of order
                    // console.log('from'+min);
                    //console.log('to'+max);
                    if(min != null)
                    {
                        min=min.setHours(0,0,0,0);
                    }
                    if(max != null)
                    {
                        max=max.setHours(0,0,0,0);
                    }
                    hireDate=hireDate.setHours(0,0,0,0);

                    if (min == null && max == null) {
                        return true; }
                    else if (min == null && hireDate <= max ) {
                        return true;}
                    else if(max == null && hireDate >= min) {
                        return true;}
                    else if (min <= hireDate && hireDate <= max) {
                        return true; }
                    return false;
                }
            );
            $("#filter").click(function () {

                var myDataTable = $('#tb1').DataTable();
                myDataTable.draw();

            });
        });
        function exportForm()
        {
            console.log('submit');
            var from = $('#from_date').datepicker("getDate");
            var to = $('#to_date').datepicker("getDate");
            if(from != null )
                from = from.getFullYear() + "/" +
                    (from.getMonth() + 1) + "/" + from.getDate();
            if(to != null)
                //to = to.toLocaleDateString();
                to = to.getFullYear() + "/" +
                    (to.getMonth() + 1) + "/" + to.getDate();
            console.log(from);

            url = '/admin/customer/export-to-csv';
            $.ajax({
                type: "post",
                url: url,
                cache: false,
                data: {_token: '{{csrf_token()}}',_method:"post",from: from,to:to},
                success: function (result) {
                    console.log(result);

                    if(result.success = 'succes')
                    {
                        window.open(result.path);
                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log('error');
                }
            });
        }
    </script>
@endsection
