@extends('admin.layouts.form')
@section('title','Edit product ')
@section('actionUrl')
    {{route('product-repair-registration.update',$regisered_product->id)}}
@endsection

<!-- use only for form with file upload -->
@section('encrypt','enctype=multipart/form-data')
<!------  --    ----------------------- -->
@section('actionName','Edit product')
@section('indexRoute')
    {{route('product-repair-registration.index')}}
@endsection
@section('formBody')
    <!-- this is needed in edit form -->
    <input type="hidden" name="_method" value="PUT">
    <!-- ---------------------------- -->

    <div class="col-6">
        <div class="form-group">
            <label class="control-label" for="product_id">Product</label>
            <select name="product_id" required class="form-control" id="product_id">
                <option value="">Select Product</option>
                @foreach($products as $product)
                    <option value="{{$product->id}}" {{old('product_id',$regisered_product->product_id) == $product->id ? 'selected' : ''}}>{{$product->name}}</option>
                @endforeach
            </select>
            @if($errors->has('product_id'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('product_id')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="serial_number" class="control-label">Serial Number</label>
            <input type="text" id="serial_number" name="serial_number" required class="form-control {{$errors->has('serial_number') ? 'is-invalid' : ''}}" value="{{old('serial_number',$regisered_product->serial_number)}}">
            @if($errors->has('serial_number'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('serial_number')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="safety_code" class="control-label">Safety Code</label>
            <input type="text" id="safety_code" name="safety_code" required pattern="(PT21ULT.[0-9]{2,}|PT20ULT.[0-9]{3,4})"
                   class="form-control {{$errors->has('safety_code') ? 'is-invalid' : ''}}" value="{{old('safety_code',$regisered_product->safety_code)}}">
            @if($errors->has('safety_code'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('safety_code')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="product_code" class="control-label">Product Code</label>
            <input type="text" id="product_code" name="product_code" required class="form-control {{$errors->has('product_code') ? 'is-invalid' : ''}}" value="{{old('product_code',$regisered_product->product_code)}}">
            @if($errors->has('product_code'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('product_code')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="purchase_date" class="control-label">Date of Purchase</label>
            <input type="date" id="purchase_date" name="purchase_date" required class="form-control {{$errors->has('purchase_date') ? 'is-invalid' : ''}}" value="{{old('purchase_date',$regisered_product->purchase_date)}}">
            @if($errors->has('purchase_date'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('purchase_date')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="purchase_place" class="control-label">Where did you purchase your appliance?</label>
            <input type="text" id="purchase_place" name="purchase_place" class="form-control {{$errors->has('purchase_place') ? 'is-invalid' : ''}}" value="{{old('purchase_place',$regisered_product->purchase_place)}}">
            @if($errors->has('purchase_place'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('purchase_place')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="issue" class="control-label">Describe the issue or request that you would like to repair</label>
            <textarea type="text" id="issue" name="issue" required class="form-control {{$errors->has('issue') ? 'is-invalid' : ''}}" >{{old('issue',$regisered_product->issue)}}</textarea>
            @if($errors->has('issue'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('issue')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="first_name" class="control-label">First Name</label>
            <input type="text" id="first_name" name="first_name" required class="form-control {{$errors->has('first_name') ? 'is-invalid' : ''}}" value="{{old('first_name',$regisered_product->first_name)}}">
            @if($errors->has('first_name'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('first_name')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="last_name" class="control-label">Last Name</label>
            <input type="text" id="last_name" name="last_name"  class="form-control {{$errors->has('last_name') ? 'is-invalid' : ''}}" value="{{old('last_name',$regisered_product->last_name)}}">
            @if($errors->has('last_name'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('last_name')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label class="control-label" for="provenance">Province</label>
            <select name="provenance" class="form-control" id="provenance">
                <option value="">Select Provenance</option>
                <option value="Nunavut" {{old('provenance',$regisered_product->provenance) == 'Nunavut' ? 'selected' : ''}}>Nunavut</option>
                <option value="Quebec" {{old('provenance',$regisered_product->provenance) == 'Quebec' ? 'selected' : ''}}>Quebec</option>
                <option value="Northwest Territories" {{old('provenance',$regisered_product->provenance) == 'Northwest Territories' ? 'selected' : ''}}>Northwest Territories</option>
                <option value="Ontario" {{old('provenance',$regisered_product->provenance) == 'Ontario' ? 'selected' : ''}}>Ontario</option>
                <option value="British Columbia" {{old('provenance',$regisered_product->provenance) == 'British Columbia' ? 'selected' : ''}}>British Columbia</option>
                <option value="Alberta" {{old('provenance',$regisered_product->provenance) == 'Alberta' ? 'selected' : ''}}>Alberta</option>
                <option value="Saskatchewan" {{old('provenance',$regisered_product->provenance) == 'Saskatchewan' ? 'selected' : ''}}>Saskatchewan</option>
                <option value="Manitoba" {{old('provenance',$regisered_product->provenance) == 'Manitoba' ? 'selected' : ''}}>Manitoba</option>
                <option value="Yukon" {{old('provenance',$regisered_product->provenance) == 'Yukon' ? 'selected' : ''}}>Yukon</option>
                <option value="Newfoundland and Labrador" {{old('provenance',$regisered_product->provenance) == 'Newfoundland and Labrador' ? 'selected' : ''}}>Newfoundland and Labrador</option>
                <option value="New Brunswick" {{old('provenance',$regisered_product->provenance) == 'New Brunswick' ? 'selected' : ''}}>New Brunswick</option>
                <option value="Nova Scotia" {{old('provenance',$regisered_product->provenance) == 'Nova Scotia' ? 'selected' : ''}}>Nova Scotia</option>
                <option value="Prince Edward Island" {{old('provenance',$regisered_product->provenance) == 'Prince Edward Island' ? 'selected' : ''}}>Prince Edward Island</option>
            </select>
            @if($errors->has('provenance'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('provenance')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="city" class="control-label">City</label>
            <input type="text" id="city" name="city"  class="form-control {{$errors->has('city') ? 'is-invalid' : ''}}" value="{{old('city',$regisered_product->city)}}">
            @if($errors->has('city'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('city')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="address" class="control-label">Address</label>
            <input type="text" id="address" name="address"  class="form-control {{$errors->has('address') ? 'is-invalid' : ''}}" value="{{old('address',$regisered_product->address)}}">
            @if($errors->has('address'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('address')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="zipcode" class="control-label">Zipcode</label>
            <input type="text" id="zipcode" name="zipcode"  class="form-control {{$errors->has('zipcode') ? 'is-invalid' : ''}}" value="{{old('zipcode',$regisered_product->zipcode)}}">
            @if($errors->has('zipcode'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('zipcode')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="phone" class="control-label">Phone Number</label>
            <input type="text" id="phone" name="phone" required data-inputmask='"mask": "(999) 999-9999"' data-mask
                   class="form-control {{$errors->has('phone') ? 'is-invalid' : ''}}" value="{{old('phone',$regisered_product->phone)}}">
            @if($errors->has('phone'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('phone')}}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="email" class="control-label">Email Address</label>
            <input type="email" id="email" name="email" required class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" value="{{old('email',$regisered_product->email)}}">
            @if($errors->has('email'))
                <span class="help-block error invalid-feedback">
                    <strong>{{$errors->first('email')}}</strong>
                </span>
            @endif
        </div>
    </div>
@endsection

