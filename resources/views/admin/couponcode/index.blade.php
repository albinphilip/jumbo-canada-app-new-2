@extends('admin.layouts.datatable')
@section('title','Coupon Codes')
@section('tableTitle','Coupon Codes')
@section('createRoute')
    {{route('couponcode.create')}}
@endsection
@section('button')
    <a class="btn btn-primary btn-sm mr-2" href="@yield('createRoute')">Add new</a>
@endsection
@section('tableHead')
    <th>#</th>
    <th>Code</th>
    <th>Expiry Date</th>
    <th>Applicable To</th>
    <th>Category</th>
    <th>Reduction Type</th>
    <th>Value</th>
    <th>Action</th>
    <th>Total Redemptions</th>
    <th>Applicable Product</th>
    <th>Redemptions per Customer</th>
    <th>Status</th>
@endsection
@section('tableBody')
    @foreach($codes->sortByDesc('id') as $code)
        <tr>
            <td></td>
            <td>{{$code->code}}</td>
            <td>{{$code->expires_on}}</td>
            <td>{{$code->applicable_to}}</td>
            <td>{{$code->category}}</td>
            <td>{{$code->reduction_type}}</td>
            <td>{{$code->value}}</td>
            <td>
                <form action="{{route('couponcode.destroy',$code->id)}}" method="POST" id="delete-form-{{$code->id}}">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <a href="#" onclick="return confirmation({{$code->id}});"><i class="fa fa-trash"></i> </a>
                </form>
                <a href="{{route('couponcode.edit',$code->id)}}"><i class="fa fa-edit"></i> </a>
            </td>
            <td>{{$code->total_redemptions}}</td>
            <td>{{($code->applicable_product != '' )?  $code->product->name : ''}}</td>
            <td>{{$code->redemptions_per_customer}}</td>
            <td>{{$code->status}}</td>

        </tr>
    @endforeach
@endsection
