@extends('admin.layouts.datatable')
@section('title','Abandoned Carts')
@section('tableTitle','Abandoned Cart')
@section('tableHead')
    <th>#</th>
    <th>Select</th>
    <th>Customer</th>
    <th>Email Id</th>
    <th>Phone</th>
    <th>Product</th>
    <th>Qty</th>
    <th>Date of Order</th>
    <th>Order Total</th>
    <th>Payment Status</th>
    <th>Coupon</th>
    <th>Shipping Address</th>
    <th>Billing Address</th>
    <th>Delivery Instruction</th>
@endsection
@section('tableBody')
    @foreach($orders->sortByDesc('created_at')->unique('customer_id')->sortByDesc('id') as $order)
        <tr>
            <td></td>
            <td>
                <input type="checkbox" id="bulk_chk" name="ids[]" value="{{$order->customer_id}}">
            </td>
            <td>{{$order->customer->firstname.' '.$order->customer->lastname}}</td>
            <td>{{$order->customer->email}}</td>
            <td>{{$order->customer->telephone}}</td>
            <td>{{$order->product->name}}<br/></td>
            <td>{{$order->quantity}}</td>
            <td>{{date_format($order->created_at,'M-d-Y')}}</td>
            <td>C${{$order->order_total}} </td>
            <td>{{$order->payment_status}}</td>
            <td>@if($order->couponCode != null){{$order->couponCode->code}}@endif</td>
            <td>{{$order->deliveryAddress->firstname.' '.$order->deliveryAddress->lastname}}<br/>
                {{$order->deliveryAddress->address_1}}<br/>
                {{$order->deliveryAddress->address_2}}<br/>
                {{$order->deliveryAddress->city}}<br/>
                {{$order->deliveryAddress->postcode}}<br/>
                {{$order->deliveryAddress->province}}

            </td>
            <td>{{$order->billingAddress->firstname.' '.$order->deliveryAddress->lastname}}<br/>
                {{$order->billingAddress->address_1}}<br/>
                {{$order->billingAddress->address_2}}<br/>
                {{$order->billingAddress->city}}<br/>
                {{$order->billingAddress->postcode}}<br/>
                {{$order->billingAddress->province}}
            </td>
            <td>{{$order->instruction}}</td>
        </tr>
    @endforeach

@endsection

@section('evenMoreScripts')
    <script type="text/javascript">
        $('#bulk_delt').click(function () {

            var ids=[];
            var token = $("meta[name='csrf-token']").attr("content");
            $('#bulk_chk:checked').each(function () {
                ids.push($(this).val());

            })
            if(ids.length > 0) {
                if (confirm("Are you sure you want to delete selected Items?")) {

                    $('#ids').val(ids);
                    $('#bulk_form').attr('action', function (i, value) {
                        return "/admin/order/bulkDelete/" + ids;
                    });
                    //$('#bulk_form').attr('action','bulkDelete/'+ids);
                }
            }
            else{
                $('#bulk_form').attr('action','')
                alert('Please select atleast one checkbox');
            }


        });
    </script>
@endsection
