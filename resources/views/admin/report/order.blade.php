@extends('admin.layouts.datatable')
@section('title','Orders')
@section('tableTitle','Order')
@section('tableHead')
    <th>#</th>
    <th>Date of Order</th>
    <th>Order Id</th>
    <th>Province</th>
    <th>Status</th>
    <th>Shipping Cost</th>
    <th>Discount</th>
    <th>Tax amount</th>
    <th>Total Amount</th>
@endsection
@section('tableBody')
    @php  $total_cost=0; @endphp
    @foreach($orders->sortByDesc('order_id') as $order)
            <tr>
                <td></td>
                <td>{{date_format($order->created_at,'Y-m-d')}}</td>
                <td>{{$order->order_id }}</td>
                <td>{{$order->deliveryAddress->province}}</td>
                <td>{{$order->status }}</td>
                <td>{{$order->OrderFeesplitup->shipping_charge}}</td>
                <td>{{$order->OrderFeesplitup->reduction}}</td>
                <td>{{($order->OrderFeesplitup->sub_total+$order->OrderFeesplitup->shipping_charge-$order->OrderFeesplitup->reduction)*$order->OrderFeesplitup->tax/100}}</td>
                <td>C${{$order->order_total}}</td>
            </tr>
    @endforeach

    <!-- date filter -->
    <div class="input-daterange">
        <div class="row">
            <div class="col-2">
                <div class="form-group">
                    <label for="province" class="control-label">Province</label>
                    <select name="province" id="province" class="form-control">
                        <option value="All">All</option>
                        <option value="Nunavut">Nunavut</option>
                        <option value="Quebec">Quebec</option>
                        <option value="Northwest Territories">Northwest Territories</option>
                        <option value="Ontario">Ontario</option>
                        <option value="British Columbia">British Columbia</option>
                        <option value="Alberta">Alberta</option>
                        <option value="Saskatchewan">Saskatchewan</option>
                        <option value="Manitoba">Manitoba</option>
                        <option value="Yukon">Yukon</option>
                        <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
                        <option value="New Brunswick">New Brunswick</option>
                        <option value="Nova Scotia">Nova Scotia</option>
                        <option value="Prince Edward Island">Prince Edward Island</option>

                    </select>
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <label for="status" class="control-label">Status</label>
                    <select name="status" id="status" class="form-control">
                        <option value="All">All</option>
                        <option value="Order placed">Order placed</option>
                        <option value="Processing">Processing</option>
                        <option value="Shipped">Shipped</option>
                        <option value="Delivered">Delivered</option>
                        <option value="Cancel requested">Cancel Requested</option>
                        <option value="Cancel & refunded">Cancel & refunded</option>
                        <option value="Return Requested">Return Requested</option>
                        <option value="Return Confirmed ">Return Confirmed</option>

                    </select>
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <label for="from" class="control-label">From</label>
                    <input type="text" id="from_date" name="from_date" class="form-control dateFilter" value="{{old('from_date')}}" >
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <label for="to" class="control-label">To</label>
                    <input type="text" id="to_date" name="to_date" required class="form-control dateFilter" value="{{old('to_date')}}" >
                </div>
            </div>
            <div class="col-1 pt-4">
                <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
            </div>
            <div class="col-1 pt-4" id="download_div">
                <div class="form-group">
                    <button type="button" class="btn btn-success" onclick="exportForm()" target="_blank"><i class="fa fa-download"></i></button>
                </div>
                <div style="display:none;" id="response"><i class="fa fa-cog fa-spin"></i>Please Wait</div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-4" id="total_div" style="display: none;">
            <div class="form-group">
                <label for="Amount" class="control-label">Total Amount</label>
                <input type="text" value="" class="form-control" name="total_amount" id="total_amount" readonly>
            </div>
        </div>
    </div>
@endsection

@section('evenMoreScripts')
    <!-- date filter -->
    <script>
        var total_amount = 0;
        $(document).ready(function(){
            $('.dateFilter').datepicker({
                dateFormat: "yy-m-dd"
            });
            var id_prev = 0;
            $.fn.dataTableExt.afnFiltering.push(
                function (settings, data, dataIndex) {
                    var min = $('#from_date').datepicker("getDate");
                    var max = $('#to_date').datepicker("getDate");
                    var province = $('#province').val();// selected province
                    var payment_status = $('#status').val();// selected status
                    console.log(payment_status);

                    var hireDate = new Date(data[1]); //date of order
                    var prov = data[3];
                    var status = data[4];
                    //console.log(min);
                    //console.log('to'+max);
                    if(min != null)
                    {
                        min=min.setHours(0,0,0,0);
                    }
                    if(max != null)
                    {
                        max=max.setHours(0,0,0,0);
                    }
                    hireDate=hireDate.setHours(0,0,0,0);
                    var order_total = data[8].slice(2); // $order->order_total
                    order = parseFloat(order_total.replace(/,/g,""));
                    var order_id = data[2];
                    // console.log('var:'+ order);
                    //  console.log(total_amount);
                    if (min == null && max == null && (province==prov || province == 'All') && (payment_status==status || payment_status == 'All')) {
                        if(status != 'Cancel & refunded' && status != 'Cancel & refunded' && id_prev != order_id)
                        {
                            total_amount = parseFloat(total_amount) + order;
                            id_prev = order_id;
                        }
                        return true; }
                    else if (min == null && hireDate <= max && (province==prov || province == 'All') && (payment_status==status || payment_status == 'All')) {
                        if( status != 'Cancel & refunded' && id_prev != order_id)
                        {
                            total_amount = parseFloat(total_amount) + order;
                            id_prev = order_id;
                        }
                        return true;}
                    else if(max == null && hireDate >= min && (province==prov || province == 'All') && (payment_status==status || payment_status == 'All')) {
                        if( status != 'Cancel & refunded' && id_prev != order_id)
                        {
                            total_amount = parseFloat(total_amount) + order;
                            id_prev = order_id;
                        }
                        return true;}
                    else if (min <= hireDate && hireDate <= max && (province==prov || province == 'All') && (payment_status==status || payment_status == 'All')) {
                        if( status != 'Cancel & refunded' && id_prev != order_id)
                        {   //console.log(id_prev);
                            //console.log(order);
                            total_amount = parseFloat(total_amount) + order;
                            id_prev = order_id;
                        }
                        return true; }
                    return false;
                }
            );
            $("#filter").click(function () {

                var myDataTable = $('#tb1').DataTable();
                myDataTable.draw();
                //console.log(total_amount);
                $('#total_div').css('display','block');
                $('#total_amount').val('C$'+parseFloat(total_amount).toFixed(2));
                total_amount = 0;
                id_prev = 0;

            });
        });
        function exportForm()
        {
            var from = $('#from_date').datepicker("getDate");
            var to = $('#to_date').datepicker("getDate");
            var province = $('#province').val();// selected province
            var status = $('#status').val();// selected status
            console.log(province);
            $('#response').css('display','block');
            if(from != null )
                from = from.getFullYear() + "/" +
                    (from.getMonth() + 1) + "/" + from.getDate();
            if(to != null)
                //to = to.toLocaleDateString();
                to = to.getFullYear() + "/" +
                    (to.getMonth() + 1) + "/" + to.getDate();
            console.log(from);

            url = '/admin/report/order-report';
            $.ajax({
                type: "post",
                url: url,
                cache: false,
                data: {_token: '{{csrf_token()}}',_method:"post",from: from,to:to,province:province,status:status},
                success: function (result) {
                    console.log(result);

                    if(result.success = 'success')

                    {
                        $('#response').css('display','none');
                        window.open(result.path,'_blank');
                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log('error');
                }
            });
        }
    </script>
@endsection


