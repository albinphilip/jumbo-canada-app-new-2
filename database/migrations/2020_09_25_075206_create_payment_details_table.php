<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('payment_id')->nullable();
            $table->string('object')->nullable();
            $table->string('amount')->nullable();
            $table->string('canceled_at')->nullable();
            $table->string('cancellation_reason')->nullable();
            $table->string('capture_method')->nullable();
            $table->string('client_secret')->nullable();
            $table->string('confirmation_method')->nullable();
            $table->string('created')->nullable();
            $table->string('currency')->nullable();
            $table->string('description')->nullable();
            $table->string('last_payment_error')->nullable();
            $table->string('livemode')->nullable();
            $table->string('next_action')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('payment_method_types')->nullable();
            $table->string('receipt_email')->nullable();
            $table->string('setup_future_usage')->nullable();
            $table->string('shipping')->nullable();
            $table->string('source')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_details');
    }
}
