<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuyBackCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buy_back_codes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('buy_back_id');
            $table->foreign('buy_back_id')->references('id')->on('buy_backs')->onUpdate('cascade')->onDelete('cascade');
            $table->string('code');
            $table->string('discount')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buy_back_codes');
    }
}
