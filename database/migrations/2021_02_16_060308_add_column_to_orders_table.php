<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('shipment_id')->nullable();
            $table->string('tracking_pin')->nullable();
            $table->string('shipment_price')->nullable();
            $table->string('shipping_label')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('shipment_id')->nullable();
            $table->dropColumn('tracking_pin')->nullable();
            $table->dropColumn('shipment_price')->nullable();
            $table->dropColumn('shipping_label')->nullable();
        });
    }
}
