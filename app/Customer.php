<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Customer extends Authenticatable
{
    use Notifiable;
    protected $guarded=['customer'];
    protected $table = 'customers';
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }
    public function wishlists(){
        return $this->hasMany(Wishlist::class);
    }
    public function orders()
    {
        return $this->hasMany(Order::class);
    }
    public function reviews()
    {
        return $this->hasMany(Reviews::class);
    }
    public function getAddress(){
        return $this->hasOne('App\Address')->where('type','=',null)->latest();
    }

}
