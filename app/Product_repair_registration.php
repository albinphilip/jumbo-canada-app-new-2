<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_repair_registration extends Model
{
    protected $guarded=[];
    public function store()
    {
        return $this->belongsTo(Store::class);
    }
}
