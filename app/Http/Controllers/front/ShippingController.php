<?php

namespace App\Http\Controllers\front;

use App\Additional_fee;
use App\Address;
use App\CouponCode;
use App\CouponRedemption;
use App\Customer;
use App\Gcaptcha;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderFee_splitup;
use App\Product;
use App\Product_variant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Auth;
use Cart;
use Intervention\Image\Facades\Image;
use Session;
use Stripe\Coupon;


class ShippingController extends Controller
{
    public function shipping()
    {
        //dd('here');
        $cartCollection = \Cart::Content();
        //dd($cartCollection);
        $shipping = null;
        $billing = null;
        if (session()->has('billingAddress'))
            $billing = Address::findOrFail(session('billingAddress'));
        if (session()->has('shippingAddress'))
            $shipping = Address::findOrFail(session('shippingAddress'));

        return view('front.checkout-shipping', ['cart' => $cartCollection, 'billing' => $billing, 'shipping' => $shipping]);
    }

    //guest checkout shipping address & billing address
    public function guest_storeaddress(Request $request)
    {

            //store details in customer table
            $data = $request->validate([
                'firstname' => 'required',
                'lastname' => 'required',
                'email' => 'required|email',
                'telephone' => 'required',
            ]);
            $data['password'] = Hash::make('');

            $customer = Customer::where('email', $request->email)->first();
            if (!$customer) {
                $customer = Customer::create($data);
            } else {
                $customer->update([
                    'telephone' => $data['telephone'],
                ]);
            }

            $data = $request->validate([
                'firstname' => 'required',
                'lastname' => 'required',
                'province' => 'required',
                'address_1' => 'required',
                'address_2' => 'nullable',
                'city' => 'required',
                'postcode' => 'required',
            ]);
            $data['zone_id'] = 1;
            $data['country_id'] = 1;
            $data['type'] = 'shipping';
            $data['customer_id'] = $customer->id;
            //update shippingAddress
            if (session()->has('shippingAddress')) {
                $address = Address::findOrFail(session('shippingAddress'));
                $address->update($data);
            } else
                $address = Address::create($data);
            session()->forget('shippingAddress');
            session()->put('shippingAddress', $address->id);
            //save province in session
            session()->forget('province');
            session()->put('province', $request->province);

            //store billing address
            $request->validate([
                'bill_firstname' => 'required',
                'bill_lastname' => 'required',
                'bill_province' => 'required',
                'bill_address_1' => 'required',
                'bill_address_2' => 'nullable',
                'bill_city' => 'required',
                'bill_postcode' => 'required',
            ]);
            $data = array(
                'firstname' => $request->bill_firstname,
                'lastname' => $request->bill_lastname,
                'province' => $request->bill_province,
                'address_1' => $request->bill_address_1,
                'address_2' => $request->bill_address_2,
                'city' => $request->bill_city,
                'postcode' => $request->bill_postcode,
                'type' => 'billing',
            );
            $data['zone_id'] = 1;
            $data['country_id'] = 1;
            $data['customer_id'] = $customer->id;
            if (session()->has('billingAddress')) {
                $billing = Address::findOrFail(session('billingAddress'));
                $billing->update($data);
            } else
                $billing = Address::create($data);
            session()->forget('billingAddress');
            session()->put('billingAddress', $billing->id);
            session()->forget('instruction');
            session()->put('instruction', $request->instruction);

            // calculate tax , shipping charge
            /*$fees = Additional_fee::where('province',Session::get('province'))->first();
            $total = str_replace(',', '',Session::get('total'));
            $tax = ($total+$fees->shipping_charge)*$fees->tax/100;
            $shipping_charge  = $fees->shipping_charge;
            $amount = $total+$fees->shipping_charge+$tax;
            $amount = number_format( $amount , 2 , '.' , ',' );*/

            /*session()->forget('final_amount');
            session()->forget('customer');
            session()->forget('tax');
            session()->forget('shipping');
            session()->forget('taxed');

            session()->put('final_amount',$amount);
            session()->put('customer',$customer->id);
            session()->put('tax',$fees->tax);
            session()->put('shipping',$fees->shipping_charge);
            session()->put('taxed',$tax);*/


            session()->forget('customer');
            session()->put('customer', $customer->id);

            return redirect(route('checkout'));


    }

    //STORE SHIPPING ADDRESS OF REGISTERED USER
    public function user_storeaddress(Request $request)
    {
        // dd($request);
        $customer = Auth::guard('customer')->user();
        $data = $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'province' => 'required',
            'address_1' => 'required',
            'address_2' => 'nullable',
            'city' => 'required',
            'postcode' => 'required',
        ]);
        $data['zone_id'] = 1;
        $data['country_id'] = 1;
        $data['type'] = 'shipping';
        $data['customer_id'] = $customer->id;
        $address = Address::create($data);
        session()->forget('shippingAddress');
        session()->put('shippingAddress', $address->id);
        // save province in session
        session()->forget('province');
        session()->put('province', $request->province);
        return redirect(route('shipping'))->with('msg', 'address added');
    }

//store user billing address
    public function user_billingaddress(Request $request)
    {
        $customer = Auth::guard('customer')->user();
        if (!session()->has('shippingAddress'))
            return back()->withInput()->with('error', 'Add Shipping address');
        $request->validate([
            'bill_firstname' => 'required',
            'bill_lastname' => 'required',
            'bill_province' => 'required',
            'bill_address_1' => 'required',
            'bill_address_2' => 'nullable',
            'bill_city' => 'required',
            'bill_postcode' => 'required',
        ]);
        $data = array(
            'firstname' => $request->bill_firstname,
            'lastname' => $request->bill_lastname,
            'province' => $request->bill_province,
            'address_1' => $request->bill_address_1,
            'address_2' => $request->bill_address_2,
            'city' => $request->bill_city,
            'postcode' => $request->bill_postcode,
            'type' => 'billing',
        );
        $data['zone_id'] = 1;
        $data['country_id'] = 1;
        $data['customer_id'] = $customer->id;

        if (session()->has('billingAddress')) {
            $billing = Address::findOrFail(session('billingAddress'));
            $billing->update($data);
        } else
            $billing = Address::create($data);

        session()->forget('billingAddress');
        session()->put('billingAddress', $billing->id);
        session()->forget('instruction');
        session()->put('instruction', $request->instruction);
        session()->put('customer', $customer->id);

        return redirect(route('checkout'));
    }
}
