<?php

namespace App\Http\Controllers\front;
use App\Http\Controllers\Controller;
use App\Wishlist;
use Illuminate\Http\Request;
use Auth;

class WishlistController extends Controller
{
    public function index()
    {
        $customer_id = Auth::guard('customer')->user()->id;
        $lists = Wishlist::where('customer_id',$customer_id)->get();

        return view('front.profile.favourite-list',['lists'=>$lists]);
    }
    public function store($request)
    {
       //dd($request);

        $data['product_id'] =$request;
        $data['customer_id'] = Auth::guard('customer')->user()->id;
        $list = Wishlist::where([['product_id',$data['product_id']],['customer_id',$data['customer_id']]])->first();
        if($list == null)
        {
            Wishlist::create($data);
            return back()->with('success','Item Added to favourite list');
        }
        else{
            return back()->with('error','Item already in favourite list');
        }
    }
    public function destroy($id)
    {
        $item = Wishlist::findOrFail($id);
        $item->delete();
        return back()->with('success','Item Removed From Favourite Lists');
    }
}
