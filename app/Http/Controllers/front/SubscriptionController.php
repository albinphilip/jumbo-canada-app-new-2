<?php

namespace App\Http\Controllers\front;
use App\Subscriber;
use App\Gcaptcha;
use App\Http\Controllers\Controller;
use App\Mail\subscriberEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SubscriptionController extends Controller
{
    public function subscribe(Request $request){
        //dd($request);
        /*$resultJson=Gcaptcha::verifyCaptcha( $request->get('recaptcha-response'));
        if ($resultJson->success != true) {
            return back()->withErrors(['captcha' => 'ReCaptcha Error']);
        }
        if ($resultJson->score >= 0.3)
        {*/
            $data = $request->validate([
                'email' => 'required|email',
            ]);
            $data['status'] = 'Subscribed';
            $subscriber = Subscriber::where('email', $data['email'])->first();
            if ($subscriber == null)
            {
                Subscriber::create($data);
                /*$email = $request->email;
                Mail::send('front.emails.subscribeMail', ['email' => $email], function ($message) use ($email) {
                    $message->to($email)
                        ->subject('Thank you for subscribing');});*/
                //Mail::to($email)->send(new SubscriberEmail($email));
                return ('OK');
            }
            else
            {
                if ($subscriber->status == 'Unsubscribed')
                {
                    $subscriber->status = 'Subscribed';
                    $subscriber->save();

                    return ('OK');
                }
                else
                {
                    return ('YES');
                }
            }
     /*   }
        else {
            return back()->withErrors(['captcha' => 'ReCaptcha Error']);
        }*/
    }
}
