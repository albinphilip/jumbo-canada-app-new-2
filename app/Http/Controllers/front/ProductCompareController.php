<?php

namespace App\Http\Controllers\front;
use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;
use Session;


class ProductCompareController extends Controller
{
    public function product_store($product_id)
    {
        $product = Product::findOrFail($product_id);
        $category = Category::findOrFail($product->category_id);
        //parts
        if(session::has('compare-'.strtolower(explode(' ',$category->name)[0])))
            {
                $i = 0;
                $products = session()->get('compare-'.strtolower(explode(' ',$category->name)[0]));
                foreach ($products as $key=>$item){
                    if($item['id'] == $product_id)
                    {
                        $i = 1;
                        break;
                    }
                }
                if($i != 1)
                    session()->push('compare-'.strtolower(explode(' ',$category->name)[0]), $product);
            }
            else {
                session()->put('compare-'.strtolower(explode(' ',$category->name)[0]),array($product));
            }
            return back()->with('success','Product Successfully added to your comparison list');

    }
    public function comparison($id)
    {
        $items = Session::get('compare-'.$id);
        if($items != null && count($items) > 4)
        $items = array_reverse(array_slice($items, -4));
        return view('front.product.product-compare',['products'=>$items]);
    }

    public function  destroy($id)
    {
        $product = Product::findOrFail($id);
        $category = Category::findOrFail($product->category_id);
        $products = session()->pull('compare-'.strtolower(explode(' ',$category->name)[0]), []); // Second argument is a default value
        foreach ($products as $key=>$item){
            if($item['id'] == $id)
                unset($products[$key]);
        }
        session()->put('compare-'.strtolower(explode(' ',$category->name)[0]), $products);
        return redirect(route('product.comparison',strtolower(explode(' ',$category->name)[0])))->with('success','Product Removed Successfully');


    }
}
