<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Product;
use App\Reviews;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Auth;

class ReviewController extends Controller
{

   public function index($id)
   {
       $product = Product::where('id',$id)->firstOrFail();
       return view('front.product.product-review',['product'=>$product]);
   }
   public function store(Request $request)
   {
       $customer_id = Auth::guard('customer')->user()->id;
       //dd($request);
        $data = $request->validate([
            'product_id' => 'required',
            'rating' => 'required|max:5',
            'review' => 'required',
        ]);
       $data['customer_id'] = $customer_id;
        Reviews::create($data);
        return redirect(route('orderDetails'))->with('success','Review Added Successfully');
   }
}
