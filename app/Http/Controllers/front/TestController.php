<?php

namespace App\Http\Controllers\front;
use App\Additional_fee;
use App\Address;
use App\CouponCode;
use App\CouponRedemption;
use App\Customer;
use App\Enquiry;
use App\Gcaptcha;
use App\Http\Controllers\Controller;
use App\Mail\EnquiryEmail;
use App\Order;
use App\OrderFee_splitup;
use App\Product;
use App\Product_return;
use App\Product_variant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Auth;
use Cart;
use Intervention\Image\Facades\Image;
use Session;
use Stripe\Coupon;
use Stripe\PaymentIntent;
use Stripe\Stripe;


class TestController extends Controller
{
    public function index(){
        $order=Order::findOrFail('41');
        dd($order->product->name);
    }
}
