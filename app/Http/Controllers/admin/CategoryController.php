<?php

namespace App\Http\Controllers\admin;

use App\Category;
use App\Product;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Cache;

class CategoryController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories=Category::all();
        return view('admin.category.index',['categories'=>$categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->validate([
            'name' => 'required|max:255',
            'description' =>'nullable',
            'keywords'=>'nullable',
            'icon'=>'nullable|image'
        ]);

        //Uploading and saving icon
        if($request->icon) {
            $image_path = request('icon')->store('uploads/category/icon', 'public');
            $naked_path = env('IMAGE_PATH') . $image_path;
            $photo = Image::make($naked_path)->fit(38,47);
            $photo->save();
            $data['icon']=$image_path;
        }
        else{
            $data['icon']='';
        }

        //Storing
        Category::create($data);
        Cache::forget('categories');
        return redirect(route('category.index'))->with('success','Category added successfully!');;


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category=Category::findOrFail($id);
        return view('admin.category.edit',['category'=>$category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category=Category::findOrFail($id);
        $data=$request->validate([
            'name' => 'required|max:255',
            'description' =>'nullable',
            'keywords'=>'nullable',
            'icon'=>'nullable|image'
        ]);

        //Uploading and saving icon
        if($request->icon) {
            $image_path = request('icon')->store('uploads/category/icon', 'public');
            $naked_path = env('IMAGE_PATH') . $image_path;
            $photo = Image::make($naked_path)->fit(38,47);
            $photo->save();
            $data['icon']=$image_path;
            //removing old image
            $file_path=env('IMAGE_PATH').$category->icon;
            if(file_exists($file_path))
            {
                @unlink($file_path);
            }
        }
        //updating
        $category->update($data);
        Cache::forget('categories');

        return redirect(route('category.index'))->with('success','Category updated successfully!');;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category=Category::findOrFail($id);
        //removing icon
        $file_path=env('IMAGE_PATH').$category->icon;
        if(file_exists($file_path))
        {
            @unlink($file_path);
        }
        //deleting

	$products = Product::where('category_id',$id)->get();
	foreach($products as $product)
		$product->delete();
	
        $category->delete();
        Cache::forget('categories');
        return back()->with('success','Category deleted successfully!');
    }
}
