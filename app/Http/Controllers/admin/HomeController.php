<?php

namespace App\Http\Controllers\admin;

use App\Customer;
use App\Http\Controllers\AdminController;
use App\Order;
use App\Product;
use App\Product_variant;
use Illuminate\Http\Request;

class HomeController extends AdminController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $customers = Customer::all()->count();
        $products = Product::all()->count();
        $orders = Order::distinct('order_id')->count();
        $out = Product_variant::where('quantity','<',5)->count();
        return view('admin.home',compact('customers','products','orders','out'));
    }
}
