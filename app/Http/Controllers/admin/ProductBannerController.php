<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Post;
use App\Product_banner;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Cache;

class ProductBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners=Product_banner::all();
        return view('admin.product-banner.index',['banners'=>$banners]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product-banner.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->validate([
            'banner_image' => 'required|image|max:500'
        ]);
        //Uploading and saving Cover
        if($request->banner_image) {
            $image_path = request('banner_image')->store('uploads/Product/Banner', 'public');
            $naked_path = env('IMAGE_PATH') . $image_path;
            $photo = Image::make($naked_path)->fit(802,476);
            $photo->save();
            $data['banner_image']=$image_path;
        }
        else{
            $data['banner_image']='';
        }

        //Storing
        Product_banner::create($data);
        Cache::forget('banners');
        return redirect(route('product-banner.index'))->with('success','Banner added successfully!');;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner=Product_banner::findOrFail($id);
        //removing cover
        $file_path=env('IMAGE_PATH').$banner->banner_image;
        if(file_exists($file_path))
        {
            @unlink($file_path);
        }
        //deleting
        $banner->delete();
        Cache::forget('banners');
        return back()->with('success','Banner deleted successfully!');
    }
}
