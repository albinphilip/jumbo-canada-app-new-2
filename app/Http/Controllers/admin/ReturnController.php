<?php

namespace App\Http\Controllers\admin;

use App\CanadaPost;
use App\Category;
use App\Discount;
use App\Http\Controllers\AdminController;
use App\Product_return;
use http\Env;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image;

class ReturnController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $returns = Product_return::all();
        //dd($returns);
        return view('admin.return.index', ['returns' => $returns]);
    }

    public function reject(Request $request)
    {
        $return = Product_return::where('return_id', $request->return_id)->first();
        $return->status = 'Rejected';
        $return->reject_reason = $request->reason;
        $return->save();
        //sending status update to user
        $email = $return->product_registration->email;
        //return($email);
        Mail::send('front.emails.return-request-rejected', ['return' => $return], function ($message) use ($email) {
            $message->to($email)
                ->subject('Jumbo Canada | Product return request rejected');
        });

        return response()->json("success");
    }

    public function approve(Request $request)
    {
        $return = Product_return::where('return_id', $request->return_id)->first();
        $return->status = 'Provisionally Approved';
        //check place of purchase
        if ($return->product_registration->store->address == 'Ultralinks Online Store') {
            //check whether weight and dimension entered
            if(($return->product_registration->product->productShipment->shipment_weight == null)
                ||($return->product_registration->product->productShipment->height == null )
                ||($return->product_registration->product->productShipment->length == null )
                ||($return->product_registration->product->productShipment->breadth == null))
                {
                    //return($return->product_registration->product->productShipment);
                $return->status = 'New';
                $return->save();
                return response("dimension");
            }
            else{
                //online purchase
                $email = $return->product_registration->email;
                //return($email);
               // get pdf from canada post
                $canadapost = $this->getReturnManifest($return);
                if(array_key_exists('tracking_pin',$canadapost) && array_key_exists('file_path',$canadapost)){
                    if(array_key_exists('expiry_date',$canadapost)) {
                        $exp_date = $canadapost['expiry_date'];
                    }
                    else $exp_date = '';
                    //store data to db
                    $data = array(
                        'status'=>'Provisionally Approved',
                        'tracking_pin'=>$canadapost['tracking_pin'],
                        'return_label'=>$canadapost['file_path'],
                        'shipping_partner' => 'Canada Post',
                        'expiry_date' => $exp_date,);

                    $return->update($data);
                    $file = $canadapost['file_path'];
                    Mail::send('front.emails.return-request-provisional-approve', ['return' => $return], function ($message) use ($email,$file) {
                        $message->to($email)
                            ->cc(env('MAIL_TO'))
                            ->subject('Jumbo Canada | Product return request provisionally approved')
                            ->attach($file);
                    });
                }
                else{
                    return response('error');
                }
            }

        } else {
            //offline purchase
            $return->save();
            $email = $return->product_registration->email;
            //return($email);
            Mail::send('front.emails.return-request-provisional-approve-offline', ['return' => $return], function ($message) use ($email) {
                $message->to($email)
                    ->subject('Jumbo Canada | Product return request provisionally approved');
            });
            //sending email to store
            $email = $return->product_registration->store->email;
            $client_email = env('MAIL_TO');
            //return($email);
            Mail::send('front.emails.return-request-provisional-approve-offline-to-store', ['return' => $return], function ($message) use ($email,$client_email) {
                $message->to($email)
                    ->cc($client_email)
                    ->subject('Jumbo Canada | Product return request provisionally approved');
            });

        }
        return response('success');
    }

    public function shippingDetails(Request $request)
    {
        //return($request);
        $data['shipping_date'] = today();
        $return = Product_return::where('return_id',$request->id)->first();
        $return->update(['shipping_partner' => $request->partner,
            'shippment_number' => $request->number,
            'shipping_date' => $data['shipping_date'],
            'status' =>'Replaced'

        ]);
        //Sending shipping mail
        $email = $return->product_registration->email;
        Mail::send('front.emails.return-shipped', ['return' => $return], function ($message) use ($email) {
            $message->to($email)
                ->subject('Jumbo Canada | Product replacement shipped');
        });
        return ('success');
    }
    // canada post
    public function getReturnManifest($return){

        //from data model
        $province_array = array('NL'=>'Newfoundland and Labrador','PE'=>'Prince Edward Island',
            'NS'=>'Nova Scotia','NB'=>'New Brunswick','QC'=>'Quebec','ON'=>'Ontario','MB'=>'Manitoba',
            'SK'=>'Saskatchewan','AB'=>'Alberta','YT'=>'Yukon','NT'=>'Northwest Territories','NU'=>'Nunavut','BC'=>'British Columbia');
       // return($return->product_registration->provenance);
        $returner = ([
            'name' => $return->product_registration->first_name.' '.$return->product_registration->last_name,
            'service_code' => 'DOM.EP',
            'company' => $return->product_registration->company,
            'address' => $return->product_registration->address,
            'city' => $return->product_registration->city,
            'province' => array_search($return->product_registration->provenance,$province_array),
            'postal_code' => preg_replace("/\s+/", "",$return->product_registration->zipcode),
            'weight' => round($return->product_registration->product->productShipment->shipment_weight),
            'length' => round($return->product_registration->product->productShipment->length),
            'breadth' => round($return->product_registration->product->productShipment->breadth),
            'height' => round($return->product_registration->product->productShipment->height),

        ]);
        $Post_item = new CanadaPost();
        $response = $Post_item->returnLabel($returner);

       // return response(['file'=>$response]);
        return $response;

    }
}
