<?php

namespace App\Http\Controllers\admin;

use App\DynamicPages;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DynamicPageController extends Controller
{

    public function edit($id)
    {
        $page=DynamicPages::findOrFail($id);
        return view('admin.dynamicpages.edit',['page'=>$page]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request);
        $page = DynamicPages::findOrFail($id);
        $data=$request->validate([
            'content' => 'nullable',
        ]);
        $page->update($data);
        return redirect(route('pages.edit',$id))->with('success','Updated successfully!');;
    }


}
