<?php

namespace App\Http\Controllers\admin;

use App\Buy_back;
use App\Buy_back_code;
use App\Category;
use App\CouponCode;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class BuyBackController extends Controller
{
    public function index()
    {
        $buy_backs=Buy_back::all();
        $categories = Category::all();
        return view('admin.buy-back.index',['buy_backs'=>$buy_backs,'category'=>$categories]);
    }

    public function buy_back_code()
    {
        $buy_back_codes=Buy_back_code::all();
        return view('admin.buy-back.code-index',['buy_back_codes'=>$buy_back_codes]);
    }
    public function approve(Request $request)
    {
        $buyback = Buy_back::findOrFail($request->buyback_id);
        $number = mt_rand('111',99999);
        $coupon_code = 'BUYBACK'.$number;
        $data = array(
            'code' => $coupon_code,
            'expires_on' => $request->expiry,
            'applicable_to' => 'All',
            'reduction_type' => 'Percent',
            'value' => $request->discount ,
            'total_redemptions' => 1 ,
            'redemptions_per_customer' => 1,
            'status' => 'Published',
            'category'=>$request->category,
            );
        if(CouponCode::create($data))
        {
            $buyback->update(['status'=>'Approved']);
            //mail
            $email = $buyback->email;
            Mail::send('front.emails.buy-back-approval', ['buyback' => $buyback, 'coupon' => $data], function ($message) use ($email) {
                $message->to($email)
                    ->subject('Jumbo Canada | Buy back request Approval');
            });
            return ('success');
        }
        else

            return ('error');
    }
}
