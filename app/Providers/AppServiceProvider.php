<?php

namespace App\Providers;

use App\Brand;
use App\Category;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use App\Free_shipping;


use Illuminate\Support\Facades\Cache;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        $categories = Cache::rememberForever('categories', function () {
            return Category::all();
        });
        $brands = Cache::rememberForever('brands', function () {
            return Brand::all();
        });
        $shipping = Cache::rememberForever('shipping', function () {
            return Free_shipping::all();
        });
       // $shipping = Free_shipping::orderBy('id','desc')->pluck('rate')->first();
        if(!empty($shipping)) $rate = $shipping[0]->rate;
        else $rate = '199';

        View::share(['categories'=> $categories,'brands' => $brands ,'free' =>$rate]);

    }
}
