<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $guarded=[];
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
    public function zone()
    {
        return $this->belongsTo(Zone::class);
    }
}
