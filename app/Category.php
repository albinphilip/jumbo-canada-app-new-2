<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded=[];
    public  function products()
    {
        return $this->hasMany(Product::class);
    }
    public function couponCodes()
    {
        return $this->hasMany(CouponCode::class);
    }
    public function shippingDiscounts()
    {
        return $this->hasMany(Shipping_discount::class);
    }
    public function brand(){
        return $this->belongsTo(Brand::class);
    }
}
