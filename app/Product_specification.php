<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_specification extends Model
{
    protected $guarded = [];
    public  function product()
    {
        return $this->belongsTo(Product::class);
    }
}
